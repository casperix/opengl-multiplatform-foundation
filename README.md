# opengl-multiplatform-foundation

Support for the OpenGl interface regardless of the platform.

Currently supported syntax is like to **OpenGl 3.0**.  In other words, you can write code with minor differences from the documentation on the page [https://docs.gl/gl3/glActiveTexture](https://docs.gl/gl3/glActiveTexture). The code will work on all supported platforms

## Renderer
You can use special textures to achieve the best effect
![sample.png](sample.png)

## Sample

In `build.gradle`, in the dependencies block, write the following:
```
implementation "io.gitlab.casperix:opengl-renderer2d:1.1.0"
```

See project
[minimal-jvm](samples%2Fminimal-jvm) 
in particular the 
[MinimalApplication.kt](samples%2Fminimal-jvm%2Fsrc%2Fmain%2Fkotlin%2Fru%2Fcasperix%2Fapp%2FMinimalApplication.kt)
file.

In code:
```
//  initialize renderer
val renderer = OpenGlRenderer2d()

val dimension = Dimension2i(500, 500)

//  set window area for renderer
renderer.viewPort = dimension

//  configure renderer
renderer.environment = Environment(
    backgroundColor = Colors.NAVY,
    projectionMatrix = Matrix3f.orthographic(dimension, true),
    ambientColor = Colors.GRAY,
    lightPosition = Vector3f(1000f),
)

//  draw something
renderer.clear()
renderer.drawQuad(
    Colors.RED, Quad2f(
        Vector2f(0f, 0f),
        Vector2f(100f, 0f),
        Vector2f(100f, 100f),
        Vector2f(0f, 100f),
    )
)
renderer.flush()        
```

See also [sample](samples)  directory

## supported platform
* browser (js, webgl-2)
* android (jvm, native gles-3.0)
* desktop (jvm, lwjgl-3.3.3)

## capabilities
All you need to draw a simple colored, textured triangle
- colored triangle
- textured triangle
- diffuse & normal & specular triangle 
- draw text

## support
* browser (js) ~95%
* browser (wasm) ~95%
* desktop (jvm) ~95%
* android ~95%

## deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`sonatypeStagingProfileId`,
`sonatypeUsername`,
`sonatypePassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.

Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin