package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.color.Colors
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.misc.nextString
import ru.casperix.misc.toFloat
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.font.FontWeight
import ru.casperix.multiplatform.text.drawText
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Environment
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.DurationUnit

class TextPerfApplication(private val surface: Surface) : SurfaceListener {
    //  initialize renderer
    private val renderer = OpenGlRenderer2d()

    private var angle = DegreeFloat(0f)
    private val random = Random(1)

    override fun nextFrame(tick: Duration) {
        val dimension = surface.getSize()

        //  set window area for renderer
        renderer.viewPort = dimension

        //  configure renderer
        renderer.environment = Environment(
            backgroundColor = Colors.NAVY,
            projectionMatrix = Matrix3f.orthographic(dimension, true),
            ambientColor = Colors.GRAY,
            lightPosition = Vector3f(1000f),
        )

        //  draw something
        renderer.clear()

        renderer.drawText("Hello", FontReference("Serif", 32), Matrix3f.IDENTITY)
        renderer.drawText(random.nextString(4..16), FontReference("Serif", 256), Matrix3f.translate(Vector2f(0f, 32f)))

        renderer.flush()
    }

    override fun dispose() {

    }

    override fun input(event: InputEvent) {

    }
}