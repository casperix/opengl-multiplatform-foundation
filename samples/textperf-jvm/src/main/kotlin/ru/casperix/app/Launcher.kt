package ru.casperix.app

import ru.casperix.opengl.core.app.jvmSurfaceLauncher

fun main() = jvmSurfaceLauncher {
    TextPerfApplication(it)
}