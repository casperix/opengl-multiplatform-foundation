package ru.casperix.app.scene

import ru.casperix.app.Assets
import ru.casperix.app.Texture2DContainer
import ru.casperix.math.vector.float32.Vector2f

enum class Model(
    val baseSize:Vector2f,
    val albedo: Texture2DContainer,
    val opacity: Texture2DContainer,
    val ambient: Texture2DContainer,
    val normal: Texture2DContainer,
    val specular: Texture2DContainer,
    val shines: Texture2DContainer,
) {
    LORRY_GREEN(
        Vector2f(4f, 2f),
        Assets.lorry_albedo,
        Assets.lorry_albedo,
        Assets.lorry_ambient,
        Assets.lorry_normals,
        Assets.lorry_specular,
        Assets.lorry_specular,
    ),
    LORRY_GREEN_CARGO(
        Vector2f(4f, 2f),
        Assets.lorry_cargo_albedo,
        Assets.lorry_cargo_albedo,
        Assets.lorry_ambient,
        Assets.lorry_cargo_normals,
        Assets.lorry_cargo_specular,
        Assets.lorry_cargo_specular,
    ),
    LORRY_RED(
        Vector2f(4f, 2f),
        Assets.lorry2_albedo,
        Assets.lorry2_albedo,
        Assets.lorry_ambient,
        Assets.lorry_normals,
        Assets.lorry_specular,
        Assets.lorry_specular,
    ),
    LORRY_RED_CARGO(
        Vector2f(4f, 2f),
        Assets.lorry2_cargo_albedo,
        Assets.lorry2_cargo_albedo,
        Assets.lorry_ambient,
        Assets.lorry_cargo_normals,
        Assets.lorry_cargo_specular,
        Assets.lorry_cargo_specular,
    ),
    SAND_SPHERE(
        Vector2f(3f, 3f),
        Assets.template_albedo,
        Assets.template_opacity,
        Assets.template_ambient,
        Assets.template_normals,
        Assets.template_specular,
        Assets.template_specular,
    ),
}