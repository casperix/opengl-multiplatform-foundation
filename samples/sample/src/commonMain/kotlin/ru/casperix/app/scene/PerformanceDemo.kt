package ru.casperix.app.scene

import ru.casperix.app.Assets
import ru.casperix.demo_platform.AbstractDemo
import ru.casperix.demo_platform.config.CameraConfig
import ru.casperix.demo_platform.config.SceneConfig
import ru.casperix.demo_platform.renderer.RenderConfig
import ru.casperix.demo_platform.renderer.RenderHelper
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.collection.getLooped
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.misc.nextString
import ru.casperix.multiplatform.text.addText
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.light.PointLight
import ru.casperix.renderer.material.PhongMaterial
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.vector.VectorGraphic
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder
import kotlin.random.Random

class PerformanceDemo : AbstractDemo {

    private var factor = 4
    private var useCache = false
    private var useText = false
    private var cachedGraphicList: List<VectorGraphic>? = null

    private val random = Random(1)
    private val randomStringList = (0 until  1000).map { random.nextString(1..7)  }

    private val info = Label()
    override val config = SceneConfig(camera = CameraConfig())
    override val name: String = "Performance"
    override val root = Container(
        Layout.VERTICAL,
        Panel(
            Layout.VERTICAL,
            info,
            Container(
                Layout.HORIZONTAL,
                ToggleButton("Cache graphic", useCache) { useCache = isChecked;drop() },
                ToggleButton("Use text", useText) { useText = isChecked;drop() },
            ).setSizeMode(SizeMode.row),
            ToggleGroup(
                Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                ToggleButton("4x4", false) { if (isChecked) factor = 4;drop() },
                ToggleButton("16x16", false) { if (isChecked) factor = 16;drop() },
                ToggleButton("32x32", false) { if (isChecked) factor = 32;drop() },
                ToggleButton("64x64", false) { if (isChecked) factor = 64;drop() },
                ToggleButton("128x128", false) { if (isChecked) factor = 128;drop() },
                ToggleButton("256x256", false) { if (isChecked) factor = 256;drop() },
            ),
        ),
    ).setSizeMode(SizeMode.view)

    private fun drop() {
        cachedGraphicList = null
    }

    override fun render(renderer: Renderer2D) {

        (renderer as? OpenGlRenderer2d)?.let {
            it.statistic.nextFrame()
            info.text = it.statistic.last.run {
                listOf(
                    "batches: " + batches,
                    "states: " + states,
                    "triangles: " + triangles,
                    "states: " + states,
                    "dynamicBufferAmount: " + dynamicBufferAmount,
                    "staticBufferAmount: " + staticBufferAmount,
                ).joinToString("\n")
            }
        }

        renderer.environment.lights = listOf(PointLight.DEFAULT.copy(position = Vector3f.Z * 20f))

        if (useCache) {
            val graphic = cachedGraphicList ?: run {
                (0 until factor).map { x ->
                    VectorGraphicBuilder.build { generateGraphic(this, renderer, x) }
                }
            }
            cachedGraphicList = graphic
            graphic.forEach {
                renderer.drawGraphic(it)
            }
        } else {
            (0 until factor).forEach { x ->
                renderer.drawGraphic {
                    generateGraphic(this, renderer, x)
                }
            }
        }

        if (RenderConfig.showAxis) {
            RenderHelper.renderAxisSystem(renderer)
        }
    }

    enum class CellType {
        SIMPLE_COLORED,
        SIMPLE_TEXTURED,
        PHONG_TEXTURED,
        TEXT,
    }

    private fun generateGraphic(builder: VectorGraphicBuilder, renderer: Renderer2D, x: Int) {
        val albedo = Assets.lorry_cargo_albedo.getOrNull() ?: return
        val normals = Assets.lorry_cargo_normals.getOrNull() ?: return
        val specular = Assets.lorry_cargo_specular.getOrNull() ?: return

        (0 until factor).forEach { y ->
            val positions = Box2f.byDimension(Vector2f(x.toFloat(), y.toFloat()), Vector2f.ONE).grow(-0.1f)

            when (getCellType(x, y)) {
                CellType.SIMPLE_TEXTURED -> builder.addRect(SimpleMaterial(albedo), positions, Box2f.ONE)
                CellType.PHONG_TEXTURED -> builder.addRect(PhongMaterial(albedo, specular, null, normals), positions, Box2f.ONE)
                CellType.SIMPLE_COLORED -> {
                    val color = RgbColor3f(x / factor.toFloat(), 0f, 0f)
                    builder. addRect(color, positions)
                }
                CellType.TEXT -> {
                    val m = Matrix3f.scale(Vector2f(0.01f)) * Matrix3f.translate(x.toFloat(), y.toFloat())
                    val s = randomStringList.getLooped(x + y * 1024)
                    builder.addText(s, m)
                }
            }
        }
    }

    private fun getCellType(x: Int, y: Int): CellType {
        if (x % 4 == 0) return CellType.PHONG_TEXTURED
        if (x % 4 == 1) return CellType.SIMPLE_TEXTURED
        if (x % 4 == 2 && useText) return CellType.TEXT
        return CellType.SIMPLE_COLORED
    }
}