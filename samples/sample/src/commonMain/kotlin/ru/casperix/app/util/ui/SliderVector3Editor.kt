package ru.casperix.app.util.ui

import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.slider.Slider
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.signals.concrete.StorageSignal

class SliderVector3Editor(
    val title: String,
    val componentRange:ClosedFloatingPointRange<Float> = 0f..1f,
    val skin: SliderValueEditorSkin = SliderValueEditorSkin(),
) {
    private val sliderX = SliderValueEditor("X")
    private val sliderY = SliderValueEditor("Y")
    private val sliderZ = SliderValueEditor("Z")
    private val info = Label("")

    val valueListener = StorageSignal(Vector3f.X)
    val root = Panel(
        Layout.VERTICAL,
        Container(
            OrientationLayout(Orientation.HORIZONTAL, AlignMode.RIGHT_TOP),
            info,
        ).setSizeMode(SizeMode.row),
        sliderX.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
        sliderY.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
        sliderZ.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
    ).setSizeMode(SizeMode.content)

    private var selfChanged = false

    init {
        sliderX.valueListener.then {
            updateValue()
        }
        sliderY.valueListener.then {
            updateValue()
        }
        sliderZ.valueListener.then {
            updateValue()
        }
        valueListener.then {
            selfChanged = true
            valueListener.value.apply {
                sliderX.valueListener.value = SliderValueEditor.rangeNormalize(x, componentRange)
                sliderY.valueListener.value = SliderValueEditor.rangeNormalize(y, componentRange)
                sliderZ.valueListener.value = SliderValueEditor.rangeNormalize(z, componentRange)
            }
            selfChanged = false

            updateInfo()
        }
        updateInfo()
    }

    private fun updateValue() {
        if (selfChanged) return

        valueListener.value = Vector3f(
            SliderValueEditor.rangeDenormalize(sliderX.valueListener.value, componentRange),
            SliderValueEditor.rangeDenormalize(sliderY.valueListener.value, componentRange),
            SliderValueEditor.rangeDenormalize(sliderZ.valueListener.value, componentRange),
        )
    }

    private fun updateInfo() {
        info.text = title
    }

}