package ru.casperix.app

import ru.casperix.app.scene.*
import ru.casperix.app.surface.DesktopWindow
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.app.util.CompoundSurfaceListener
import ru.casperix.app.util.ContinuousRenderSupport
import ru.casperix.demo_platform.application.DemoApplication
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.opengl.renderer.OpenGlRenderer2d

fun createApplication(surface: Surface): SurfaceListener {
    (surface as? DesktopWindow)?.maximize()
    PlatformMisc.continuousRendering = false

    val renderer = OpenGlRenderer2d()
    return CompoundSurfaceListener(
        listOf(
            DemoApplication(
                surface, renderer, listOf(
                    PhongMaterialDemo(renderer),
                    PerformanceDemo(),
                    TileMapDemo(renderer),
                    TextDemo(),
                    DisplayDemo(surface, renderer),
                )
            ),
            ContinuousRenderSupport(surface)
        )
    )
}