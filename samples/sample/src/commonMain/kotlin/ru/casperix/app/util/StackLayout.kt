package ru.casperix.app.util

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f

class StackLayout : Layout {
    override fun calculate(placement: ElementPlacement, items: List<LayoutTarget>): Box2f {
        val mainViewportSize = placement.viewportSize
        items.forEach {
            it.placement.apply {
                viewportSize = calculateSize(mainViewportSize, contentArea.dimension, mainViewportSize)
            }
        }
        return Box2f(Vector2f.ZERO, mainViewportSize)
    }
}