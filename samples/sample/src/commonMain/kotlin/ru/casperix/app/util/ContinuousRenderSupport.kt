package ru.casperix.app.util

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.misc.time.getTime
import kotlin.time.Duration

class ContinuousRenderSupport(val surface: Surface) : SurfaceListener {
    private var lastInputTime = Duration.ZERO

    override fun dispose() {

    }

    override fun input(event: InputEvent) {
        lastInputTime = getTime()
        update()
    }

    override fun nextFrame(tick: Duration) {
        update()
    }

    private fun update() {
        if ((getTime() - lastInputTime).inWholeMilliseconds <= 1500) {
            surface.requestFrame()
        }
    }

}