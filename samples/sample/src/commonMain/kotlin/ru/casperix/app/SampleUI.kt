package ru.casperix.app

import ru.casperix.app.scene.Scene
import ru.casperix.light_ui.component.tabmenu.Tab
import ru.casperix.light_ui.component.tabmenu.TabMenu
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.LayoutSide

class SampleUI(sceneList: List<Scene>) {
    val tabs = sceneList.map { scene ->
        Tab(scene.name, scene.ui)
    }
    val txtInfo = Label()
    val tabMenu = TabMenu(LayoutSide.LEFT, tabs).apply {
        setSizeMode(SizeMode.view)
    }
    val root: Element = Container(
        Layout.VERTICAL,
        txtInfo,
        tabMenu,
    ).setSizeMode(SizeMode.max)
}