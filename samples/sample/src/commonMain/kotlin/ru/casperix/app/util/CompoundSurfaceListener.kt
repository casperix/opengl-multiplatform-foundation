package ru.casperix.app.util

import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import kotlin.time.Duration

class CompoundSurfaceListener(val listeners: List<SurfaceListener>) : SurfaceListener {
    override fun dispose() {
        listeners.forEach { it.dispose() }
    }

    override fun input(event: InputEvent) {
        listeners.forEach { it.input(event) }
    }

    override fun nextFrame(tick: Duration) {
        listeners.forEach { it.nextFrame(tick) }
    }
}