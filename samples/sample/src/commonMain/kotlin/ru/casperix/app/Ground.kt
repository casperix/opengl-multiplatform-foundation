package ru.casperix.app

enum class Ground(val albedoIndex: UByte, val normalIndex: UByte) {
    TEMPLATE(7u, 16u),
    SOIL(7u, 9u),
    DRY(1u, 9u),
    HERB(3u, 10u),
    FOREST(2u, 10u),
    ROCK(4u, 11u),
    ROCK2(4u, 12u),
    ROCK3(4u, 13u),
}