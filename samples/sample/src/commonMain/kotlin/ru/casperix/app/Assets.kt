package ru.casperix.app

import ru.casperix.math.color.rgb.RgbColor3b
import ru.casperix.math.perlin.ValueNoise2D
import ru.casperix.math.vector.float64.Vector3d
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.Texture2DArray
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.material.TextureFilter
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.pixel_map.Rgb8PixelMap
import kotlin.random.Random

object Assets {
    fun loadTexture(path: String): Texture2DContainer {
        return Texture2DContainer(path, TextureConfig(TextureFilter.LINEAR, TextureFilter.NEAREST))
    }

    val sample = loadTexture("assets/sample.png")

    val lorry_specular = loadTexture("assets/small_lorry_specular.png")
    val lorry_ambient = loadTexture("assets/small_lorry_ambient.png")
    val lorry_albedo = loadTexture("assets/small_lorry_albedo.png")
    val lorry_normals = loadTexture("assets/small_lorry_normals.png")

    val lorry_cargo_albedo = loadTexture("assets/small_lorry_cargo_albedo.png")
    val lorry_cargo_normals = loadTexture("assets/small_lorry_cargo_normals.png")
    val lorry_cargo_specular = loadTexture("assets/small_lorry_cargo_specular.png")

    val lorry2_cargo_albedo = loadTexture("assets/small_lorry2_cargo_albedo.png")
    val lorry2_albedo = loadTexture("assets/small_lorry2_albedo.png")

    val tile_clay = loadTexture("assets/tiles/albedo/clay0.png")
    val tile_dry = loadTexture("assets/tiles/albedo/dry0.png")
    val tile_forest = loadTexture("assets/tiles/albedo/forest0.png")
    val tile_herb = loadTexture("assets/tiles/albedo/herb0.png")
    val tile_rock = loadTexture("assets/tiles/albedo/rock0.png")
    val tile_sand = loadTexture("assets/tiles/albedo/sand0.png")
    val tile_snow = loadTexture("assets/tiles/albedo/snow0.png")
    val tile_soil = loadTexture("assets/tiles/albedo/soil0.png")

    val tile_flat_normal = loadTexture("assets/tiles/normals/flat.png")
    val tile_dry_normal = loadTexture("assets/tiles/normals/dry0.png")
    val tile_herb_normal = loadTexture("assets/tiles/normals/herb.png")
    val tile_rock1_normal = loadTexture("assets/tiles/normals/rock1.png")
    val tile_rock2_normal = loadTexture("assets/tiles/normals/rock2.png")
    val tile_rock3_normal = loadTexture("assets/tiles/normals/rock3.png")
    val tile_sand1_normal = loadTexture("assets/tiles/normals/sand1.png")
    val tile_sand2_normal = loadTexture("assets/tiles/normals/sand2.png")

    val template_ambient = loadTexture("assets/test_ambient.png")
    val template_opacity = loadTexture("assets/test_opacity.png")
    val template_albedo = loadTexture("assets/test_albedo.png")
    val template_normals = loadTexture("assets/test_normals.png")
    val template_specular = loadTexture("assets/test_specular.png")

    val tile_map = Texture2D(run {
        val pixelMap = Rgb8PixelMap(16, 16, "types")

        val r = Random(1)
        val noise = ValueNoise2D(scale = Vector3d(1.0 / 4.0))
        (0 until pixelMap.width).forEach { x ->
            (0 until pixelMap.height).forEach { y ->

                val value = noise.output(x.toDouble(), y.toDouble())

                val ground = if (y == 0) {
                    Ground.TEMPLATE
                } else if (value < -0.4) {
                    Ground.SOIL
                } else if (value < -0.2) {
                    Ground.DRY
                } else if (value < 0.0) {
                    Ground.HERB
                } else if (value < 0.2) {
                    Ground.FOREST
                } else {
                    listOf(Ground.ROCK, Ground.ROCK2, Ground.ROCK3).random(r)
                }

                pixelMap.set(x, y, RgbColor3b(ground.albedoIndex, ground.normalIndex, 0u))
            }
        }
        pixelMap
    }, TextureConfig(TextureFilter.NEAREST, TextureFilter.NEAREST, useMipMap = false))


    fun getTexturesNoMipMap(): Texture2DArray? {
        Assets.apply {
            val tile_clay = tile_clay.getOrNull() ?: return null
            val tile_dry = tile_dry.getOrNull() ?: return null
            val tile_forest = tile_forest.getOrNull() ?: return null
            val tile_herb = tile_herb.getOrNull() ?: return null
            val tile_rock = tile_rock.getOrNull() ?: return null
            val tile_sand = tile_sand.getOrNull() ?: return null
            val tile_snow = tile_snow.getOrNull() ?: return null
            val tile_soil = tile_soil.getOrNull() ?: return null

            val tile_flat_normal = tile_flat_normal.getOrNull() ?: return null
            val tile_dry_normal = tile_dry_normal.getOrNull() ?: return null
            val tile_herb_normal = tile_herb_normal.getOrNull() ?: return null
            val tile_rock1_normal = tile_rock1_normal.getOrNull() ?: return null
            val tile_rock2_normal = tile_rock2_normal.getOrNull() ?: return null
            val tile_rock3_normal = tile_rock3_normal.getOrNull() ?: return null
            val tile_sand1_normal = tile_sand1_normal.getOrNull() ?: return null
            val tile_sand2_normal = tile_sand2_normal.getOrNull() ?: return null
            val template_normals = template_normals.getOrNull() ?: return null

            return Texture2DArray(
                listOf(
                    tile_clay,//0
                    tile_dry,//1
                    tile_forest,//2
                    tile_herb,//3
                    tile_rock,//4
                    tile_sand,//5
                    tile_snow,//6
                    tile_soil,//7

                    tile_flat_normal,//8
                    tile_dry_normal,//9
                    tile_herb_normal,//10
                    tile_rock1_normal,//11
                    tile_rock2_normal,//12
                    tile_rock3_normal,//13
                    tile_sand1_normal,//14
                    tile_sand2_normal,//15
                    template_normals,//16
                ).map { it.map },
                TextureConfig(TextureFilter.NEAREST, TextureFilter.NEAREST, useMipMap = false)
            )
        }
    }

    fun getTexturesLinearMipMap(): Texture2DArray? {
        return getTexturesNoMipMap()?.run {
            copy(config = TextureConfig(TextureFilter.LINEAR, TextureFilter.NEAREST, useMipMap = true))
        }
    }

    fun getTexturesNearestMipMap(): Texture2DArray? {
        return getTexturesNoMipMap()?.run {
            copy(config = TextureConfig(TextureFilter.NEAREST, TextureFilter.NEAREST, useMipMap = true))
        }
    }
}