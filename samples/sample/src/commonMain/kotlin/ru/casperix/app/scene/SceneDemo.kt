package ru.casperix.app.scene

import ru.casperix.app.util.ui.OpenableContainer
import ru.casperix.demo_platform.PointBasedDemo
import ru.casperix.demo_platform.renderer.RenderConfig
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.scroll.ScrollBox
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.math.color.Color
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.opengl.renderer.OpenGlRendererConfig
import ru.casperix.opengl.renderer.impl.ShaderLightModel
import ru.casperix.renderer.Renderer2D

abstract class SceneDemo(val renderer: OpenGlRenderer2d) : PointBasedDemo() {
    class LightEntry(val container: OpenableContainer, val editor: LightEditor)

    val lightEditorList = mutableListOf<LightEntry>()
    val materialEditor = MaterialEditor()

    final override val root = ScrollBox().setSizeMode(FixedDimension(500f), ViewDimension)
    private val panel = Panel(
        Layout.VERTICAL,
        ToggleGroup(
            Layout.HORIZONTAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            ToggleButton("Phong", true) {
                if (isChecked) renderer.config = OpenGlRendererConfig(lightModel = ShaderLightModel.PHONG)
            },
            ToggleButton("Blinn-Phong", true) {
                if (isChecked) renderer.config = OpenGlRendererConfig(lightModel = ShaderLightModel.BLINN_PHONG)
            },
        ),
        Container(
            Layout.HORIZONTAL,
            PushButton("Add light") { addLight() },
            PushButton("Remove light") { removeLight() },
        ),
        OpenableContainer("Material", materialEditor.root),
    )

    init {
        root.content.element = panel
        addLight()
    }

    fun addElements(vararg items: Element) {
        panel.children += items.toList().wrapNodes()
    }

    fun addLight() {
        val lightEditor = LightEditor(pointController)
        val lightEntry = LightEntry(OpenableContainer("Light", lightEditor.root), lightEditor)

        panel.children += Node(lightEntry.container)
        lightEditorList += lightEntry

        renderer.apply {
            RenderConfig.showAxis = false
            config = config.copy(lightMaxAmount = lightEditorList.size)
        }
    }

    fun removeLight() {
        if (lightEditorList.size <= 1) return

        val lightEntry = lightEditorList.removeLast()
        panel.children.removeAll { it.element == lightEntry.container }
        lightEntry.editor.dispose()
        lightEditorList -= lightEntry

        renderer.apply {
            config = config.copy(lightMaxAmount = lightEditorList.size)
        }
    }

    fun setupEnvironment(renderer: Renderer2D) {
        renderer.environment.lights = lightEditorList.map {
            it.editor.buildLight()
        }
    }
}