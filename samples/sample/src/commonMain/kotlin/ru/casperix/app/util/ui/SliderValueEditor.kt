package ru.casperix.app.util.ui

import ru.casperix.app.util.StackLayout
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.slider.Slider
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.types.Orientation
import ru.casperix.misc.toPrecision
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.signals.concrete.StorageSignal

class SliderValueEditor(
    val title: String,
    val range: ClosedFloatingPointRange<Float> = 0f..1f,
    val valueFormatter: (Float) -> String = { it.toPrecision(1) },
    val skin: SliderValueEditorSkin = SliderValueEditorSkin(),
) {
    private val slider = Slider()
    private val name = Label("")
    private val value = Label("")

    val valueListener = StorageSignal(0f)
    val root = Panel(
        OrientationLayout(Orientation.HORIZONTAL, AlignMode.RIGHT_TOP),
        name,
        Container(
            StackLayout(),
            slider.setSizeMode(SizeMode.fixed(skin.defaultSize)),
            value.setSizeMode(SizeMode.fixed(skin.defaultSize)),
        ).setSizeMode(SizeMode.fixed(skin.defaultSize))
    ).setSizeMode(SizeMode.content)

    private var selfChanged = false

    init {
        slider.selectSignal.then {
            if (!selfChanged) {
                valueListener.value = rangeDenormalize(slider.value, range)
            }
        }
        valueListener.then {
            selfChanged = true
            slider.value = rangeNormalize(valueListener.value, range)
            selfChanged = false

            updateInfo()
        }

        updateInfo()
    }

    private fun updateInfo() {
        name.text = "$title:"
        value.text = valueFormatter(valueListener.value)
    }

    companion object {
        fun rangeNormalize(raw: Float, range: ClosedFloatingPointRange<Float>) =
            (raw - range.start) / (range.endInclusive - range.start)

        fun rangeDenormalize(normalized: Float, range: ClosedFloatingPointRange<Float>) =
            range.start + (range.endInclusive - range.start) * normalized

    }
}