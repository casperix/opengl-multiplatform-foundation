package ru.casperix.app.scene

import ru.casperix.app.util.ui.OpenableContainer
import ru.casperix.demo_platform.config.CameraConfig
import ru.casperix.demo_platform.config.SceneConfig
import ru.casperix.demo_platform.renderer.RenderConfig
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.addText
import ru.casperix.multiplatform.text.drawText
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.TextureColorSource
import ru.casperix.renderer.material.TextureVector1Source
import ru.casperix.renderer.material.TextureVector3Source

class PhongMaterialDemo(renderer:OpenGlRenderer2d) : SceneDemo(renderer) {


    private var model = Model.SAND_SPHERE

    override val config =
        SceneConfig(backColor = Color.BLACK, camera = CameraConfig(zoomRanges = 10f..1000f, bound = Box2f(Vector2f(-2f), Vector2f(8f))))

    override val name: String = "Phong material"

    private val modelUI = Panel(
        Layout.VERTICAL,
        Label("Model:"),
        ToggleGroup(
            Layout.VERTICAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            ToggleButton("Green lorry", false)
            { if (isChecked) model = Model.LORRY_GREEN },
            ToggleButton("Green lorry + cargo", false)
            { if (isChecked) model = Model.LORRY_GREEN_CARGO },
            ToggleButton("Red lorry", false)
            { if (isChecked) model = Model.LORRY_RED },
            ToggleButton("Red lorry + cargo", false)
            { if (isChecked) model = Model.LORRY_RED_CARGO },
            ToggleButton("Sand sphere", true)
            { if (isChecked) model = Model.SAND_SPHERE },
        ),
    )

    init {
        lightEditorList.first().editor.heightSlider.valueListener.value = 0.1f
        materialEditor.specularShinesSlider.valueListener.value = 2f
        materialEditor.useNormalMap = true

        addElements(OpenableContainer("Model", modelUI))
    }

    override fun render(renderer: Renderer2D) {
        setupEnvironment(renderer)

        val font = FontReference("Serif", 24)
        val fontScale = 1f / RenderConfig.camera.zoom
        val baseSize = model.baseSize

        val material = materialEditor.buildMaterial(model) ?: return

        renderer.drawGraphic {
            var pivot = Vector2f(0f)
            val interval = Vector2f(2f, 1f)

            val positionQuad = Box2f.byDimension(pivot, baseSize)
            addRect(material, positionQuad, Box2f.ONE)

            val textureList = listOf(
                Pair("Diffuse", (material.diffuseSource as? TextureColorSource)?.value),
                Pair("Specular", (material.specularSource as? TextureColorSource)?.value),
                Pair("Shines", (material.shinesSource as? TextureVector1Source)?.value),
                Pair("Ambient", (material.ambientSource as? TextureColorSource)?.value),
                Pair("Normal", (material.normalSource as? TextureVector3Source)?.value),
                Pair("Opacity", (material.opacitySource as? TextureVector1Source)?.value),
            )

            pivot += baseSize.xAxis + interval.xAxis
            textureList.forEach { (name, texture) ->
                if (texture != null) {
                    addRect(SimpleMaterial(texture), Box2f.byDimension(pivot, baseSize), Box2f.ONE)
                    pivot += baseSize.yAxis

                    addText(name,  Matrix3f.scale(Vector2f(fontScale)) * Matrix3f.translate(pivot), font)
                    pivot += interval.yAxis
                }
            }

        }

        super.render(renderer)
    }

}