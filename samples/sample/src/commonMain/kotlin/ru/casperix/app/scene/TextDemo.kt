package ru.casperix.app.scene

import ru.casperix.demo_platform.AbstractDemo
import ru.casperix.demo_platform.config.CameraConfig
import ru.casperix.demo_platform.config.SceneConfig
import ru.casperix.demo_platform.renderer.RenderConfig
import ru.casperix.demo_platform.renderer.RenderHelper
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.collection.getLooped
import ru.casperix.math.color.Color
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.font.FontWeight
import ru.casperix.multiplatform.text.drawText
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.renderer.Renderer2D

class TextDemo : AbstractDemo {
    private val BASE_TEMPLATE = listOf(
        "Ре́ндеринг или отрисо́вка (англ. rendering — «визуализация») — термин в компьютерной графике, обозначающий процесс получения изображения по модели с помощью компьютерной программы.",
        "التقديم هو مصطلح في رسومات الكمبيوتر يشير إلى عملية الحصول على صورة من نموذج باستخدام برنامج كمبيوتر.",
        "Le rendu est un terme en infographie qui fait référence au processus d'obtention d'une image à partir d'un modèle à l'aide d'un programme informatique.",
        "渲染是計算機圖形學中的一個術語，指的是使用計算機程式從模型中獲取圖像的過程。",
    ).joinToString("\n")

    private val HARD_FONT = FontReference("Serif", 16)
    private val DEFAULT_FONT = FontReference("Serif", 24)
    private val FONTS = listOf(
        "Serif",
        "Arial",
        "Verdana",
        "Comic Sans MS",
    )
    private val DEMO_SIZE = Vector2f(1000f, 1000f)
    private val WEIGHTS = listOf(200, 400, 600, 800, 1000)
    private val SIZES = listOf(8, 16, 32, 48, 64)
    private val COLORS = listOf(Color.RED, Color.GREEN, Color.BLUE)

    private val LINE_BREAK = TextView("\n", DEFAULT_FONT, Color.WHITE)
    private val EXPERIMENTAL_BLOCKS = listOf(TextView(BASE_TEMPLATE, DEFAULT_FONT, Color.WHITE)) + LINE_BREAK +
            TextView("Use any available font:", DEFAULT_FONT, Color.WHITE) +
            FONTS.map {
                TextView("$it: ÀÇ", DEFAULT_FONT.copy(name = it), Color.WHITE)
            } + LINE_BREAK + SIZES.map {
        TextView("Font size: $it; ", DEFAULT_FONT.copy(size = it), Color.WHITE)
    } + LINE_BREAK + WEIGHTS.map {
        TextView("weight: $it; ", DEFAULT_FONT.copy(size = 32, weight = FontWeight(it)), Color.WHITE)
    } + LINE_BREAK + COLORS.map {
        TextView("Color: ${it.toHexString()}; ", DEFAULT_FONT, it)
    }

    private val HARD_BLOCKS_MAP = listOf(16, 32, 64, 128).map { maxU ->
        (0 until maxU).flatMap { y ->
            val color = Color.all.getLooped(y)
            (0 until maxU).map { x ->
                TextView("$x,$y; ", FontReference("Serif", 512 / maxU), color)
            }
        }
    }


    private var hardIndex: Int? = null
    private var showMetrics = true

    override val config = SceneConfig(
        camera = CameraConfig(
            initialZoom = 1f,
            initialPosition = DEMO_SIZE / 2f,
            zoomRanges = 1f..10f,
            bound = Box2f(Vector2f.ZERO, DEMO_SIZE)
        )
    )
    override val name: String = "Text"
    override val root = Container(
        Layout.VERTICAL,
        Panel(
            Layout.VERTICAL,
            ToggleButton("show metrics", showMetrics) { showMetrics = isChecked },
//            ToggleButton("round positions", TextRenderConfig.textRoundToPixel) { TextRenderConfig.textRoundToPixel = isChecked },

            ToggleGroup(
                Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                ToggleButton("sample", false) { if (isChecked) hardIndex = null },
                ToggleButton("16 lines", false) { if (isChecked) hardIndex = 0 },
                ToggleButton("32 lines", false) { if (isChecked) hardIndex = 1 },
                ToggleButton("64 lines", false) { if (isChecked) hardIndex = 2 },
                ToggleButton("128 lines", false) { if (isChecked) hardIndex = 3 },
            ),
        ),
    ).setSizeMode(SizeMode.view)

    override fun render(renderer: Renderer2D) {
        val hardIndex = hardIndex

        renderer.drawGraphic {
            addRect(Color.BLACK, Box2f(Vector2f.ZERO, DEMO_SIZE))
        }

        if (hardIndex != null) {
            val hardBlock = HARD_BLOCKS_MAP[hardIndex]
            renderer.drawText(
                hardBlock,
                Matrix3f.IDENTITY,
                DEMO_SIZE,
            )
        } else {
            renderer.drawText(
                EXPERIMENTAL_BLOCKS,
                Matrix3f.IDENTITY,
                DEMO_SIZE,
                showMetrics = showMetrics,
            )
        }

        if (RenderConfig.showAxis) {
            RenderHelper.renderAxisSystem(renderer)
        }
    }
}