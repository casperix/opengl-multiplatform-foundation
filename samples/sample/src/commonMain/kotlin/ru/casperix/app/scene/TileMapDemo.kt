package ru.casperix.app.scene

import ru.casperix.app.Assets
import ru.casperix.demo_platform.config.CameraConfig
import ru.casperix.demo_platform.config.SceneConfig
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.opengl.renderer.exp.TileMapMaterial
import ru.casperix.opengl.renderer.exp.TileSmooth
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.Texture2DArray

class TileMapDemo(renderer:OpenGlRenderer2d) : SceneDemo(renderer) {
    private var mipMapMap = MipMapMode.NONE
    private var smoothMode = TileSmooth.NONE
    private var gird = false

    override val config = SceneConfig(
        camera = CameraConfig(
            initialZoom = 100f,
            zoomRanges = 50f..500f,
            bound = Box2f(Vector2f.ZERO, Vector2f(16f))
        )
    )
    override val name: String = "TileMap"

    init {
        addElements(
            Container(
                Layout.HORIZONTAL,
                Label("Mipmap:"),
                ToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("None", true) { if (isChecked) mipMapMap = MipMapMode.NONE },
                    ToggleButton("Nearest", false) { if (isChecked) mipMapMap = MipMapMode.NEAREST },
                    ToggleButton("Linear", false) { if (isChecked) mipMapMap = MipMapMode.LINEAR },
                )
            ).setSizeMode(SizeMode.content),
            Container(
                Layout.HORIZONTAL,
                Label("Smooth:"),
                ToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("None", true) { if (isChecked) smoothMode = TileSmooth.NONE },
                    ToggleButton("Linear", false) { if (isChecked) smoothMode = TileSmooth.LINEAR },
                    ToggleButton("Smoothest", false) { if (isChecked) smoothMode = TileSmooth.SMOOTHEST },
                )
            ).setSizeMode(SizeMode.content),
            ToggleButton("Gird", gird) { gird = isChecked },
        )
    }

    enum class MipMapMode {
        NONE,
        LINEAR,
        NEAREST,
    }

    override fun render(renderer: Renderer2D) {
        setupEnvironment(renderer)

        val textures = when (mipMapMap) {
            MipMapMode.NONE -> Assets.getTexturesNoMipMap()
            MipMapMode.LINEAR -> Assets.getTexturesLinearMipMap()
            MipMapMode.NEAREST -> Assets.getTexturesNearestMipMap()
        } ?: return

        val position1Quad = Box2f.byDimension(Vector2f.ZERO, Assets.tile_map.map.dimension.toVector2f())

        drawMap(
            renderer,
            position1Quad,
            Assets.tile_map,
            textures,
            smoothMode,
            gird,
        )

        super.render(renderer)
    }

    private fun drawMap(
        renderer: Renderer2D,
        quad: Box2f,
        tileMap: Texture2D,
        textures: Texture2DArray,
        smooth: TileSmooth,
        gird: Boolean,
    ) {
        renderer.drawGraphic {
            addRect(Color.GRAY, quad.grow(1f), Box2f.ONE)
            addRect(TileMapMaterial(tileMap, textures, smooth, Vector2f(1f), gird), quad, Box2f.ONE)
        }

    }

}