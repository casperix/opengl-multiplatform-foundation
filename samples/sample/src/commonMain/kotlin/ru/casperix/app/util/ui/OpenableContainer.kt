package ru.casperix.app.util.ui

import ru.casperix.light_ui.component.button.ButtonLogic
import ru.casperix.light_ui.component.button.ButtonState
import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.AbstractElement
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.math.vector.float32.Vector2f

class OpenableContainer(titleValue: String, val content: Element) :
    AbstractElement(SizeMode.layout),
    AbstractContainer {
    override val layout = Layout.VERTICAL
    private val skin = SkinProvider.skin.checkButton.copy(defaultSize = Vector2f(300f, 40f))
    private val openButton = CheckBox(titleValue, false, skin) { setOpen(isChecked) }

    val isOpened get() = openButton.isChecked

    override val input = ButtonLogic(ButtonState())

    override var children = listOf(Node(openButton))

    init {
        setOpen(isOpened)
    }


    fun setOpen(value: Boolean) {
        openButton.isChecked = value

        val summaryChild = if (value) {
            listOf(openButton, content)
        } else {
            listOf(openButton)
        }

        children = listOf(
            Node(
                Panel(Layout.VERTICAL, summaryChild.wrapNodes())
            )
        )
    }


}

