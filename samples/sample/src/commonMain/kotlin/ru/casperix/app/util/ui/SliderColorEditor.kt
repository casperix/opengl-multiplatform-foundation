package ru.casperix.app.util.ui

import ru.casperix.light_ui.component.image.Image
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.toPrecision
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder
import ru.casperix.signals.concrete.StorageSignal

class SliderColorEditor(
    val title: String,
    val skin: SliderValueEditorSkin = SliderValueEditorSkin(),
) {
    private val sampleSize = Vector2f(80f, 40f)
    private val sliderR = SliderValueEditor("R")
    private val sliderG = SliderValueEditor("G")
    private val sliderB = SliderValueEditor("B")
    private val info = Label("")
    private val sample = Image().setSizeMode(SizeMode.fixed(sampleSize))

    val valueListener = StorageSignal(Color.BLACK)
    val root = Panel(
        Layout.VERTICAL,
        Container(
            OrientationLayout(Orientation.HORIZONTAL, AlignMode.RIGHT_TOP),
            info,
            sample,
        ).setSizeMode(SizeMode.row),
        sliderR.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
        sliderG.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
        sliderB.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
    ).setSizeMode(SizeMode.content)

    private var selfChanged = false

    init {
        sliderR.valueListener.then {
            updateValue()
        }
        sliderG.valueListener.then {
            updateValue()
        }
        sliderB.valueListener.then {
            updateValue()
        }
        valueListener.then {
            selfChanged = true
            valueListener.value.apply {
                sliderR.valueListener.value = red
                sliderG.valueListener.value = green
                sliderB.valueListener.value = blue
            }
            selfChanged = false

            updateInfo()
        }

        updateInfo()
    }

    private fun updateValue() {
        if (selfChanged) return
        valueListener.value =
            RgbColor3f(sliderR.valueListener.value, sliderG.valueListener.value, sliderB.valueListener.value)
    }

    private fun RgbColor3f.formatColor() = listOf(
        red.toPrecision(2),
        green.toPrecision(2),
        blue.toPrecision(2),
    ).joinToString("; ", "(", ")")

    private fun updateInfo() {

        val color = valueListener.value

        sample.setGraphic(VectorGraphicBuilder.build {
            addRect(color, Box2f(Vector2f.ZERO, sampleSize))
        }, sampleSize)

        info.text = "$title:"// + color.formatColor()
    }

}