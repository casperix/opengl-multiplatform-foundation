package ru.casperix.app.scene

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.component.FullScreenSupport
import ru.casperix.demo_platform.AbstractDemo
import ru.casperix.demo_platform.config.SceneConfig
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.Empty
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Renderer2D

class DisplayDemo(val surface: Surface, val glRenderer: OpenGlRenderer2d) : AbstractDemo {
    override val config = SceneConfig()
    private val info = Label()
    override val name: String = "Display"
    override val root = Container(
        Layout.VERTICAL,
        Panel(
            Layout.VERTICAL,
            Container(
                Layout.HORIZONTAL,
                listOf(
                    if (surface is FullScreenSupport) {
                        ToggleButton("Full screen", false) { (surface as? FullScreenSupport)?.setFullScreen(isChecked) }
                    } else {
                        Label("Full screen not supported")
                    },
                    ToggleButton("VSync", surface.getVerticalSync()) { surface.setVerticalSync(isChecked) },
                    ToggleButton("Continuous", PlatformMisc.continuousRendering) {
                        PlatformMisc.continuousRendering = isChecked
                    },
                ).wrapNodes()
            ),
            info.setSizeMode(SizeMode.row),
        ).setSizeMode(SizeMode.row),
        Empty().setSizeMode(SizeMode.view),
    ).setSizeMode(SizeMode.view)

    override fun render(renderer: Renderer2D) {
        surface.getDisplay().apply {
            info.text = listOf(
                "size: " + resolution.width + "x" + resolution.height,
                "BPP: $bitsPerPixel",
                "Hz: $frequency",
                "DPI: $densityPerInch",
            ).joinToString("\n")
        }

        glRenderer.statistic.nextFrame()
    }
}

