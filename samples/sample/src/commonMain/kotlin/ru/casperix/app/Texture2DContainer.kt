package ru.casperix.app

import ru.casperix.multiplatform.loader.ResourceLoadError
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.pixel_map.Rgba8PixelMap
import ru.casperix.signals.concrete.EitherFuture

class Texture2DContainer(val future: EitherFuture<PixelMap, ResourceLoadError>, val config: TextureConfig = TextureConfig()) {
    private var result: Texture2D? = null
    private var error: ResourceLoadError? = null

    constructor(path:String, config: TextureConfig = TextureConfig()) : this(resourceLoader.loadImage(path), config)

    init {
        future.then({
            result = Texture2D(it, config)
        }, {
            println(it)
            error = it
        })
    }

    fun getOrNull(): Texture2D? {
        return result
    }

    fun getOrBlank(): Texture2D {
        return result ?: BLANK
    }

    companion object {
        val BLANK = Texture2D(Rgba8PixelMap(1, 1))
    }
}