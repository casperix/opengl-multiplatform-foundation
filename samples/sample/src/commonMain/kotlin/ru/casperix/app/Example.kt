package ru.casperix.app

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Renderer2D

object Example {
    fun simple() {
        //  initialize renderer
        val renderer: Renderer2D = OpenGlRenderer2d()

        //  configure renderer
        val viewport = Dimension2i(500, 500)
        renderer.environment.apply {
            clearColor = Color.NAVY.toRGBA()
            setOrthographicCamera(viewport)
        }

        //  draw something
        renderer.clear()
        renderer.drawGraphic {
            addRect(Color.RED, Box2f.byDimension(0f, 0f, 100f, 100f))
        }

        renderer.flush()
    }
}