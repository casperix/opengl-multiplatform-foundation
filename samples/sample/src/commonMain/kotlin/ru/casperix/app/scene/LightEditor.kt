package ru.casperix.app.scene

import ru.casperix.app.util.ui.SliderColorEditor
import ru.casperix.app.util.ui.SliderValueEditor
import ru.casperix.demo_platform.point.Point
import ru.casperix.demo_platform.point.PointController
import ru.casperix.demo_platform.shape_editor.AbstractPointEditor
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.Disposable
import ru.casperix.renderer.light.Light
import ru.casperix.renderer.light.LightColors
import ru.casperix.renderer.light.PointLight

class LightEditor(pointController: PointController) : AbstractPointEditor(pointController), Disposable {
    val ambientSlider = SliderColorEditor("Ambient")
    val diffuseSlider = SliderColorEditor("Diffuse")
    val specularSlider = SliderColorEditor("Specular")
    val heightSlider = SliderValueEditor("Height", -20f..20f)

    val lightPoint = Point(Vector2f.ZERO)

    val root = Panel(
        Layout.VERTICAL,
        ambientSlider.root,
        diffuseSlider.root,
        specularSlider.root,
        heightSlider.root,
    ).setSizeMode(SizeMode.content)

    init {
        selfPoints += lightPoint

        ambientSlider.valueListener.value = RgbColor3f(0.1f)
        diffuseSlider.valueListener.value = RgbColor3f(0.5f)
        specularSlider.valueListener.value = RgbColor3f(1f)
        heightSlider.valueListener.value = 3f
    }

    fun buildLight(): Light {
        val lightAmbientColor = ambientSlider.valueListener.value
        val lightDiffuseColor = diffuseSlider.valueListener.value
        val lightSpecularColor = specularSlider.valueListener.value
        val lightHeight = heightSlider.valueListener.value
        val lightPosition = lightPoint.position.expand(lightHeight)

        return PointLight.DEFAULT.copy(
            position = lightPosition,
            colors = LightColors(
                lightAmbientColor,
                lightDiffuseColor,
                lightSpecularColor
            ),
        )
    }
}