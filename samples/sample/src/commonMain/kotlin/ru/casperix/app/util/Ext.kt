package ru.casperix.app.util

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.vector.float32.Vector2f


fun Polygon2f.rotate(angle: DegreeFloat): Polygon2f {
    return convert {
        it.rotate(angle)
    }
}
fun Polygon2f.rotate(angle: RadianFloat): Polygon2f {
    return convert {
        it.rotate(angle)
    }
}

fun Polygon2f.translate(offset: Vector2f): Polygon2f {
    return convert { it + offset }
}

fun Polygon2f.scale(value: Vector2f): Polygon2f {
    return convert { it* value }
}

fun Polygon2f.scale(x: Float, y:Float): Polygon2f {
    return scale(Vector2f(x, y))
}

fun Polygon2f.toQuadOrNull():Quad2f? {
    if (this is Quad2f) return this
    val vertices = getVertices()
    if (vertices.size == 4) {
        return Quad2f(vertices[0], vertices[1], vertices[2], vertices[3])
    }
    return null
}