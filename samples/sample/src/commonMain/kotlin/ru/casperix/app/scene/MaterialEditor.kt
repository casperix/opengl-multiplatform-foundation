package ru.casperix.app.scene

import ru.casperix.app.util.ui.SliderColorEditor
import ru.casperix.app.util.ui.SliderValueEditor
import ru.casperix.app.util.ui.SliderVector3Editor
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.renderer.material.*

class MaterialEditor {
    val diffuseColorSlider = SliderColorEditor("Diffuse")
    val ambientColorSlider = SliderColorEditor("Ambient")
    val opacitySlider = SliderValueEditor("Opacity", 0f..1f)
    val normalSlider = SliderVector3Editor("Normal", componentRange = -1f..1f)
    val specularColorSlider = SliderColorEditor("Specular")
    val specularShinesSlider = SliderValueEditor("Shines", 1f..256f)

    var useDiffuseMap = false
    var useAmbientMap = false
    var useSpecularMap = false
    var useShinesMap = false
    var useNormalMap = false
    var useOpacityMap = false

    val root = Container(
        Layout.VERTICAL,
        Panel(
            Layout.HORIZONTAL,
            ToggleButton("Diffuse", useDiffuseMap) { useDiffuseMap = isChecked },
            Container(
                Layout.VERTICAL,
                diffuseColorSlider.root,
            )
        ),
        Panel(
            Layout.HORIZONTAL,
            ToggleButton("Ambient", useAmbientMap) { useAmbientMap = isChecked },
            Container(
                Layout.VERTICAL,
                ambientColorSlider.root,
            )
        ),
        Panel(
            Layout.HORIZONTAL,
            ToggleButton("Normal", useNormalMap) { useNormalMap = isChecked },
            Container(
                Layout.VERTICAL,
                normalSlider.root,
            )
        ),
        Panel(
            Layout.HORIZONTAL,
            ToggleButton("Opacity", useOpacityMap) { useOpacityMap = isChecked },
            Container(
                Layout.VERTICAL,
                opacitySlider.root,
            )
        ),
        Panel(
            Layout.HORIZONTAL,
            ToggleButton("Specular", useSpecularMap) { useSpecularMap = isChecked },
            Container(
                Layout.VERTICAL,
                specularColorSlider.root,
            )
        ),
        Panel(
            Layout.HORIZONTAL,
            ToggleButton("Shines", useShinesMap) { useShinesMap = isChecked },
            Container(
                Layout.VERTICAL,
                specularShinesSlider.root,
            )
        ),

        ).setSizeMode(SizeMode.content)

    init {
        ambientColorSlider.valueListener.value = RgbColor3f(0.2f)
        diffuseColorSlider.valueListener.value = RgbColor3f(1f, 0f, 0f)
        opacitySlider.valueListener.value = 0.9f
        normalSlider.valueListener.value = Vector3f.Z
        specularColorSlider.valueListener.value = RgbColor3f(0.5f)
        specularShinesSlider.valueListener.value = 32f
    }

    fun buildMaterial(info: Model): PhongMaterial? {

        val specularConst = specularColorSlider.valueListener.value
        val shinesConst = specularShinesSlider.valueListener.value
        val opacityConst = opacitySlider.valueListener.value
        val diffuseConst = diffuseColorSlider.valueListener.value
        val ambientConst = ambientColorSlider.valueListener.value
        val normalConst = normalSlider.valueListener.value

        val ambient = if (useAmbientMap) info.ambient.getOrNull() ?: return null else null
        val diffuse = if (useDiffuseMap) info.albedo.getOrNull() ?: return null else null
        val normal = if (useNormalMap) info.normal.getOrNull() ?: return null else null
        val opacity = if (useOpacityMap) info.opacity.getOrNull() ?: return null else null
        val specular = if (useSpecularMap) info.specular.getOrNull() ?: return null else null
        val shines = if (useShinesMap) info.shines.getOrNull() ?: return null else null

        val ambientSource = ambient?.run { TextureColorSource(this) } ?: ConstantColorSource(ambientConst)
        val diffuseSource = diffuse?.run { TextureColorSource(this) } ?: ConstantColorSource(diffuseConst)
        val normalSource = normal?.run { TextureVector3Source(this) }?: ConstantVector3Source(normalConst)
        val opacitySource = opacity?.run { TextureVector1Source(this, 3) } ?: ConstantVector1Source(opacityConst)

        val specularSource = specular?.run { TextureColorSource(this) } ?: ConstantColorSource(specularConst)
        val shinesSource = shines?.run { TextureVector1Source(this, 3) } ?: ConstantVector1Source(shinesConst)

        return PhongMaterial(diffuseSource, specularSource, shinesSource, ambientSource, normalSource, opacitySource)
    }
}