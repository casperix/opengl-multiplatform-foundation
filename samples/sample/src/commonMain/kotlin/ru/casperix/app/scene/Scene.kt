package ru.casperix.app.scene

import ru.casperix.light_ui.element.Element

interface Scene {
    val name: String
    val ui: Element
}