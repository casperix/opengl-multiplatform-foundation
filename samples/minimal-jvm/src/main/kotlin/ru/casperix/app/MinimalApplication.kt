package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.curve.float32.BezierCubic2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.toFloat
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.font.FontWeight
import ru.casperix.multiplatform.text.drawText
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import kotlin.time.Duration
import kotlin.time.DurationUnit

class MinimalApplication(private val surface: Surface) : SurfaceListener {
    //  initialize renderer
    private val renderer:Renderer2D = OpenGlRenderer2d()

    private var angle = DegreeFloat(0f)

    override fun nextFrame(tick: Duration) {
        val dimension = surface.getSize()

        //  configure environment
        renderer.environment.apply {
            setOrthographicCamera(dimension, true)
        }

        //  draw something
        renderer.clear()

        renderer.drawGraphic {
            addRoundRect(SimpleMaterial(Color.BLACK), Box2f.byRadius(0f, 0f, 200f, 200f), 50f)
            addCurve(
                SimpleMaterial(Color.WHITE),
                BezierCubic2f(
                    Vector2f(-200f, -150f),
                    Vector2f(-200f, 500f),
                    Vector2f(200f, -100f),
                    Vector2f(200f, 150f),
                )
            )
        }

        renderer.drawText(
            listOf(
                TextView("casperix\n", FontReference("Serif", 32, FontWeight.BOLD), Color.RED),
                TextView("OpenGl\n", FontReference("Serif", 38), Color.GREEN),
            ), Matrix3f.translate(-50f, -150f)
        )

        renderer.drawText(
            listOf(
                TextView("Multiplatform\n", FontReference("Serif", 64), Color.BLUE),
            ),
            Matrix3f.translate(
                Vector2f(
                    -180f,
                    -60f
                )
            ) * Matrix3f.scale(Vector2f(0.5f)) * Matrix3f.rotate(angle) * Matrix3f.translate(Vector2f(-90f, -20f))
        )

        renderer.flush()

        angle += DegreeFloat(90f) * tick.toFloat(DurationUnit.SECONDS)
    }

    override fun dispose() {

    }

    override fun input(event: InputEvent) {

    }
}