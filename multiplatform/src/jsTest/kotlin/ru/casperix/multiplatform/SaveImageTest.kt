package ru.casperix.multiplatform

import ru.casperix.math.color.Colors
import ru.casperix.math.color.uint8.Color4b
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.renderer.pixel_map.PixelMap
import kotlin.test.Test

class SaveImageTest {
    @Test
    fun test() {
        val image = PixelMap.RGBA(256, 32)
        image.RGBA()?.apply {
            (0 until dimension.x).forEach { x ->
                (0 until 4).forEach { y ->
                    set(x, y, Color4b(x.toUByte(), 0u, 0u, 255u))
                }
                (4 until 8).forEach { y ->
                    set(x, y, Colors.RED.toColor4b())
                }
                (8 until 12).forEach { y ->
                    set(x, y, Colors.GREEN.toColor4b())
                }
                (12 until 16).forEach { y ->
                    set(x, y, Colors.BLUE.toColor4b())
                }
                (16 until 20).forEach { y ->
                    set(x, y, Color4b(0u, 0u, 0u, 0u))
                }
            }
        }

        resourceLoader.saveImage("test.png", image)
    }

}

