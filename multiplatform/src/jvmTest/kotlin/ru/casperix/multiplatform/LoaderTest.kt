package ru.casperix.multiplatform

import ru.casperix.multiplatform.loader.resourceLoader
import kotlin.test.Test
import kotlin.test.assertEquals

class LoaderTest {
    @Test
    fun loadSome() {
        resourceLoader.loadText("some/test.txt").then({
            assertEquals("text content", it)
        }, {
            println(it)
        })

        resourceLoader.loadText("some/invalid.txt").then({

        }, {
            println(it)
        })
    }
}