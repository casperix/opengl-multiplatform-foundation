package ru.casperix.multiplatform

import ru.casperix.multiplatform.text.impl.TextProcessor
import java.util.*
import java.util.Locale.LanguageRange
import kotlin.test.Test

class Test {
    private val TEST = listOf(
        "Aaaa1 aa2    a3   a4\t",
        " ",
        " aaa",
        "aaa ",
        "aaa",
        "a\nb",
        "a\rb",
        "a\r\nb",
    )

    @Test
    fun other() {
        TEST.forEach {
            println("Text: $it")
            TextProcessor.splitByGroups(it).forEachIndexed { index, s ->

                val p = s.replace(' ', '.').replace('\t', '_').replace("\r\n", "N").replace("\r", "N").replace("\n", "N")
                println("$index. $p")
            }
        }
    }



    @Test
    fun tst() {
        val range = LanguageRange("zh-Hant")
        val t = range.range

        't'.category
        Locale.SIMPLIFIED_CHINESE
        val locale = Locale("zh-Hant")
        println(locale)
    }
}