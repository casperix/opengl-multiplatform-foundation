package ru.casperix.multiplatform.loader

import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.int32.Vector3i
import ru.casperix.multiplatform.put
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.pixel_map.Rgb8PixelMap
import ru.casperix.renderer.pixel_map.Rgba8PixelMap
import java.awt.AlphaComposite
import java.awt.Transparency
import java.awt.color.ColorSpace
import java.awt.image.*

object JvmImageFactory {
    /**
     *  Actually support only two format
     *  - RGB
     *  - RGBA
     *
     *  And RGBA can pre-multiplied (or not)
     */
    data class ImageFormat(val hasAlpha: Boolean, val isAlphaPremultiplied: Boolean)

    /**
     *
     *  @see <a href="https://stackoverflow.com/questions/65569243/getting-a-rgba-byte-array-from-a-bufferedimage-java">stackoverflow</a>
     */
    fun createPixelMapByImage(image: BufferedImage, name: String, format: ImageFormat): PixelMap {
        val compatibleImage = if (compatibleFormat(image, format)) {
            image
        } else {
            // Draw the image onto the RGBA buffer, which will be updated immediately
            val imageRGBA = createImage(image.getDimension(), format)
            val g = imageRGBA.createGraphics()
            try {
                g.composite = AlphaComposite.Src
                g.drawImage(image, 0, 0, null)
            } finally {
                g.dispose()
            }
            imageRGBA
        }

        val raw = compatibleImage.rawBytes()

        return if (format.hasAlpha) {
            Rgba8PixelMap(UByteArray3D(raw.toUByteArray(), Vector3i(image.width, image.height, 4)), name)
        } else {
            Rgb8PixelMap(UByteArray3D(raw.toUByteArray(), Vector3i(image.width, image.height, 3)), name)
        }
    }


    fun createImage(size: Dimension2i, format: ImageFormat): BufferedImage {
        val colorModel = createColorModel(format)
        val raster = createRaster(format, size)
        return BufferedImage(colorModel, raster, colorModel.isAlphaPremultiplied, null)
    }

    fun createImageByPixelMap(pixelMap: PixelMap, format: ImageFormat): BufferedImage {
        val image = createImage(pixelMap.dimension.toDimension2i(), format)
        val imageBytes = image.rawBytes()
        if (imageBytes.size != pixelMap.byteArray.size) {
            throw Exception("Invalid PixelMap size")
        }
        imageBytes.put(pixelMap.byteArray.data.toByteArray(), 0)
        return image
    }


    private fun createColorModel(format: ImageFormat) = format.run {
        ComponentColorModel(
            ColorSpace.getInstance(ColorSpace.CS_sRGB),
            hasAlpha,
            isAlphaPremultiplied,
            if (hasAlpha) Transparency.TRANSLUCENT else Transparency.OPAQUE,
            DataBuffer.TYPE_BYTE
        )
    }

    private fun createRaster(format: ImageFormat, size: Dimension2i) = format.run {
        val channelsAmount = if (format.hasAlpha) 4 else 3
        val channelOffsets = if (format.hasAlpha) intArrayOf(0, 1, 2, 3) else intArrayOf(0, 1, 2)
        Raster.createInterleavedRaster(
            DataBuffer.TYPE_BYTE,
            size.width,
            size.height,
            size.width * channelsAmount,
            channelsAmount,
            channelOffsets,
            null
        ) // R, G, B, A order
    }

    private fun compatibleFormat(image: BufferedImage, format: ImageFormat): Boolean {
        return image.colorModel == createColorModel(format) && compatibleRaster(
            image.raster,
            format,
            image.getDimension()
        )
    }

    private fun compatibleRaster(actual: Raster, expectedFormat: ImageFormat, expectedSize: Dimension2i): Boolean {
        val channelsAmount = if (expectedFormat.hasAlpha) 4 else 3
        val channelOffsets = if (expectedFormat.hasAlpha) intArrayOf(0, 1, 2, 3) else intArrayOf(0, 1, 2)

//        if (actual.javaClass != ByteInterleavedRaster::class.java) return false
        if (actual.dataBuffer.dataType != DataBuffer.TYPE_BYTE) return false
        if (actual.width != expectedSize.width) return false
        if (actual.height != expectedSize.height) return false
        if (actual.numBands != channelsAmount) return false
//        if (!actual.dataBuffer.offsets.contentEquals(channelOffsets)) return false

        return true
    }

    fun BufferedImage.rawBytesOrNull(): ByteArray? {
        (raster.dataBuffer as? DataBufferByte)?.let {
            return it.data
        }
        return null
    }

    fun BufferedImage.rawBytes(): ByteArray {
        return rawBytesOrNull() ?: throw Exception("Can't get bytes from ${raster.dataBuffer.javaClass.simpleName}")
    }

    fun BufferedImage.getDimension() = Dimension2i(width, height)
}