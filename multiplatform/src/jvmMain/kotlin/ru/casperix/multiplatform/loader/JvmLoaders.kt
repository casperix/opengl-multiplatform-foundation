package ru.casperix.multiplatform.loader

import ru.casperix.multiplatform.loader.JvmImageFactory.createPixelMapByImage
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.pixel_map.codec.RGBACodec
import ru.casperix.renderer.pixel_map.codec.RGBCodec
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal
import java.io.File
import javax.imageio.ImageIO

actual val resourceLoader: AbstractResourceLoader = JvmLoaders()

class JvmLoaders : CachedResourceLoader() {

    override fun saveImage(path: String, pixelMap: PixelMap): ResourceSaveError? = runCatching {
        val isRGBA = pixelMap.pixelCodec == RGBACodec
        val isRGB = pixelMap.pixelCodec != RGBCodec
        if (!isRGB && !isRGBA) throw Exception("Support only RGB & RGBA. Actual: ${pixelMap.pixelCodec}")

        val format = JvmImageFactory.ImageFormat(isRGBA, false)
        val image = JvmImageFactory.createImageByPixelMap(pixelMap, format)
        ImageIO.write(image, "png", createFile(path))
    }.exceptionOrNull()?.let {
        ResourceCustomSaveError(it.message ?: "")
    }


    private fun createFile(path: String): File {
        val file = File(path)
        file.parentFile?.mkdirs()
        return file
    }

    override fun saveText(path: String, data: String): EitherFuture<Unit, ResourceSaveError> {
        runCatching {
            createFile(path).writeText(data)
        }.exceptionOrNull()?.let {
            return EitherFuture.reject(ResourceCustomSaveError(it.message ?: ""))
        }

        return EitherFuture.accept(Unit)
    }

    override fun saveBytes(path: String, data: ByteArray): EitherFuture<Unit, ResourceSaveError> {
        runCatching {
            createFile(path).writeBytes(data)
        }.exceptionOrNull()?.let {
            return EitherFuture.reject(ResourceCustomSaveError(it.message ?: ""))
        }

        return EitherFuture.accept(Unit)
    }

    override fun loadBytesDirect(path: String): EitherFuture<ByteArray, ResourceLoadError> {
        val loader = EitherSignal<ByteArray, ResourceLoadError>()
        val classloader = Thread.currentThread().contextClassLoader
        val stream = classloader.getResourceAsStream(path)

        loader.apply {
            if (stream == null) {
                reject(ResourceNotFoundLoadError(path))
            } else {
                accept(stream.readBytes())
            }
        }
        return loader
    }

    override fun loadImageDirect(path: String): EitherFuture<PixelMap, ResourceLoadError> {
        val loader = EitherSignal<PixelMap, ResourceLoadError>()
        try {
            val classloader = Thread.currentThread().contextClassLoader
            val url = classloader.getResource(path)
            if (url === null) {
                loader.reject(ResourceNotFoundLoadError(path))
                return loader
            }
            val image = ImageIO.read(url)
            val hasAlpha= image.colorModel.hasAlpha()

            val pixelMap = createPixelMapByImage(image, path, JvmImageFactory.ImageFormat(hasAlpha, true))

            loader.accept(pixelMap)
        } catch (e: Throwable) {
            loader.reject(ResourceCustomLoadError("Unknown error for $path:" + e.message + ""))
        }
        return loader
    }
}

