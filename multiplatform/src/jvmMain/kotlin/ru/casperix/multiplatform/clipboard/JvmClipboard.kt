package ru.casperix.multiplatform.clipboard

import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.*
import java.awt.Toolkit
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.StringSelection


actual val clipboard: ClipboardApi = JvmClipboard()

class JvmClipboard : ClipboardApi {
    private val awtClipboard = Toolkit.getDefaultToolkit().systemClipboard

    override fun pushString(value: String): EitherFuture<Unit, Throwable> {
        val selection = StringSelection(value)
        awtClipboard.setContents(selection, selection)
        return EitherSignal<Unit, Throwable>().apply {
            accept(Unit)
        }
    }

    override fun popString(): EitherFuture<String, Throwable> {
        return EitherSignal<String, Throwable>().apply {
            val contents = awtClipboard.getContents(null)
            if (contents == null) {
                reject(Exception("No content in clipboard"))
            } else if (!contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                reject(Exception("No string in clipboard"))
            } else {
                val data = contents.getTransferData(DataFlavor.stringFlavor) as String
                accept(data)
            }
        }
    }
}

