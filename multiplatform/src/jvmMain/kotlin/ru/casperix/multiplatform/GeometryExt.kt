package ru.casperix.multiplatform

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import java.awt.geom.Point2D
import java.awt.geom.Rectangle2D

fun Rectangle2D.toBox2f(): Box2f {
    return Box2f(Vector2f(minX.toFloat(), minY.toFloat()), Vector2f(maxX.toFloat(), maxY.toFloat()))
}

fun Point2D.toVector2f():Vector2f {
    return Vector2f(x.toFloat(), y.toFloat())
}