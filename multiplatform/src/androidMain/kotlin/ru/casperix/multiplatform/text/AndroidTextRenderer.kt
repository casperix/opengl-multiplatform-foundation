package ru.casperix.multiplatform.text

import ru.casperix.multiplatform.text.impl.CacheBasedTextRenderer

actual val textRenderer: TextRendererApi = CacheBasedTextRenderer(AndroidTextGraphicProcessor)