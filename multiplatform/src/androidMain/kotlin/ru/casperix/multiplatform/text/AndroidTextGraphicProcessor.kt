package ru.casperix.multiplatform.text

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Typeface
import android.text.TextPaint
import ru.casperix.math.axis_aligned.dimensionOf
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.ceilToInt
import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.pixel_map.BitmapConverter
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder


object AndroidTextGraphicProcessor : TextGraphicProcessor {
    override fun create(builder: VectorGraphicBuilder, scheme: TextScheme, transform: Matrix3f): Boolean {
        val value = scheme.summaryArea.value
        val minX = value.min.x
        val minY = value.min.y
        val maxX = value.max.x
        val maxY = value.max.y

        val imageWidth = (maxX - minX).ceilToInt()
        val imageHeight = (maxY - minY).ceilToInt()

        if (imageWidth == 0 || imageHeight == 0) {
            return false
        }

        val bitmap = Bitmap.createBitmap(imageWidth, imageHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)

        scheme.elements.forEach {
            val paint = getTextPaint(it.font, it.foreground)

            val position = Vector2f(
                it.textArea.min.x - minX,
                it.textArea.min.y - minY - paint.fontMetrics.ascent
            ).run { if (TextRenderConfig.textRoundToPixel) round() else this }

            canvas.drawText(it.part.text, position.x, position.y, paint)
        }

        val pixelMap = BitmapConverter.bitmapToPixelMap(bitmap, "")

        val area = Box2f.byDimension(Vector2f(minX, minY), dimensionOf(imageWidth, imageHeight).toVector2f())
        builder.addRect(SimpleMaterial(Texture2D(pixelMap, TextureConfig(useMipMap = false))), area, Box2f.ONE, transform)
        return true
    }

    override fun getStringMetrics(font: FontReference, line: String): StringMetrics {
        val paint = getTextPaint(font)
        return StringMetrics.calculate(line, {
            paint.measureText(line, it, it + 1)
        }, -paint.fontMetrics.ascent + paint.fontMetrics.descent)
    }

    override fun getFontMetrics(font: FontReference): FontMetrics {
        val metrics = getTextPaint(font, Color.WHITE).fontMetrics
        if (metrics.ascent >= 0f || metrics.top >= 0f || metrics.descent <= 0f || metrics.bottom <= 0f) {
            throw Exception("Invalid font metrics: $metrics")
        }
        return FontMetrics(-metrics.ascent, metrics.descent, metrics.leading)
    }

    private fun getTextPaint(reference: FontReference, customColor: Color? = null): TextPaint {
        val paint = TextPaint()
        paint.typeface = Typeface.SERIF
        paint.textSize = reference.size.toFloat()
        paint.isAntiAlias = true
        if (customColor != null) {
            paint.color = customColor.toRGBA().toColor1i().value.toInt()
        }
        return paint
    }


}