package ru.casperix.multiplatform.clipboard

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal

actual val clipboard: ClipboardApi = AndroidClipboard


object AndroidClipboard : ClipboardApi {
    private var clipboard: ClipboardManager? = null
    private val error = Exception("Use androidSurfaceLauncher or call AndroidClipboard.setup firstly")

    fun setup(activity: Activity) {
        clipboard = activity.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    override fun pushString(value: String): EitherFuture<Unit, Throwable> {
        val future = EitherSignal<Unit, Throwable>()
        val clipboard = clipboard

        if (clipboard == null) {
            future.reject(error)
        } else {
            clipboard.setPrimaryClip(ClipData.newPlainText("label", value))
            future.accept(Unit)
        }
        return future
    }

    override fun popString(): EitherFuture<String, Throwable> {
        val future = EitherSignal<String, Throwable>()
        val clipboard = clipboard

        if (clipboard == null) {
            future.reject(error)
        } else {
            val clip = clipboard.primaryClip
            if (clip == null || clip.itemCount == 0) {
                future.reject(Exception("No content in clipboard"))
            } else {
                val first = clip.getItemAt(0)
                if (first.text == null) {
                    future.reject(Exception("No text in clipboard"))
                } else {
                    future.accept(first.text.toString())
                }
            }
        }

        return future
    }
}


