package ru.casperix.multiplatform.util

import android.graphics.Rect
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i


fun Rect.toBox2i(): Box2i {
    return Box2i(Vector2i(left, top), Vector2i(right, bottom))
}