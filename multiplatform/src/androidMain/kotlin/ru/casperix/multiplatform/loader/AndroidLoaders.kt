package ru.casperix.multiplatform.loader

import android.graphics.BitmapFactory
import ru.casperix.multiplatform.pixel_map.BitmapConverter
import ru.casperix.misc.Left
import ru.casperix.misc.Right
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal
import ru.casperix.signals.concrete.mapAccept


actual val resourceLoader: AbstractResourceLoader = AndroidLoaders()

class AndroidLoaders : CachedResourceLoader() {
    override fun loadBytesDirect(path: String): EitherFuture<ByteArray, ResourceLoadError> {
        val loader = EitherSignal<ByteArray, ResourceLoadError>()
        val classloader = Thread.currentThread().getContextClassLoader()
        val stream = classloader.getResourceAsStream("assets/$path")

        loader.apply {
            if (stream == null) {
                reject(ResourceNotFoundLoadError(path))
            } else {
                accept(stream.readBytes())
            }
        }
        return loader
    }

    override fun saveImage(path: String, pixelMap: PixelMap): ResourceSaveError? {
        return ResourceCustomSaveError("Android not support image save now")
    }

    override fun saveText(path: String, data: String): EitherFuture<Unit, ResourceSaveError> {
        return EitherFuture.reject(ResourceCustomSaveError("Android not support save data"))
    }

    override fun saveBytes(path: String, data: ByteArray): EitherFuture<Unit, ResourceSaveError> {
        return EitherFuture.reject(ResourceCustomSaveError("Android not support save data"))
    }

    override fun loadImageDirect(path: String): EitherFuture<PixelMap, ResourceLoadError> {
        return loadBytes(path).mapAccept { bytes ->
            try {
                val image = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                Right(BitmapConverter.bitmapToPixelMap(image, path))
            } catch (e: Exception) {
                println("Can't load png: ${e.message}")
                Left(ResourceCustomLoadError(e.message ?: ""))
            }
        }
    }
}

