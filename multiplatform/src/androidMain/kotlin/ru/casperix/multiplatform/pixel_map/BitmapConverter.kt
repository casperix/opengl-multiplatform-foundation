package ru.casperix.multiplatform.pixel_map

import android.graphics.Bitmap
import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.vector.int32.Vector3i
import ru.casperix.misc.DirectByteBuffer
import ru.casperix.misc.toUByteArray
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.pixel_map.Rgba8PixelMap

object BitmapConverter {
    fun bitmapToPixelMap(image: Bitmap, name: String): PixelMap {
        val buffer = DirectByteBuffer(image.width * image.height * 4)
        image.copyPixelsToBuffer(buffer)
        return Rgba8PixelMap(UByteArray3D(buffer.toUByteArray(), Vector3i(image.width, image.height, 4)), name)
    }
}