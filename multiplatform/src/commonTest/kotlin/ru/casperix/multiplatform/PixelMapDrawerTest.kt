package ru.casperix.multiplatform

import ru.casperix.math.color.uint8.Color4b
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.multiplatform.util.PixelMapDrawer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class PixelMapDrawerTest {
    @Test
    fun createRGBA() {
        val source = PixelMap.RGBA(2, 4)
        assertTrue(source.RGB() == null)
        assertTrue(source.RGBA() != null)
    }

    @Test
    fun createRGB() {
        val source = PixelMap.RGB(2, 4)
        assertTrue(source.RGB() != null)
        assertTrue(source.RGBA() == null)
    }

    @Test
    fun draw() {
        val templateColors = listOf(
            Color4b(0u, 0u, 0u, 0u),
            Color4b(255u, 0u, 0u, 32u),
            Color4b(0u, 255u, 0u, 64u),
            Color4b(0u, 0u, 255u, 128u),
            Color4b(255u, 255u, 255u, 255u),
        )

        val blendingColors = listOf(
            Color4b(0u, 0u, 0u, 0u),
            Color4b(32u, 0u, 0u, 32u),
            Color4b(0u, 64u, 0u, 64u),
            Color4b(0u, 0u, 128u, 128u),
            Color4b(255u, 255u, 255u, 255u),
        )

        listOf(true, false).forEach { alphaBlending ->
            val source = PixelMap.RGBA(5, 1)
            val sourceMap = source.RGBA()!!

            templateColors.forEachIndexed { index, color ->
                sourceMap.set(Vector2i(index, 0), color)
            }

            val target = PixelMap.RGBA(5, 1)
            val targetMap = target.RGBA()!!

            PixelMapDrawer.drawImage(target, Vector2i.ZERO, source, Vector2i.ZERO, Vector2i(5, 1), alphaBlending)


            val targetColors = if (alphaBlending) {
                blendingColors
            } else {
                templateColors
            }
            targetColors.forEachIndexed { index, color ->
                val actualColor = targetMap.get(Vector2i(index, 0))
                assertEquals(color, actualColor)
            }

        }
    }
}