package ru.casperix.multiplatform

import ru.casperix.multiplatform.font.localize.CharacterSets
import kotlin.test.Test

class StringTest {
    @Test
    fun range() {
//        val a = listOf(('a'..'c').toList(),listOf('d')).flatten()

        val a = CharacterSets.all.flatMap { it.charSet }.toSet()


        println(a)
    }

    @Test
    fun match() {
        val text = "test\nstring with\n\na b c"
        val a1 = text.split(Regex("(\\r\\n|\\r|\\n)")).flatMapIndexed { index: Int, s: String ->
            if (index == 0) {
                listOf(s)
            } else {
                listOf("\n", s)
            }
        }.flatMap {
            it.split(Regex("\\b"))
        }.filter { it.isNotEmpty() }

        println(a1)
    }
}

