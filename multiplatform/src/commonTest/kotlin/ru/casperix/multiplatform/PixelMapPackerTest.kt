package ru.casperix.multiplatform

import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.math.collection.getLooped
import ru.casperix.math.color.Colors
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.random.nextVector2i
import ru.casperix.multiplatform.rectangle_packer.RectanglePacker
import ru.casperix.multiplatform.rectangle_packer.RectangleSource
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.multiplatform.util.PixelMapDrawer
import kotlin.random.Random
import kotlin.test.Test

@ExperimentalUnsignedTypes
class PixelMapPackerTest {

    @Test
    fun pack() {
        val random = Random(1)

        val sourceList = (0 until 50).map {
            val color = Colors.all.getLooped(it)
            val size = random.nextVector2i(2..50)
            RectangleSource(Pair(color, size), size)
        }


        val packedList = RectanglePacker.pack(sourceList)
            ?: return

        val pixels = PixelMap.RGBA(packedList.dimension)
        packedList.containers.forEach {
            val (color, size) = it.source
            val borderColor = InterpolationFloat.color(Colors.BLACK, color, 0.5f)
            PixelMapDrawer.fillColor(pixels, it.position, borderColor.toColor4b(), size, false)

            if (size.greaterOrEq(Vector2i(3))) {
                PixelMapDrawer.fillColor(pixels, it.position + Vector2i(1), color.toColor4b(), size - Vector2i(2), false)
            }
        }

        resourceLoader.saveImage("test_image.png", pixels)
    }
}