package ru.casperix.multiplatform

import ru.casperix.math.color.Colors
import ru.casperix.misc.nextString
import ru.casperix.multiplatform.text.impl.TextPart
import ru.casperix.multiplatform.text.impl.TextProcessor
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

class TextProcessorTest {

    @Test
    fun colorTest() {
        Colors.all.forEach {
            val colorIn= it.toColor4b()
            val colorOut = colorIn.toColor4f().toColor4b()
            assertEquals(colorIn, colorOut)
        }

    }
    @Test
    fun simpleTest() {
        TextProcessor.splitByParts("t\nt").apply {
            assertEquals(3, size)
            assertEquals(true, get(1).isLineEnd)
        }

        val expectedList = listOf(
            TextPart("a", false, false),
            TextPart(" ", true, false),
            TextPart("b", false, false),
            TextPart("\n", true, true),
            TextPart("c", false, false),
            TextPart(" ", true, false),
            TextPart("d", false, false),
            TextPart("\r", true, true),
            TextPart("e", false, false),
            TextPart(" ", true, false),
            TextPart("f", false, false),
            TextPart("\r\n", true, true),
            TextPart("g", false, false),
            TextPart(" ", true, false),
            TextPart("hello", false, false),
            TextPart(" ", true, false),
            TextPart("\r", true, true),
            TextPart(" ", true, false),
            TextPart("i", false, false),
            TextPart(" ", true, false),
            TextPart("k", false, false),
            TextPart("\r", true, true),
            TextPart(" ", true, false),
            TextPart("n", false, false),
            TextPart("\r\n", true, true),
            TextPart("   \t", true, false),
            TextPart("4", false, false),
        )

        TextProcessor.splitByParts("a b\nc d\re f\r\ng hello \r i k\r n\r\n   \t4").apply {
            assertContentEquals(expectedList, this)
        }
    }

    @Test
    fun testSplit() {
        val random = Random(1)

        val defaultStringSource = " \tAZaz09~!@#$%^&*()_+=-`?:%;№\"!Ё\n"

        repeat(1000) { index ->
            val next = random.nextString(0..5, defaultStringSource)

            val spaces = next.filter { it == ' ' }
            val enters = next.filter { it == '\n' }

            val blocks = TextProcessor.splitByGroups(next)
            val spacesActual = blocks.filter { it == " " }
            val entersActual = blocks.filter { it == "\n" }

            assertEquals(spacesActual.size, spaces.length, "Invalid split (spaces for $index): \"$next\"")
            assertEquals(entersActual.size, enters.length, "Invalid split (enters for $index): \"$next\"")
        }
    }

    @Test
    fun testSpecialCase() {
        val english = TextProcessor.splitByGroups(TextAsset.enText)
        val arabic = TextProcessor.splitByGroups(TextAsset.arText)
        val chinese = TextProcessor.splitByGroups(TextAsset.zhText)
        val russian = TextProcessor.splitByGroups(TextAsset.ruText)
        val france = TextProcessor.splitByGroups(TextAsset.frText)

        assertEquals(140, english.size)
        assertEquals(59, arabic.size)
        assertEquals(18, chinese.size)
        assertEquals(50, russian.size)
        assertEquals(111, france.size)
    }

    @Test
    fun testSpecialCase2() {
        val english = TextProcessor.splitByParts(TextAsset.enText)
        val arabic = TextProcessor.splitByParts(TextAsset.arText)
        val chinese = TextProcessor.splitByParts(TextAsset.zhText)
        val russian = TextProcessor.splitByParts(TextAsset.ruText)
        val france = TextProcessor.splitByParts(TextAsset.frText)

        assertEquals(140, english.size)
        assertEquals(59, arabic.size)
//      TODO: actual 5 must 18
//      assertEquals(18, chinese.size)
        assertEquals(50, russian.size)
        assertEquals(111, france.size)
    }
}