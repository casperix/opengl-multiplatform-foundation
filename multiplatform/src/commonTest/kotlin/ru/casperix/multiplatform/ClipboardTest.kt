package ru.casperix.multiplatform

import ru.casperix.misc.time.getTime
import ru.casperix.multiplatform.clipboard.clipboard
import kotlin.test.Test
import kotlin.test.assertEquals

class ClipboardTest {
    /**
     * TODO: Need async test
     */
    @Test
   fun test() {
        val time = getTime().toIsoString()
        clipboard.pushString(time)
        clipboard.popString().thenAccept {
            assertEquals(time, it)
            println("checked")
        }
    }
}