package ru.casperix.multiplatform

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.vertex.Vertex
import ru.casperix.renderer.vector.vertex.VertexPosition2
import ru.casperix.renderer.vector.vertex.VertexTextureCoord
import kotlin.test.Test
import kotlin.time.measureTimedValue

class PerformanceTest {

    @Test
    fun perf() {

        val testList = listOf<(Int) -> Pair<String, FloatArray>>(
            { pVertex(it) },
            { pBuilder2(it) },
            { pBuilder(it) },
            { pBuilder2(it) },
            { pDirect(it) },
        )

        var lastBuffer:FloatArray? = null

        val vertices = 2_000_000
        val cycles = 10
        repeat(cycles) {
            println("================")
            testList.forEach { test ->
                val (value, time) = measureTimedValue {
                    test.invoke(vertices)
                }

                if (lastBuffer != null) {
                  if (!lastBuffer.contentEquals(value.second)) {
//                      throw Exception("invalid")
                  }
                }
                lastBuffer = value.second

                println("test: ${value.first} for ${time.inWholeMilliseconds} ms")
           }
        }

    }

    class FloatBufferBuilder(val buffer: FloatArray) {
        fun setPosition(offset: Int, x: Float, y: Float) {
            buffer[offset] = x
            buffer[offset + 1] = y
        }

        fun setTextureCoord(offset: Int, x: Float, y: Float) {
            buffer[offset] = x
            buffer[offset + 1] = y
        }
    }

    class FloatBufferBuilder2(val buffer: FloatArray) {
        fun setPosition(offset: Int, x: Float, y: Float) {
            if (buffer.size <= offset+1){
                throw Exception("invalid")
            }
            buffer[offset] = x
            buffer[offset + 1] = y
        }

        fun setTextureCoord(offset: Int, x: Float, y: Float) {
            if (buffer.size <= offset+1){
                throw Exception("invalid")
            }
            buffer[offset] = x
            buffer[offset + 1] = y
        }
    }


    fun pBuilder(amount: Int): Pair<String, FloatArray> {
        val floats = FloatArray(amount * 4)
        val buffer = FloatBufferBuilder(floats)
        (0 until amount).forEach {
            buffer.setPosition(it * 4, 0f, it.toFloat())
            buffer.setTextureCoord(it * 4 + 2, it.toFloat(), 0f)
        }

        return Pair("builder", floats)
    }

    fun pBuilder2(amount: Int): Pair<String, FloatArray> {
        val floats = FloatArray(amount * 4)
        val buffer = FloatBufferBuilder2(floats)
        (0 until amount).forEach {
            buffer.setPosition(it * 4, 0f, it.toFloat())
            buffer.setTextureCoord(it * 4 + 2, it.toFloat(), 0f)
        }

        return Pair("builder2", floats)
    }

    fun pDirect(amount: Int): Pair<String, FloatArray> {
        val floats = FloatArray(amount * 4)
        (0 until amount).forEach {
            val i = it * 4
            floats[i + 0] = 0f
            floats[i + 1] = it.toFloat()
            floats[i + 2] = it.toFloat()
            floats[i + 3] = 0f
        }
        return Pair("direct", floats)
    }

    fun pVertex(amount: Int): Pair<String, FloatArray> {
        val floats = FloatArray(amount * 4)
        var offset = 0
        (0 until amount).forEach {
           val v= Vertex(
                VertexPosition2(Vector2f(0f, it.toFloat())),
                VertexTextureCoord(Vector2f(it.toFloat(), 0f))
            )
            v.attributes.map {
                offset = it.encodeTo(floats, offset)
            }
        }
        return Pair("vertex", floats)
    }


}