package ru.casperix.multiplatform.loader

import ru.casperix.misc.Right
import ru.casperix.misc.toByteArray
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal
import ru.casperix.signals.concrete.map
import ru.casperix.multiplatform.util.WasmImageConverter
import org.khronos.webgl.*
import org.w3c.dom.Image
import org.w3c.xhr.ARRAYBUFFER
import org.w3c.xhr.XMLHttpRequest
import org.w3c.xhr.XMLHttpRequestResponseType

actual val resourceLoader: AbstractResourceLoader = WasmLoaders()

class WasmLoaders : CachedResourceLoader() {

    override fun loadBytesDirect(path: String): EitherFuture<ByteArray, ResourceLoadError> {
        return loadCustom(path, XMLHttpRequestResponseType.ARRAYBUFFER).map({
            val bytes = (it as ArrayBuffer).toByteArray()
            Right(bytes)
        }, {
            ResourceCustomLoadError(it)
        })

    }

    override fun loadImageDirect(path: String): EitherFuture<PixelMap, ResourceLoadError> {
        val loader = EitherSignal<PixelMap, ResourceLoadError>()
        val image = Image()
        image.onerror = { p1, p2, p3, p4, p5 ->
            loader.reject(ResourceCustomLoadError("Unknown"))
            null
        }
        image.onload = {
            val pixelMap = WasmImageConverter.imageToPixelMap(image, path)
            if (pixelMap == null) {
                loader.reject(ResourceCustomLoadError("Can't convert (may be empty image)"))
            } else {
                loader.accept(pixelMap)
            }
        }
        image.src = path

        return loader
    }

    override fun saveImage(path: String, pixelMap: PixelMap): ResourceSaveError? {
        val canvas = WasmImageConverter.pixelMapToCanvas( pixelMap)


        val canvasUrl = canvas.toDataURL("image/png")
        return ResourceCustomSaveError("Web can only make image link:\n$canvasUrl")
    }

    override fun saveText(path: String, data: String): EitherFuture<Unit, ResourceSaveError> {
        return EitherFuture.reject(ResourceCustomSaveError("Web can't save data"))
    }

    override fun saveBytes(path: String, data: ByteArray): EitherFuture<Unit, ResourceSaveError> {
        return EitherFuture.reject(ResourceCustomSaveError("Web can't save data"))
    }

    private fun loadCustom(
        path: String,
        responseType: XMLHttpRequestResponseType,
    ): EitherFuture<Any?, String> {
        val loader = EitherSignal<Any?, String>()

        val request = XMLHttpRequest()
        request.responseType = responseType
        request.onload = {
            if (request.status != 200.toShort()) {
                loader.reject("Can't load data from $path, with status ${request.status}")
            } else {
                loader.accept(request.response)
            }
        }
        request.onerror = {
            loader.reject(it.toString())
        }

        request.open("GET", path, true)
        request.send()
        return loader
    }
}