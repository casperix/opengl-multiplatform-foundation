package ru.casperix.multiplatform.text

import kotlinx.browser.document
import org.w3c.dom.*
import ru.casperix.math.axis_aligned.dimensionOf
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.misc.ColorDecoder.toHexRGB
import ru.casperix.math.color.rgba.RgbaColor4b
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.Disposable
import ru.casperix.misc.ceilToInt
import ru.casperix.multiplatform.FontSmoothMode
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.util.WasmImageConverter
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.pixel_map.Rgba8PixelMap
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder


object WasmTextGraphicProcessor : TextGraphicProcessor {

    override fun getFontMetrics(font: FontReference): FontMetrics = builder {
        setFont(font)
        val jsMetrics = context.measureText("A")
        val leading =
            font.size.toFloat() - jsMetrics.fontBoundingBoxAscent.toFloat() + jsMetrics.fontBoundingBoxDescent.toFloat()

        FontMetrics(
            jsMetrics.fontBoundingBoxAscent.toFloat(),
            jsMetrics.fontBoundingBoxDescent.toFloat(),
            leading
        )
    }

    override fun getStringMetrics(font: FontReference, line: String): StringMetrics = builder {
        setFont(font)

        val jsMetrics = context.measureText("A")
        val height = jsMetrics.fontBoundingBoxAscent + jsMetrics.fontBoundingBoxDescent

        StringMetrics.calculate(line, {
            context.measureText(line.slice(it..it)).width.toFloat()
        }, height.toFloat())
    }

    private fun <Result> builder(callback: BuilderContext.() -> Result): Result {
        val context = BuilderContext()
        return callback(context).apply {
            context.dispose()
        }
    }

    class BuilderContext : Disposable {
        val canvas = document.createElement("canvas") as HTMLCanvasElement
        val fontSmoothMode = PlatformMisc.fontSmoothMode
        val subpixelRendering =
            fontSmoothMode == FontSmoothMode.SUBPIXEL_WHITE_ON_BLACK || fontSmoothMode == FontSmoothMode.SUBPIXEL_BLACK_ON_WHITE

        val context =
            canvas.getContext("2d", CanvasRenderingContext2DSettings(!subpixelRendering)) as? CanvasRenderingContext2D
                ?: throw Exception("canvas not supported 2d")

        fun setFont(font: FontReference) {
            context.font = "${font.size}px ${font.name}"
        }

        override fun dispose() {
            canvas.remove()
        }
    }

    override fun create(builder: VectorGraphicBuilder, scheme: TextScheme, transform: Matrix3f): Boolean = builder {
        val value = scheme.summaryArea.value
        val minX = value.min.x
        val minY = value.min.y
        val maxX = value.max.x
        val maxY = value.max.y

        val imageWidth = (maxX - minX).ceilToInt()
        val imageHeight = (maxY - minY).ceilToInt()

        if (imageWidth == 0 || imageHeight == 0) {
            return@builder false
        }

        canvas.width = imageWidth
        canvas.height = imageHeight
        canvas.style.width = imageWidth.toString() + "px";
        canvas.style.height = imageHeight.toString() + "px";
        canvas.style.imageRendering = "pixelated"

        context.textAlign = CanvasTextAlign.START
        context.imageSmoothingEnabled = false
        context.textBaseline = CanvasTextBaseline.TOP

        scheme.elements.forEach {
            val font = it.font
            val jsFontName = "${font.size}px ${font.name}"

            context.font = jsFontName
            context.fillStyle = it.foreground.toHexRGB().toJsString()

            canvas.style.fontWeight = font.weight.value.toString()

            //  text aligned by emHeightAscent
            val textMetrics = context.measureText("A")
            val yLocalOffset = textMetrics.fontBoundingBoxAscent - textMetrics.emHeightAscent

            val position = Vector2f(
                it.textArea.min.x - minX,
                it.textArea.min.y - minY + yLocalOffset.toFloat()
            ).run { if (TextRenderConfig.textRoundToPixel) round() else this }

            context.fillText(it.part.text, position.x.toDouble(), position.y.toDouble())
        }

        if (fontSmoothMode == FontSmoothMode.SUBPIXEL_WHITE_ON_BLACK) {
            context.fillStyle = "black".toJsString()
            context.fillRect(0.0, 0.0, imageWidth.toDouble(), imageHeight.toDouble())
        }

        if (fontSmoothMode == FontSmoothMode.SUBPIXEL_BLACK_ON_WHITE) {
            context.fillStyle = "white".toJsString()
            context.fillRect(0.0, 0.0, imageWidth.toDouble(), imageHeight.toDouble())
        }

        if (fontSmoothMode == FontSmoothMode.SUBPIXEL_BLACK_ON_WHITE) {
            context.fillStyle = "black".toJsString()
        } else {
            context.fillStyle = "white".toJsString()
        }

        val pixelMap = WasmImageConverter.canvasToPixelMap(context, "")
            ?: throw Exception("Invalid text graphic")

        if (subpixelRendering) {
            val needInvert = fontSmoothMode == FontSmoothMode.SUBPIXEL_BLACK_ON_WHITE

            replaceColor(pixelMap) {
                val factor = (it.red.toUInt() + it.green.toUInt() + it.blue.toUInt()) / 3u
                if (needInvert) {
                    it.copy(alpha = (255u - factor).toUByte())
                } else {
                    it.copy(alpha = factor.toUByte())
                }
            }
        }

        val area = Box2f.byDimension(Vector2f(minX, minY), dimensionOf(imageWidth, imageHeight).toVector2f())
        builder.addRect(
            SimpleMaterial(Texture2D(pixelMap, TextureConfig(useMipMap = false))),
            area,
            Box2f.ONE,
            transform
        )
        true
    }

    private fun replaceColor(pixelMap: Rgba8PixelMap, replace: (RgbaColor4b) -> RgbaColor4b) {
        (0 until pixelMap.dimension.y).forEach { y ->
            (0 until pixelMap.dimension.x).forEach { x ->
                val input = pixelMap.get(x, y)
                val output = replace(input)
                pixelMap.set(x, y, output)
            }
        }
    }
}