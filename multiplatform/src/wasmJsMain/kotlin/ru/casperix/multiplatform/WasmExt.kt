package ru.casperix.multiplatform

import org.w3c.dom.Navigator
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal
import kotlin.js.Promise

public external val navigator: Navigator

fun <Output:JsAny?> Promise<Output>.asEitherFuture(): EitherFuture<Output, Throwable> {
    val signal = EitherSignal<Output, Throwable>()
    then({
        signal.accept(it)
        null
    }, {
        signal.reject(it.toThrowableOrNull() ?: throw Throwable("Unknown error: $it"))
        null
    })
    return signal
}