package ru.casperix.multiplatform.clipboard

import ru.casperix.misc.Right
import ru.casperix.multiplatform.asEitherFuture
import ru.casperix.multiplatform.navigator
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.mapAccept


actual val clipboard: ClipboardApi = WasmClipboard()

class WasmClipboard : ClipboardApi {

    override fun pushString(value: String): EitherFuture<Unit, Throwable> {
        val future = navigator.clipboard.writeText(value).asEitherFuture()
        return future.mapAccept {
            Right(Unit)
        }
    }

    override fun popString(): EitherFuture<String, Throwable> {
        val future = navigator.clipboard.readText().asEitherFuture()
        return future.mapAccept { js: JsString ->
            Right(js.toString())
        }

    }
}


