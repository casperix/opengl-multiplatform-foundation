package ru.casperix.multiplatform.util

import org.khronos.webgl.Uint8ClampedArray
import org.khronos.webgl.set

object BytesHelper {

    /**
     * https://youtrack.jetbrains.com/issue/KT-24583/JS-Uint8ClampedArray-declaration-unusable
     */
    fun copyBytes(jsData: Uint8ClampedArray, uBytes:UByteArray) {
        val jsRaw = jsData

        for (i in 0 until jsData.length) {
            jsRaw[i] = uBytes[i].toByte()
        }
    }
}