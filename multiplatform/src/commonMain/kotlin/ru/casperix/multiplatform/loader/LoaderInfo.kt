package ru.casperix.multiplatform.loader

import ru.casperix.signals.concrete.EitherFuture

class LoaderInfo<Content : Any, Error:Any>(val file: String, val signal: EitherFuture<Content, Error>) {
    var content: Content? = null  ; private set
    var error:Error? = null; private set
    var isLoading = true; private set

    init {
        signal.then({
            isLoading = false
            content = it
        }, {
            isLoading = false
            error = it
        })
    }
}