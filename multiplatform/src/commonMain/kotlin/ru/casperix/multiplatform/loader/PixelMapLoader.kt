package ru.casperix.multiplatform.loader

import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.EmptySignal
import ru.casperix.multiplatform.util.PixelMapLoaderInfo

object PixelMapLoader {
    private val loaders = mutableMapOf<String, PixelMapLoaderInfo>()

    private val loadingComplete = EmptySignal()

    fun thenComplete(callback: () -> Unit) {
        if (getLoadingAmount() == 0) {
            callback()
        } else {
            loadingComplete.then {
                callback()
            }
        }
    }

    fun getLoadingAmount(): Int {
        return loaders.values.filter { it.isLoading }.size
    }

    fun getLoaderList(fileList: List<String>): List<PixelMapLoaderInfo> {
        return fileList.map {
            getLoader(it)
        }
    }

    fun getLoader(file: String): PixelMapLoaderInfo {
        return loaders.getOrPut(file) {
            PixelMapLoaderInfo(file, resourceLoader.loadImage(file)).apply {
                signal.then({
                    checkComplete()
                }, {
                    checkComplete()
                })
            }
        }
    }

    private fun checkComplete() {
        if (getLoadingAmount() != 0) return
        loadingComplete.set()
    }

    fun getOrNull(file: String): PixelMap? {
        val loader = loaders[file] ?: return null
        return loader.content
    }

}