package  ru.casperix.multiplatform.loader

import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal


expect val resourceLoader: AbstractResourceLoader

interface AbstractResourceLoader {
    fun loadBytes(path: String): EitherFuture<ByteArray, ResourceLoadError>
    fun loadText(path: String): EitherFuture<String, ResourceLoadError> {
        val proxy = EitherSignal<String, ResourceLoadError>()

        loadBytes(path).then({ bytes ->
            proxy.accept(bytes.decodeToString())
        }, {
            proxy.reject(it)
        })
        return proxy
    }

    fun loadImage(path: String): EitherFuture<PixelMap, ResourceLoadError>

    fun saveImage(path: String, pixelMap: PixelMap): ResourceSaveError?

    fun saveText(path: String, data:String):EitherFuture<Unit, ResourceSaveError>
    fun saveBytes(path: String, data:ByteArray):EitherFuture<Unit, ResourceSaveError>
}

