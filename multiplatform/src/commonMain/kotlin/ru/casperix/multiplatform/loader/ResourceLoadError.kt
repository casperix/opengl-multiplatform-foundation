package ru.casperix.multiplatform.loader

interface ResourceLoadError

data class ResourceNotFoundLoadError(val path:String) : ResourceLoadError

data class ResourceCustomLoadError(val message: String) : ResourceLoadError

interface ResourceSaveError

data class ResourceCustomSaveError(val message: String) : ResourceSaveError
