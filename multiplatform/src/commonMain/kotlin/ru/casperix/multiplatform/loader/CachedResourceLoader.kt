package ru.casperix.multiplatform.loader

import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.EitherFuture

abstract class CachedResourceLoader : AbstractResourceLoader {
    private val bytesCache = mutableMapOf<String, EitherFuture<ByteArray, ResourceLoadError>>()
    private val imagesCache = mutableMapOf<String, EitherFuture<PixelMap, ResourceLoadError>>()

    fun clearAll() {
        bytesCache.clear()
        imagesCache.clear()
    }

    fun clear(path: String) {
        bytesCache.remove(path)
        imagesCache.remove(path)
    }

    final override fun loadBytes(path: String): EitherFuture<ByteArray, ResourceLoadError> {
        return bytesCache.getOrPut(path) {
            loadBytesDirect(path)
        }
    }

    final override fun loadImage(path: String): EitherFuture<PixelMap, ResourceLoadError> {
        return imagesCache.getOrPut(path) {
            loadImageDirect(path)
        }
    }

    abstract fun loadBytesDirect(path: String): EitherFuture<ByteArray, ResourceLoadError>
    abstract fun loadImageDirect(path: String): EitherFuture<PixelMap, ResourceLoadError>
}