package ru.casperix.multiplatform

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.pixel_map.PixelMap


fun Vector2f.toDimension(): Dimension2f {
    return Dimension2f(x, y)
}

fun PixelMap.dimension(): Dimension2i {
    return Dimension2i(width, height)
}


fun ByteArray.put(source: ByteArray, receiverOffset: Int, sourceOffset: Int = 0, sourceLength: Int = source.size) {
    source.copyInto(this, receiverOffset, sourceOffset, sourceOffset + sourceLength)
}
