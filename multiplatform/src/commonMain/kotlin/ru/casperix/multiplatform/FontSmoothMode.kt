package ru.casperix.multiplatform

enum class FontSmoothMode {
    NONE,
    SUBPIXEL_WHITE_ON_BLACK,
    SUBPIXEL_BLACK_ON_WHITE,
}