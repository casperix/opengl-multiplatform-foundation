package ru.casperix.multiplatform

import ru.casperix.multiplatform.font.FontReference

internal expect val currentPlatformType:PlatformType

object PlatformMisc {
    var debugOneFPSMode = false

    /**
     *  In this mode, you need to manually call `Surface::requestFrame` (for render next frame)
     */
    var continuousRendering = true
    var fontSmoothMode = FontSmoothMode.NONE
    var defaultFont = FontReference("Serif", 24)

    val platformType = currentPlatformType
    var loaderCacheEnabled = true

    val canSaveFile = currentPlatformType == PlatformType.JVM
    val canLoadResource = true
}
