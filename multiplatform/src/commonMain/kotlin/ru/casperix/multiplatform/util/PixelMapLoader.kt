package ru.casperix.multiplatform.util

import ru.casperix.multiplatform.loader.LoaderInfo
import ru.casperix.multiplatform.loader.ResourceLoadError
import ru.casperix.renderer.pixel_map.PixelMap


typealias PixelMapLoaderInfo = LoaderInfo<PixelMap, ResourceLoadError>

