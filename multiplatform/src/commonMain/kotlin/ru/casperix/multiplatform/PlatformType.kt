package ru.casperix.multiplatform

enum class PlatformType {
    ANDROID,
    JVM,
    JS,
    WASM
}