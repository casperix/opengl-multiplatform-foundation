package ru.casperix.multiplatform.clipboard

import ru.casperix.signals.concrete.EitherFuture


expect val clipboard: ClipboardApi

interface ClipboardApi {
    fun pushString(value: String): EitherFuture<Unit, Throwable>

    fun popString(): EitherFuture<String, Throwable>
}