package ru.casperix.multiplatform.text

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.impl.TextDirectionSolver
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder

interface TextGraphicProcessor {
    fun create(builder: VectorGraphicBuilder, scheme: TextScheme, transform: Matrix3f):Boolean
    fun getStringMetrics(font: FontReference, line: String): StringMetrics

    fun getFontMetrics(font: FontReference): FontMetrics
    fun isLeftToRight(line: String): Boolean {
        return TextDirectionSolver.isLeftToRight(line)
    }
}

