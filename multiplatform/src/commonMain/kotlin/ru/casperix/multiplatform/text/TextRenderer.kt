package ru.casperix.multiplatform.text


import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.vector.VectorShape
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder


expect val textRenderer: TextRendererApi

fun Renderer2D.drawText(
    scheme: TextScheme,
    transform: Matrix3f = Matrix3f.IDENTITY,
    showText: Boolean = true,
    showMetrics: Boolean = false,
) {
    if (showMetrics) {
        val graphic = textRenderer.getTextMetricGraphic(scheme)
        drawGraphic(graphic, transform)
    }
    if (showText) {
        val graphic = textRenderer.getTextGraphic(scheme)
        drawGraphic(graphic, transform)
    }
}


fun Renderer2D.drawText(
    text: String,
    transform: Matrix3f,
    reference: FontReference = PlatformMisc.defaultFont,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    color: Color = Color.WHITE,
    backgroundColor: Color? = null,
    showText: Boolean = true,
    showMetrics: Boolean = false,
): TextScheme {
    return drawText(
        listOf(TextView(text, reference, color, backgroundColor)),
        transform,
        availableArea,
        showText,
        showMetrics
    )
}

fun Renderer2D.drawText(
    text: String,
    position: Vector2f,
    reference: FontReference = PlatformMisc.defaultFont,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    color: Color = Color.WHITE,
    backgroundColor: RgbaColor4f? = null,
    showText: Boolean = true,
    showMetrics: Boolean = false,
): TextScheme {
    return drawText(
        text,
        Matrix3f.translate(position.x, position.y),
        reference,
        availableArea,
        color,
        backgroundColor,
        showText,
        showMetrics
    )
}

fun Renderer2D.drawText(
    text: String,
    x: Float,
    y: Float,
    reference: FontReference = PlatformMisc.defaultFont,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    color: Color = Color.WHITE,
    backgroundColor: RgbaColor4f? = null,
    showText: Boolean = true,
    showMetrics: Boolean = false,
): TextScheme {
    return drawText(
        text,
        Matrix3f.translate(x, y),
        reference,
        availableArea,
        color,
        backgroundColor,
        showText,
        showMetrics
    )
}

fun Renderer2D.drawText(
    blocks: List<TextView>,
    transform: Matrix3f,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    showText: Boolean = true,
    showMetrics: Boolean = false,
): TextScheme {
    val scheme = textRenderer.getTextScheme(blocks, availableArea)
    drawText(scheme, transform, showText, showMetrics)
    return scheme
}

fun VectorGraphicBuilder.addText(
    scheme: TextScheme,
    transform: Matrix3f = Matrix3f.IDENTITY,
) {
    val graphic = textRenderer.getTextGraphic(scheme)
    add(graphic.shapes, transform)
}

fun VectorGraphicBuilder.addText(
    blocks: List<TextView>,
    transform: Matrix3f,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
): TextScheme {
    val scheme = textRenderer.getTextScheme(blocks, availableArea)
    addText(scheme, transform)
    return scheme
}


fun VectorGraphicBuilder.addText(
    text: String,
    transform: Matrix3f,
    reference: FontReference = PlatformMisc.defaultFont,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    color: Color = Color.WHITE,
    backgroundColor: Color? = null,
): TextScheme {
    return addText(
        listOf(TextView(text, reference, color, backgroundColor)),
        transform,
        availableArea,
    )
}

fun VectorGraphicBuilder.addText(
    text: String,
    position: Vector2f,
    reference: FontReference = PlatformMisc.defaultFont,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    color: Color = Color.WHITE,
    backgroundColor: RgbaColor4f? = null,
): TextScheme {
    return addText(
        text,
        Matrix3f.translate(position.x, position.y),
        reference,
        availableArea,
        color,
        backgroundColor,
    )
}

fun VectorGraphicBuilder.addText(
    text: String,
    x: Float,
    y: Float,
    reference: FontReference = PlatformMisc.defaultFont,
    availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
    color: Color = Color.WHITE,
    backgroundColor: RgbaColor4f? = null,
): TextScheme {
    return addText(
        text,
        Matrix3f.translate(x, y),
        reference,
        availableArea,
        color,
        backgroundColor,
    )
}