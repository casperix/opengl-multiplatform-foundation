package ru.casperix.multiplatform.text

import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.pixel.PixelFontSymbol
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f

/**
 *  Work only for traditional languages -- one symbol -- one glyph
 */
data class TextLayout(val metrics: FontMetrics, val text: String, val symbols: List<SymbolLayout>) {
    fun getArea(): Box2f {
        if (symbols.isEmpty()) {
            return Box2f(Vector2f.ZERO, Vector2f.ZERO)
        }
        var minCorner = Vector2f(Float.POSITIVE_INFINITY)
        var maxCorner = Vector2f(Float.NEGATIVE_INFINITY)
        symbols.forEach {
            val area = it.area

            minCorner = minCorner.lower(area.min)
            maxCorner = maxCorner.upper(area.max)
        }

        return Box2f(minCorner, maxCorner)
    }

    companion object {
        val EMPTY = TextLayout(FontMetrics(0f, 0f, 0f), "", emptyList())
    }
}

data class SymbolLayout(val pivot: Vector2f, val symbol: PixelFontSymbol, val symbolArea:Box2f) {
    val area = symbolArea + pivot
}