package ru.casperix.multiplatform.text.impl

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.StringMetrics

data class TextSchemeElement(
    val part: TextPart,
    val textArea: Box2f,
    val stringMetrics: StringMetrics,
    val font: FontReference,
    val foreground: RgbaColor4f,
    val background: RgbaColor4f?
)