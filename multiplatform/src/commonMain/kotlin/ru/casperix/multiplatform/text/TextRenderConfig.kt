package ru.casperix.multiplatform.text

import ru.casperix.multiplatform.text.impl.MissingSymbolBehaviour

object TextRenderConfig {
    var debugDrawFontAtlas = false
    var debugSaveFontAtlas = true
    var textRoundToPixel = true
    var textMissingSymbolBehaviour = MissingSymbolBehaviour.SHOW_WHITE_SQUARE
    val useAntiAlias = true
    val useBicubicInterpolation = false
    val useSubpixelRender = false
}