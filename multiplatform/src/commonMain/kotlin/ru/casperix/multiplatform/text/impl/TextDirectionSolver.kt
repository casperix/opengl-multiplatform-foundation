package ru.casperix.multiplatform.text.impl

/**
 * TODO: For js see "Intl.Segmenter"
 */
object TextDirectionSolver {
    private val arabic = ('\u0600'..'\u06FF').iterator().asSequence().toList().toSet()
    private val persian = ('\u0600'..'\u06FF').iterator().asSequence().toList().toSet()
    private val hebrew = ('\u0590'..'\u05FF').iterator().asSequence().toList().toSet()
    private val sindhi = ('\u0AE0'..'\u0AEF').iterator().asSequence().toList().toSet()
    private val urdu = ('\u067E'..'\u06DF').iterator().asSequence().toList().toSet()

    private val rightDirectionChars: Set<Char> = arabic + persian + hebrew + sindhi + urdu
    private val leftDirectionsChars: Set<Char> = emptySet()

    fun isLeftToRight(line: String): Boolean {
        line.forEach {
            if (it.isLetter()) {
                if (leftDirectionsChars.contains(it)) return true
                if (rightDirectionChars.contains(it)) return false
            }
        }
        return true
    }

}