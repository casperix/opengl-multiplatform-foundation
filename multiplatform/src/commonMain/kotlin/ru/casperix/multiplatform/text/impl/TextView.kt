package ru.casperix.multiplatform.text.impl

import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.multiplatform.font.FontReference

data class TextView(val text: String, val font: FontReference, val foreground: RgbaColor4f, val background: RgbaColor4f? = null) {
    constructor(text: String, font: FontReference, foreground: Color, background: Color? = null) : this(text, font, foreground.toRGBA(), background?.toRGBA())
}