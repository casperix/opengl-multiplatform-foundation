package ru.casperix.multiplatform.text

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.renderer.vector.VectorGraphic
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder

interface TextRendererApi {
    fun getFontMetrics(font: FontReference): FontMetrics

    fun getTextScheme(
        blocks: List<TextView>,
        availableArea: Vector2f = Vector2f(Float.POSITIVE_INFINITY),
        alignMode: AlignMode = AlignMode.LEFT_TOP,
    ): TextScheme

    fun getTextGraphic(
        scheme: TextScheme
    ): VectorGraphic

    fun getTextMetricGraphic(
        scheme: TextScheme
    ): VectorGraphic

    fun buildTextGraphic(builder: VectorGraphicBuilder, scheme: TextScheme, transform: Matrix3f)

}