package ru.casperix.multiplatform.text.impl

object TextProcessor {
    private val BREAK_BY_GROUPS = Regex("(\\r\\n|\\r|\\n| |。|，)")

    fun splitByGroups(text: String): List<String> {
        var nextIndex = 0
        val groups = BREAK_BY_GROUPS.findAll(text).toList().flatMap {
            val value = it.value
            val range = it.range

            val divider = when (value) {
                "\r\n" -> "\n"
                "\r" -> "\n"
                "\n" -> "\n"
                " " -> " "
                else -> if (!range.isEmpty()) {
                    value
                } else {
                    null
                }
            }

            val beforeDivider = if (nextIndex < range.first) {
                text.substring(nextIndex until range.first)
            } else null

            nextIndex = it.range.last + 1
            listOfNotNull(beforeDivider, divider)
        }

        val afterDivider = if (nextIndex < text.length) {
            text.substring(nextIndex until text.length)
        } else null

        return groups + listOfNotNull(afterDivider)
    }

    private val END_LINE = Regex("(\\r\\n|\\r|\\n)")
    private val BREAK_BY_PARTS = Regex("(\\r\\n|\\r|\\n)|([^\\r\\n\\s]+|。|，)|(^\\s+)")

    fun splitByParts(value: String): List<TextPart> {
        val partList = mutableListOf<TextPart>()

        fun addPart(range: IntRange) {
            val subValue = value.slice(range)
            val isWhitespace = subValue.first().isWhitespace()
            val isLineEnd = END_LINE.matchesAt(subValue, 0)

            partList += TextPart(subValue, isWhitespace, isLineEnd)
        }

        var lastCharIndex = 0
        BREAK_BY_PARTS.findAll(value).toList().forEach {

            if (lastCharIndex < value.length && lastCharIndex < it.range.min()) {
                addPart(lastCharIndex..<it.range.min())
            }
            addPart(it.range)

            lastCharIndex = it.range.max() + 1
        }

        if (lastCharIndex < value.length) {
            addPart(lastCharIndex..<value.length)
        }

        return partList
    }

}