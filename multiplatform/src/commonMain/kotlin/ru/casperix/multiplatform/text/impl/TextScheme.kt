package ru.casperix.multiplatform.text.impl

import ru.casperix.math.axis_aligned.float32.Box2f

data class TextScheme(val elements: List<TextSchemeElement>) {
    private val _hashCode = elements.hashCode()

    override fun hashCode(): Int = _hashCode

    val summaryArea = lazy {
        if (elements.isEmpty()) {
            Box2f.ZERO
        } else {
            val min = elements.map { it.textArea.min }.reduce { a, b -> a.lower(b) }
            val max = elements.map { it.textArea.max }.reduce { a, b -> a.upper(b) }
            Box2f(min, max)
        }
    }

    companion object {
        val EMPTY = TextScheme(emptyList())
    }
}