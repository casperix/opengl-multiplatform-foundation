package ru.casperix.multiplatform.text.impl

data class TextPart(val text: String, val isWhitespace: Boolean, val isLineEnd:Boolean)
