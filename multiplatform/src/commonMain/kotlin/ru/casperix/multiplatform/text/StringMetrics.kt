package ru.casperix.multiplatform.text

import ru.casperix.math.axis_aligned.dimensionOf
import ru.casperix.math.axis_aligned.float32.Dimension2f

data class StringMetrics(val size: Dimension2f, val advances:List<Float>) {
    companion object {
        fun calculate(line:String, widthByIndex:(Int)->Float, charHeight:Float):StringMetrics {
            val widths = line.indices.map {
                widthByIndex(it)
            }

            var last = 0f
            val advances = widths.map {
                val back = last
                last += it
                back
            }

            val imageWidth = widths.sum()
            val symbolSize = dimensionOf(imageWidth, charHeight)
            return StringMetrics(symbolSize, advances)
        }

        fun calculate(charsWidth: (CharArray, Int, Int) -> Float, line: String, height:Float): StringMetrics {
            val chars = line.toCharArray()
            val widths = line.mapIndexed { index, c ->
                charsWidth(chars, index, index+1)
            }

            var last = 0f
            val advances = widths.map {
                val back = last
                last += it
                back
            }

            val width = widths.sum()
            return StringMetrics( dimensionOf(width, height), advances)

        }
    }
}