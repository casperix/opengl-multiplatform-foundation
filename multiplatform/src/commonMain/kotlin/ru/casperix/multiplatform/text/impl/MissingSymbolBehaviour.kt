package ru.casperix.multiplatform.text.impl

@Deprecated(message = "Not used")
enum class MissingSymbolBehaviour {
    IGNORE,
    SHOW_WHITE_SQUARE,
    SHOW_CHAR_CODE,
}