package ru.casperix.multiplatform.pixel_map

import ru.casperix.multiplatform.dimension
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.renderer.material.*

@ExperimentalUnsignedTypes
data class PixelMapRegion(val atlas: Texture2D, val region: Box2i) {
    val dimension get() = region.dimension

    constructor(pixelMap: Texture2D) : this(pixelMap, Box2i(Vector2i.ZERO, pixelMap.map.dimension().toVector2i()))
}