package ru.casperix.multiplatform.font

data class FontLeading(val factor:Float) {
	companion object {
		val LIGHT = FontLeading(1.4f)
		val NORMAL = FontLeading(1f)
		val DENSITY = FontLeading(0.6f)
	}
}