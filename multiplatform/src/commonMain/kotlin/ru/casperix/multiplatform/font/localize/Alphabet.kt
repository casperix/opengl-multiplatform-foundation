package ru.casperix.multiplatform.font.localize

class Alphabet(val language: Language, hasTitleCase: Boolean, chars: String) {
    val charSet = if (!hasTitleCase) {
        chars.toSet()
    } else {
        (chars.map { it.titlecaseChar() }.toSet() + chars.toSet())
    }
    val chars = charSet.joinToString("")
}