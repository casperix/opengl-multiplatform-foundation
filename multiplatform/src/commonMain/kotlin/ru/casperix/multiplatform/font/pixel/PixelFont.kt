package ru.casperix.multiplatform.font.pixel

import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.pixel_map.PixelMapRegion
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.mapping

data class PixelFont(
    val reference: FontReference, val metrics: FontMetrics, val symbols: Collection<PixelFontSymbol>
) {
    val symbolMap = symbols.mapping { Pair(it.char, it) }
    val lineHeight = metrics.lineHeight * reference.leading.factor

    //TODO: move to font-renderer
    val digitWidth = (symbols.filter { it.char.isDigit() }.maxOfOrNull { it.size.x } ?: 0).toFloat()

    //TODO: move to font-renderer
    val symbolWidth = run {
        val a = symbolMap['A']
        a?.size?.x?.toFloat() ?: 0f
        //        val letters = symbols.filter { it.char.isLetter() }
//        if (letters.isEmpty()) {
//            0f
//        } else {
//            letters.map { it.size.x }.reduce { a, b -> a + b } / letters.size.toFloat()
//        }
    }
}


data class PixelFontSymbol(val char: Char, val baselineOffset: Vector2i, val size: Vector2i, val graphic: PixelMapRegion?)