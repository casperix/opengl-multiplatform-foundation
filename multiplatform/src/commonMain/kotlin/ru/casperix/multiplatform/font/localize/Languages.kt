package ru.casperix.multiplatform.font.localize

object Languages {
    val russian = Language("ru")
    val english = Language("en")
    val chinese = Language("zh")
}