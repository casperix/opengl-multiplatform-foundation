package ru.casperix.multiplatform.font.localize


class CharacterExpandByLocale {
	fun expand(characters: Set<Char>): Set<Char> {
		val filteredAlphabets = CharacterSets.all.filter { alphabet ->
			characters.firstNotNullOfOrNull { char ->
				if (alphabet.charSet.contains(char)) char else null
			} != null
		}

		var output = emptySet<Char>()
		filteredAlphabets.forEach { alphabet ->
			output = output + alphabet.charSet
		}
		return output + characters + CharacterSets.COMMON_CHARACTER_SET
	}

}



