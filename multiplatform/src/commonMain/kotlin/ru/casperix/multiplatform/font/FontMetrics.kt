package ru.casperix.multiplatform.font

/**
 *
 * ----------------------------------------------
 * /\          L
 * |           L
 * | ascent    L                    y    y
 * |           L                    y    y
 *\/           LLLLLLL               yyyy
 * --- baseline (y = 0, grow-down) -----y--------
 * /\                                   y
 * |                                   yy
 * | descent
 *\/
 * ----------------------------------------------
 * /\
 * | leading
 *\/
 * ---
 *
 */
data class FontMetrics(
    val ascent: Float,
    val descent: Float,
    val leading: Float,
) {
    val textHeight = ascent + descent
    val lineHeight = ascent + descent + leading
}