package ru.casperix.multiplatform.font

data class FontWeight(val value:Int) {
	companion object {
		val LIGHT = FontWeight(200)
		val NORMAL = FontWeight(400)
		val BOLD = FontWeight(800)
	}
}

