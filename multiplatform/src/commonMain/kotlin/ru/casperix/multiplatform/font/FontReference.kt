package ru.casperix.multiplatform.font

data class FontReference(
    val name: String,
    val size: Int,
    val weight: FontWeight = FontWeight.NORMAL,
    val style: FontStyle = FontStyle.NORMAL,
    val leading: FontLeading = FontLeading.NORMAL,
)