package ru.casperix.multiplatform.font

enum class FontStyle {
	NORMAL,
	ITALIC,
}