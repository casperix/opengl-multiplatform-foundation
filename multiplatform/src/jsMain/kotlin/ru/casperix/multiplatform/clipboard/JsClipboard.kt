package ru.casperix.multiplatform.clipboard

import ru.casperix.multiplatform.asEitherFuture
import ru.casperix.multiplatform.navigator
import ru.casperix.signals.concrete.EitherFuture

actual val clipboard: ClipboardApi = JsClipboard()

class JsClipboard : ClipboardApi {

    override fun pushString(value: String): EitherFuture<Unit, Throwable> {
        return navigator.clipboard.writeText(value).asEitherFuture()
    }

    override fun popString(): EitherFuture<String, Throwable> {
        return navigator.clipboard.readText().asEitherFuture()
    }
}


