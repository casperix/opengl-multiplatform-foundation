package ru.casperix.multiplatform

import org.w3c.dom.Navigator
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.EitherSignal
import kotlin.js.Promise

public external val navigator: Navigator

fun <Output> Promise<Output>.asEitherFuture(): EitherFuture<Output, Throwable> {
    val signal = EitherSignal<Output, Throwable>()
    then({
        signal.accept(it)
    }, {
        signal.reject(it)
    })
    return signal
}