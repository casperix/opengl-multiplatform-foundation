package ru.casperix.multiplatform.util

import kotlinx.browser.document
import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.Image
import org.w3c.dom.ImageData
import ru.casperix.math.array.uint8.UByteArray3D
import ru.casperix.math.vector.int32.Vector3i
import ru.casperix.misc.toUByteArray
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.pixel_map.Rgba8PixelMap

@ExperimentalUnsignedTypes
object JsImageConverter {
    fun imageToPixelMap(image: Image, name: String): PixelMap? {
        val element = document.createElement("canvas") as HTMLCanvasElement
        element.width = image.width
        element.height = image.height
        val context = element.getContext("2d") as? CanvasRenderingContext2D
            ?: throw Exception("canvas not supported 2d")

        context.drawImage(image, 0.0, 0.0)
        return canvasToPixelMap(context, name).also {
            element.remove()
        }
    }

    fun canvasToPixelMap(context: CanvasRenderingContext2D, name: String): Rgba8PixelMap? {
        val width = context.canvas.width
        val height = context.canvas.height

        if (width <= 0 || height <= 0) {
            return null
        }
        val imageData = context.getImageData(0.0, 0.0, width.toDouble(), height.toDouble())
        val bytes = imageData.data.buffer.toUByteArray()
        return Rgba8PixelMap(UByteArray3D(bytes, Vector3i(width, height, 3)), name).apply {
            multiplyByAlpha()
        }
    }

    fun pixelMapToCanvas(pixelMap: PixelMap): HTMLCanvasElement {
        val element = document.createElement("canvas")
        val canvas = element as? HTMLCanvasElement ?: throw Error("Invalid canvas")

        canvas.width = pixelMap.width
        canvas.height = pixelMap.height

        val context = canvas.getContext("2d") as CanvasRenderingContext2D

        val jsImage = ImageData(pixelMap.width, pixelMap.height)//always 32 bits

        if (pixelMap is Rgba8PixelMap) {
            BytesHelper.copyBytes(jsImage.data, pixelMap.byteArray.data)
        } else throw Exception("Actual is ${pixelMap::class.simpleName}. But only ${Rgba8PixelMap::class.simpleName} supports now")

        context.putImageData(jsImage, .0, .0)
        return canvas
    }

}

