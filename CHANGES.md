1.1.2
- Save text / bytes
- Fix: Jvm: VSync
- Fix: Set invalid texture filter 
- Jvm: Support non-continuous rendering 
  
1.1.1
- Cache for load resources

1.1.0
- Logger used
- Fix: Redundant print
- PlatformMisc.debugKeyboard - removed

1.0.1
- Fix: memory leak (text-renderer)
- Fix: invalid texture config
- Load image error
- Fix: text rendering sometimes ignores new line
- More samples

1.0.0
- Clipboard support
- Improved text rendering

0.11.1
- fix blending

0.11.0
- android sample
- package refactor

0.10.0
- use package `ru.casperix`
- back to java 17