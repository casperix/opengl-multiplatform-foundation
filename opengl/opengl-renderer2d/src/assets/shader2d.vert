layout (location = 1) in vec2 aPos;

#ifdef VERTEX_TEXTURE_COORD
layout (location = 2) in vec2 aTexCoord;
out vec2 TexCoord;
#endif

#ifdef VERTEX_COLOR
layout (location = 3) in vec3 aVertexColor;
out vec3 VertexColor;
#endif

#ifdef VERTEX_OPACITY
layout (location = 4) in float aVertexOpacity;
out float VertexOpacity;
#endif

#ifdef USE_TANGENT_SPACE
layout (location = 5) in vec2 aTangent;
#endif

out vec3 Pos;
out vec3 ViewPos;

#ifdef USE_LIGHTING
uniform vec3 uLightPos[NUM_LIGHTS];
out vec3 LightPos[NUM_LIGHTS];
#endif//USE_LIGHTING

uniform mat3 uModel;
uniform mat3 uProjectionViewModel;
uniform vec3 uViewDir;


void main()
{
    vec3 localPosH = vec3(aPos, 1.0);
    vec3 worldPosH = uModel * localPosH;
    vec3 worldPos = vec3(worldPosH.x / worldPosH.z, worldPosH.y / worldPosH.z, 0.0);

    #ifdef USE_TANGENT_SPACE
    mat3 normalMatrix = transpose(inverse(uModel));
    vec3 T = normalize(normalMatrix * vec3(aTangent, 0.0));
    vec3 N = normalize(normalMatrix * vec3(0.0, 0.0, 1.0));
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);

    mat3 TBN = transpose(mat3(T, B, N));

    Pos = TBN * worldPos;
    ViewPos = TBN * (worldPos - uViewDir);

    #ifdef USE_LIGHTING
    for (int l = 0; l < NUM_LIGHTS; l++) {
        LightPos[l] = TBN * uLightPos[l];
    }
    #endif

    #else

    #ifdef USE_LIGHTING
    for (int l = 0; l < NUM_LIGHTS; l++) {
        LightPos[l] = uLightPos[l];
    }
    #endif

    Pos = worldPos;
    ViewPos = worldPos - uViewDir;
    #endif

    gl_Position = vec4(uProjectionViewModel * localPosH, 1.0);

    #ifdef VERTEX_TEXTURE_COORD
    TexCoord = aTexCoord;
    #endif

    #ifdef VERTEX_COLOR
    VertexColor = aVertexColor;
    #endif

    #ifdef VERTEX_OPACITY
    VertexOpacity = aVertexOpacity;
    #endif

}