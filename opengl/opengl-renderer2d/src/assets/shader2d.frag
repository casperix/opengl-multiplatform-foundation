uniform vec3 uAmbientColor;

/**
*   Base color source (no light, no shading)
*/
#ifdef MATERIAL_COLOR_CONST
uniform vec3 uMaterialColorConst;
#endif
#ifdef MATERIAL_COLOR_TEXTURE
uniform sampler2D uMaterialColorTexture;
#endif

/**
*   Base opacity source
*/
#ifdef MATERIAL_OPACITY_CONST
uniform float uMaterialOpacityConst;
#endif
#ifdef MATERIAL_OPACITY_TEXTURE
uniform sampler2D uMaterialOpacityTexture;
uniform int uMaterialOpacityTextureChannelOffset;
#endif

/**
*   Albedo source
*/
#ifdef MATERIAL_ALBEDO_CONST
uniform vec3 uMaterialAlbedoConst;
#endif
#ifdef MATERIAL_ALBEDO_TEXTURE
uniform sampler2D uMaterialAlbedoTexture;
#endif


/**
*   Ambient source
*/
#ifdef MATERIAL_AMBIENT_CONST
uniform vec3 uMaterialAmbientConst;
#endif
#ifdef MATERIAL_AMBIENT_TEXTURE
uniform sampler2D uMaterialAmbientTexture;
#endif

/**
*   Normal source
*/
#ifdef MATERIAL_NORMAL_CONST
uniform vec3 uMaterialNormalConst;
#endif
#ifdef MATERIAL_NORMAL_TEXTURE
uniform sampler2D uMaterialNormalTexture;
#endif

/**
*   Specular source
*/
#ifdef MATERIAL_SPECULAR_CONST
uniform vec3 uMaterialSpecularConst;
#endif
#ifdef MATERIAL_SPECULAR_TEXTURE
uniform sampler2D uMaterialSpecularTexture;
#endif


/**
*   Shines source
*/
#ifdef MATERIAL_SHINES_CONST
uniform float uMaterialShinesConst;
#endif
#ifdef MATERIAL_SHINES_TEXTURE
uniform sampler2D uMaterialShinesTexture;
uniform int uMaterialShinesTextureChannelOffset;
#endif

/**
*   Multi-tiles
*/
#ifdef MATERIAL_TILE_MAP
uniform sampler2D uMaterialTileMapTexture;
uniform vec2 uMaterialTexScale;
uniform int uMaterialSmoothMode;
uniform mediump sampler2DArray uMaterialTileTypesTextureArray;
#endif

/**
*   Other
*/
#ifdef VERTEX_TEXTURE_COORD
in vec2 TexCoord;
#endif

#ifdef VERTEX_COLOR
in vec3 VertexColor;
#endif

#ifdef VERTEX_OPACITY
in float VertexOpacity;
#endif

in vec3 Pos;
in vec3 ViewPos;

#ifdef USE_LIGHTING
uniform vec3 uLightAmbientColor[NUM_LIGHTS];
uniform vec3 uLightDiffuseColor[NUM_LIGHTS];
uniform vec3 uLightSpecularColor[NUM_LIGHTS];

in vec3 LightPos[NUM_LIGHTS];
#endif//USE_LIGHTING


out vec4 FragColor;

int getBestIndex(ivec4 tiles, vec4 weights) {
    float c1 = dot(weights, vec4(equal(tiles, ivec4(tiles[0]))));
    float c2 = dot(weights, vec4(equal(tiles, ivec4(tiles[1]))));
    float c3 = dot(weights, vec4(equal(tiles, ivec4(tiles[2]))));
    float c4 = dot(weights, vec4(equal(tiles, ivec4(tiles[3]))));

    if (c1 >= c2 && c1 >= c3 && c1 >= c4) return 0;
    if (c2 >= c1 && c2 >= c3 && c2 >= c4) return 1;
    if (c3 >= c1 && c3 >= c2 && c3 >= c4) return 2;
    if (c4 >= c1 && c4 >= c2 && c4 >= c3) return 3;

    return 0;
}

#ifdef MATERIAL_TILE_MAP
ivec4 getTileType(vec2 texCoord) {
    return ivec4(255.0 * texture(uMaterialTileMapTexture, texCoord));
}
#endif

float weight(vec2 offset) {
    return 1.0 / length(offset);
}

vec3 getNormalFromColor(vec3 normalColor) {
    vec3 normal = normalize(normalColor * 2.0 - 1.0);
    //  yDown = true
    return vec3(normal.x, -normal.y, normal.z);
}

int tileTypeHash(ivec4 source) {
    return source.x + source.y * 256 + source.z * 65536;
}

vec4 multiplyByAlpha(vec4 color) {
    return vec4(color.rgb * color.a, color.a);
}

void main() {
    vec4 temp = vec4(0.0, 0.0, 0.0, 1.0);
    vec3 baseColor = vec3(1.0);
    float baseOpacity = 1.0;
    vec2 texScale = vec2(1.0);

    /**
    *   TILE TYPES
    */
    #ifdef MATERIAL_TILE_MAP
    vec2 tileMapSize = vec2(textureSize(uMaterialTileMapTexture, 0));
    vec2 P = fract(TexCoord * tileMapSize + vec2(0.5));

    vec2 weightMin = vec2(1.0) - P;
    vec2 weightMax = P;
    if (uMaterialSmoothMode == 2) {
        weightMin = smoothstep(vec2(0, 0), vec2(1, 1), weightMin);
        weightMax = smoothstep(vec2(0, 0), vec2(1, 1), weightMax);
    }

    float wA = weightMin.x * weightMin.y;
    float wB = weightMax.x * weightMin.y;
    float wC = weightMin.x * weightMax.y;
    float wD = weightMax.x * weightMax.y;

    float dtex = 0.5 / tileMapSize.x;
    int albedoIndex = 0;
    int normalIndex = 0;
    int specularIndex = 0;
    if (uMaterialSmoothMode == 0) {
        ivec4 tile = getTileType(TexCoord);
        albedoIndex = tile.x;
        normalIndex = tile.y;
        specularIndex = tile.z;
    } else if (uMaterialSmoothMode == 1 || uMaterialSmoothMode == 2) {
        ivec4 tileA = getTileType(TexCoord + vec2(-dtex, -dtex));
        ivec4 tileB = getTileType(TexCoord + vec2(dtex, -dtex));
        ivec4 tileC = getTileType(TexCoord + vec2(-dtex, dtex));
        ivec4 tileD = getTileType(TexCoord + vec2(dtex, dtex));

        vec4 weights = vec4(wA, wB, wC, wD);
        ivec4 hashList = ivec4(tileTypeHash(tileA), tileTypeHash(tileB), tileTypeHash(tileC), tileTypeHash(tileD));
        int tileIndex = getBestIndex(hashList, weights);

        ivec4 albedoList = ivec4(tileA.x, tileB.x, tileC.x, tileD.x);
        ivec4 normalList = ivec4(tileA.y, tileB.y, tileC.y, tileD.y);
        ivec4 specularList = ivec4(tileA.z, tileB.z, tileC.z, tileD.z);

        albedoIndex = albedoList[tileIndex];
        normalIndex = normalList[tileIndex];
        specularIndex = specularList[tileIndex];
    }

    //  TODO: ambient
    int ambientIndex = albedoIndex;
    //  TODO: shines
    int shinesIndex = specularIndex;
    texScale = uMaterialTexScale * tileMapSize;
    #endif

    /**
    *   COLOR
    */
    #ifdef MATERIAL_COLOR_CONST
    baseColor *= uMaterialColorConst;
    #endif
    #ifdef MATERIAL_COLOR_TEXTURE
    baseColor *= texture(uMaterialColorTexture, TexCoord * texScale).rgb;
    #endif
    #ifdef VERTEX_COLOR
    baseColor *= VertexColor;
    #endif

    /**
    *   OPACITY
    */
    #ifdef MATERIAL_OPACITY_CONST
    baseOpacity *= uMaterialOpacityConst;
    #endif
    #ifdef MATERIAL_OPACITY_TEXTURE
    baseOpacity *= texture(uMaterialOpacityTexture, TexCoord * texScale)[uMaterialOpacityTextureChannelOffset];
    #endif
    #ifdef VERTEX_OPACITY
    baseOpacity *= VertexOpacity;
    #endif

    /**
    *   ALBEDO
    */
    #ifdef MATERIAL_ALBEDO_CONST
    baseColor *= uMaterialAlbedoConst;
    #endif
    #ifdef MATERIAL_ALBEDO_TEXTURE
    baseColor *= texture(uMaterialAlbedoTexture, TexCoord * texScale).rgb;
    #endif
    #ifdef MATERIAL_ALBEDO_TEXTURE_ARRAY
    baseColor *= texture(uMaterialTileTypesTextureArray, vec3(TexCoord * texScale, albedoIndex)).rgb;
    #endif

    /**
    *   AMBIENT
    */
    vec3 ambientColor = vec3(0.0);
    #ifdef MATERIAL_AMBIENT_CONST
    ambientColor = uMaterialAmbientConst;
    #endif
    #ifdef MATERIAL_AMBIENT_TEXTURE
    ambientColor = texture(uMaterialAmbientTexture, TexCoord * texScale).rgb;
    #endif
    #ifdef MATERIAL_AMBIENT_TEXTURE_ARRAY
    ambientColor = texture(uMaterialTileTypesTextureArray, vec3(TexCoord * texScale, ambientIndex)).rgb;
    #endif

    /**
    *   NORMAL
    */
    vec3 normal = vec3(0.0, 0.0, 1.0);
    #ifdef MATERIAL_NORMAL_CONST
    normal = uMaterialNormalConst;
    #endif
    #ifdef MATERIAL_NORMAL_TEXTURE
    vec3 normalColor = texture(uMaterialNormalTexture, TexCoord * texScale).rgb;
    normal = getNormalFromColor(normalColor);
    #endif
    #ifdef MATERIAL_NORMAL_TEXTURE_ARRAY
    vec3 normalColor = texture(uMaterialTileTypesTextureArray, vec3(TexCoord * texScale, normalIndex)).rgb;
    normal = getNormalFromColor(normalColor);
    #endif

    /**
    *   SPECULAR
    */
    vec3 specularColor = vec3(0.5);

    #ifdef MATERIAL_SPECULAR_CONST
    specularColor = uMaterialSpecularConst;
    #endif
    #ifdef MATERIAL_SPECULAR_TEXTURE
    specularColor = texture(uMaterialSpecularTexture, TexCoord * texScale).rgb;
    #endif
    #ifdef MATERIAL_SPECULAR_TEXTURE_ARRAY
    specularColor = texture(uMaterialTileTypesTextureArray, vec3(TexCoord * texScale, specularIndex)).rgb;
    #endif

    /**
    *   SHINES
    */
    float specularShines = 32.0;
    #ifdef MATERIAL_SHINES_CONST
    specularShines = uMaterialShinesConst;
    #endif
    #ifdef MATERIAL_SHINES_TEXTURE
    float specularShinesRaw = texture(uMaterialShinesTexture, TexCoord * texScale)[uMaterialShinesTextureChannelOffset];
    specularShines = 1.0 + specularShinesRaw * 255.0;
    #endif
    #ifdef MATERIAL_SHINES_TEXTURE_ARRAY
    //  TODO: extract component with channel offset
    float specularShinesRaw = texture(uMaterialTileTypesTextureArray, vec3(TexCoord * texScale, shinesIndex)).a;
    specularShines = 1.0 + specularShinesRaw * 255.0;
    #endif


    /**
    *   SHADING
    */

    #ifdef MATERIAL_COLOR_ANY
    //  Color shading
    vec4 outputColor = vec4(baseColor, baseOpacity);
    #else
    //  Phong shading
    #ifdef USE_LIGHTING
    vec3 ambient = vec3(0.0);
    vec3 diffuse = vec3(0.0);
    vec3 specular = vec3(0.0);

    for(int l = 0; l < NUM_LIGHTS; l++) {
        vec3 lightDirection = normalize(Pos - LightPos[l]);
        float lightness = max(0.0, dot(normal, -lightDirection));

        ambient += ambientColor * uLightAmbientColor[l];
        diffuse += baseColor * lightness * uLightDiffuseColor[l];

        #if defined(MATERIAL_SPECULAR_ANY) || defined(MATERIAL_SHINES_ANY)
        vec3 viewDirection = normalize(Pos - ViewPos);

        #ifdef LIGHT_MODEL_PHONG
        vec3 reflectDir = reflect(lightDirection, normal);
        float specularFactor = pow(max(dot(-viewDirection, reflectDir), 0.0), specularShines);
        #endif

        #ifdef LIGHT_MODEL_BLINN_PHONG
        vec3 halfwayDirection = normalize(lightDirection + viewDirection);
        float specularFactor = pow(max(dot(normal, -halfwayDirection), 0.0), specularShines);
        #endif

        specular += specularColor * specularFactor * uLightSpecularColor[l];
        #endif
    }
    vec3 lightDirection = normalize(Pos - LightPos[0]);
//    vec4 outputColor = vec4(lightDirection, baseOpacity);

    vec4 outputColor = vec4((ambient + diffuse + specular), baseOpacity);
    #endif//USE_LIGHTING
    #endif//MATERIAL_COLOR_ANY

    /**
    *   POST PROCESS
    */
    #ifdef NEED_ALPHA_MULTIPLIER
    outputColor = multiplyByAlpha(outputColor);
    #endif


    #ifdef MATERIAL_TILE_GIRD
    vec2 gird = fract(TexCoord * tileMapSize);
    float border = 0.02;
    if (abs(gird.x) < border || abs(gird.y) < border || abs(gird.x) > 1.0 - border || abs(gird.y) > 1.0 - border) {
        outputColor.rgb = vec3(0.0);
    }
    #endif

    #ifdef CONFIG_DISCARD_ALPHA
    if (outputColor.a <= 0.01) {
        discard;
    }
    #endif

    #ifdef CONFIG_GAMMA_CORRECTION
    float gamma = 2.2;
    outputColor.rgb = pow(outputColor.rgb, vec3(1.0 / gamma));
    #endif

    FragColor = outputColor;
}