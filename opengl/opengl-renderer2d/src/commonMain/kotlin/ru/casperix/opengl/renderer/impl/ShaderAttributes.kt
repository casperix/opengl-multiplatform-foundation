package ru.casperix.opengl.renderer.impl


data class ShaderAttributes(
    //  Common
    val numLights:Int = 0,
    val hasVertexColor: Boolean = false,
    val hasVertexOpacity: Boolean = false,
    val needAlphaMultiplier: Boolean = true,

    //  CommonMaterial
    val materialOpacitySource: ShaderSourceAttribute? = null,

    //  SimpleMaterial
    val materialColorSource: ShaderSourceAttribute? = null,

    //  PhongMaterial
    val materialAlbedoSource: ShaderSourceAttribute? = null,
    val materialNormalSource: ShaderSourceAttribute? = null,
    val materialSpecularSource: ShaderSourceAttribute? = null,
    val materialShinesSource: ShaderSourceAttribute? = null,
    val materialAmbientSource: ShaderSourceAttribute? = null,

    //  TileMapMaterial
    val hasMaterialTileMap: Boolean = false,
    val hasMaterialTileGird: Boolean = false,

    val lightModel:ShaderLightModel = ShaderLightModel.BLINN_PHONG,
) {

    val useTangentSpace = materialNormalSource == ShaderSourceAttribute.TEXTURE

    val hasVertexTextureCoord = hasMaterialTileMap ||
            materialOpacitySource == ShaderSourceAttribute.TEXTURE ||
            materialColorSource == ShaderSourceAttribute.TEXTURE ||
            materialAlbedoSource == ShaderSourceAttribute.TEXTURE ||
            materialNormalSource == ShaderSourceAttribute.TEXTURE ||
            materialSpecularSource == ShaderSourceAttribute.TEXTURE ||
            materialShinesSource == ShaderSourceAttribute.TEXTURE ||
            materialAmbientSource == ShaderSourceAttribute.TEXTURE
}


