package ru.casperix.opengl.renderer.impl

import ru.casperix.opengl.core.GLLocation
import ru.casperix.opengl.core.GLuint
import ru.casperix.opengl.core.glGetUniformLocation

class DeviceShader(val handle: GLuint, val vertexShader: String, val fragmentShader: String) {

    fun getLocation(name: String): GLLocation? {
        return glGetUniformLocation(handle, name)
    }
}