package ru.casperix.opengl.renderer

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.opengl.core.glUseProgram
import ru.casperix.opengl.renderer.impl.MaterialBinder
import ru.casperix.opengl.renderer.shader.ShaderController
import ru.casperix.renderer.light.DirectionLight
import ru.casperix.renderer.light.Light
import ru.casperix.renderer.light.LightColors
import ru.casperix.renderer.light.PointLight
import ru.casperix.renderer.material.Material

class StateController() {
    private var controller: ShaderController? = null
    private var geometry: DeviceGeometryData? = null
    private var material: Material? = null
    private var model: Matrix3f = Matrix3f.IDENTITY
    private var projectionViewModel: Matrix3f = Matrix3f.IDENTITY

    private val materialBinder = MaterialBinder()

    fun setShader(value: ShaderController?) {
        if (controller == value) return
        controller = value

        setMaterial(null)
        dropTransform()

        if (value != null) {
            value.shader.bind()
        } else {
            glUseProgram(0)
        }
    }

    fun dropTransform() {
        model = Matrix3f.IDENTITY
        projectionViewModel = Matrix3f.IDENTITY
    }

    fun setMaterial(value: Material?) {
        val controller = controller ?: return

        if (material == value) return
        material = value

        if (value != null) {
            materialBinder.set(controller, value)
        } else {
            materialBinder.unset()
        }
    }

    fun setTransform(model: Matrix3f, projectionViewModel: Matrix3f) {
        val controller = controller ?: return

        if (this.projectionViewModel == projectionViewModel) return
        this.projectionViewModel = projectionViewModel
        this.model = model

        controller.uProjectionViewModel?.set(projectionViewModel)
        controller.uModel?.set(model)
    }

    fun setGeometry(value: DeviceGeometryData?) {
        if (geometry == value) return
        geometry = value

        if (value != null) {
            value.bind()
        } else {
            DeviceGeometryData.unbind()
        }
    }



    fun setViewDirection(value: Vector3f) {
        controller?.uViewDir?.set(value)
    }

    fun setLight(index: Int, light: Light) {
        controller?.lights?.getOrNull(index)?.setLight(light)
    }
}
