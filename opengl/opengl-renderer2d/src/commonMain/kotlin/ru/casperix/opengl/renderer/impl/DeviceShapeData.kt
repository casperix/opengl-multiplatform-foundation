package ru.casperix.opengl.renderer.impl

import ru.casperix.misc.Disposable
import ru.casperix.opengl.renderer.DeviceGeometryBuffer
import ru.casperix.opengl.renderer.shader.ShaderController
import ru.casperix.renderer.vector.VectorGraphic
import ru.casperix.renderer.vector.VectorShape


class DeviceShapeData(
    val graphic: VectorShape,
    val shader: ShaderController,
    val buffer: DeviceGeometryBuffer,
) : Disposable {


    var isDisposed = false; private set

    override fun dispose() {
        isDisposed = true
        buffer.dispose()
    }

}