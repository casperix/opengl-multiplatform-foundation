package ru.casperix.opengl.renderer.impl

enum class ShaderLightModel {
    PHONG,
    BLINN_PHONG,
}