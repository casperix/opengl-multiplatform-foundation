package ru.casperix.opengl.renderer

import ru.casperix.opengl.core.GL_LINEAR
import ru.casperix.opengl.core.GL_NEAREST_MIPMAP_LINEAR
import ru.casperix.opengl.renderer.impl.ShaderLightModel

data class OpenGlRendererConfig(
    val lightMaxAmount: Int = 4,
    val lightModel: ShaderLightModel = ShaderLightModel.BLINN_PHONG,
    val lightGammaCorrection: Boolean = false,

    val alphaPremultiplied: Boolean = true,
    val alphaSeparate: Boolean = false,
    val alphaDiscard: Boolean = true,

    val geometryCullFace: Boolean = false,

    val textureMinFilter: Int = GL_NEAREST_MIPMAP_LINEAR,
    val textureMagFilter: Int = GL_LINEAR,

    val cacheMaxDrawBufferIndices: Int = 1000,
    val cacheAccumulateBatches: Boolean = true,

    val cacheStaticUsing: Boolean = true,
    val cacheStaticItemMinIndices: Int = 1000,
    val cacheStaticSummaryIndicesMax: Int = 2_000_000,
    val cacheStaticSummaryIndicesMin: Int = 1_500_000,
)
