package ru.casperix.opengl.renderer.impl

enum class VertexAttributeLayout(val size: Int, val layout: Int) {
    POSITION2(2, 1),
    POSITION3(3, 1),
    TEXTURE_COORD(2, 2),
    RGB(3, 3),
    OPACITY(1, 4),
    TANGENT(2, 5),
}
