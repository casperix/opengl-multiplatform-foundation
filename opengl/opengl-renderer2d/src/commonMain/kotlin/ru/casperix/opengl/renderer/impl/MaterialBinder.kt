package ru.casperix.opengl.renderer.impl

import ru.casperix.opengl.core.*
import ru.casperix.opengl.renderer.TextureProvider
import ru.casperix.opengl.renderer.exp.TileMapMaterial
import ru.casperix.opengl.renderer.shader.ShaderController
import ru.casperix.opengl.renderer.shader.ShaderSourceController
import ru.casperix.opengl.renderer.shader.ShaderUniform
import ru.casperix.opengl.renderer.texture.GLTexture
import ru.casperix.renderer.material.*

@ExperimentalUnsignedTypes
class MaterialBinder {
    private val textureBinder = TextureProvider()
    private var textureLayers = Array<TextureLayer?>(80) { null }

    data class TextureLayer(val texture: GLTexture, val shader: ShaderUniform)

    fun set(controller: ShaderController, material: Material) {
        (material as? SimpleMaterial)?.apply {
            setSource(colorSource, controller.uMaterialColor, 0)
            setSource(opacitySource, controller.uMaterialOpacity,1)
        }
        (material as? PhongMaterial)?.apply {
            setSource(diffuseSource, controller.uMaterialAlbedo, 2)
            setSource(opacitySource, controller.uMaterialOpacity, 3)
            setSource(normalSource, controller.uMaterialNormal, 4)
            setSource(specularSource, controller.uMaterialSpecular, 5)
            setSource(shinesSource, controller.uMaterialShines, 6)
            setSource(ambientSource, controller.uMaterialAmbient, 7)
        }
        (material as? TileMapMaterial)?.apply {
            setTexture(controller.uMaterialTileTypesTextureArray, 8, textureBinder.getTexture(tileTypesArray))
            setTexture(controller.uMaterialTileMapTexture, 9, textureBinder.getTexture(tileMap))

            controller.uMaterialTexScale?.set(texScale)
            controller.uMaterialSmoothMode?.set(smooth.ordinal)
        }
    }

    private fun setSource(source: ColorSource?, sourceController:  ShaderSourceController, textureChannel: Int) {
        if (source is TextureColorSource) {
            setTexture(sourceController.Texture, textureChannel, textureBinder.getTexture(source.value))
            sourceController.TextureChannelOffset?.set(source.channelOffset)
        } else if (source is ConstantColorSource) {
            sourceController.Const?.set(source.value)
        }
    }

    private fun setSource(source: Vector1Source?, sourceController:  ShaderSourceController, textureChannel: Int) {
        if (source is TextureVector1Source) {
            setTexture(sourceController.Texture, textureChannel, textureBinder.getTexture(source.value))
            sourceController.TextureChannelOffset?.set(source.channelOffset)
        } else if (source is ConstantVector1Source) {
            sourceController.Const?.set(source.value)
        }
    }

    private fun setSource(source: Vector2Source?, sourceController:  ShaderSourceController, textureChannel: Int) {
        if (source is TextureVector2Source) {
            setTexture(sourceController.Texture, textureChannel, textureBinder.getTexture(source.value))
            sourceController.TextureChannelOffset?.set(source.channelOffset)
        } else if (source is ConstantVector2Source) {
            sourceController.Const?.set(source.value)
        }
    }

    private fun setSource(source: Vector3Source?, sourceController:  ShaderSourceController, textureChannel: Int) {
        if (source is TextureVector3Source) {
            setTexture(sourceController.Texture, textureChannel, textureBinder.getTexture(source.value))
            sourceController.TextureChannelOffset?.set(source.channelOffset)
        } else if (source is ConstantVector3Source) {
            sourceController.Const?.set(source.value)
        }
    }

    private fun setTexture(textureLocation: ShaderUniform?, textureChannel: Int, texture: GLTexture) {
        if (textureLocation == null) {
            return
        }
        if (textureChannel < 0 || textureChannel >= 80) {
            throw Error("Texture layer must be positive and smaller than 80")
        }

        val layer = TextureLayer(texture, textureLocation)

        if (textureLayers[textureChannel] == layer) return
        textureLayers[textureChannel] = layer

        textureLocation.set(textureChannel, texture)
    }

    fun unset() {
        textureLayers.forEachIndexed { channel, textureLayer ->
            if (textureLayer != null) {
                glActiveTexture(GL_TEXTURE0 + channel)
                glBindTexture(GL_TEXTURE_2D, 0)
                glBindTexture(GL_TEXTURE_2D_ARRAY, 0)
                glBindTexture(GL_TEXTURE_CUBE_MAP, 0)
                textureLayers[channel] = null
            }
        }
    }
}