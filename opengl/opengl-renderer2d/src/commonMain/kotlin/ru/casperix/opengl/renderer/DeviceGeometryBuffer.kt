package ru.casperix.opengl.renderer

import ru.casperix.math.array.float32.FloatArrayBuilder
import ru.casperix.math.array.float32.UIntArrayBuilder
import ru.casperix.misc.Disposable

class DeviceGeometryBuffer(val data: DeviceGeometryData, val isStatic: Boolean) : Disposable {
    private val indicesBuilder = UIntArrayBuilder()
    private val verticesBuilder = FloatArrayBuilder()

    var lastFrame = 0L

    override fun dispose() {
        data.dispose()
    }


    fun appendData(nextVertices: FloatArray, nextIndices: UIntArray) {
        //  TODO: is CRITICAL place by performance
        val indexOffset = verticesBuilder.length / data.vertexSize
        verticesBuilder.append(nextVertices)
        indicesBuilder.append(UIntArray(nextIndices.size) { indexOffset.toUInt() + nextIndices[it] })
    }

    fun reset() {
        indicesBuilder.length = 0
        verticesBuilder.length = 0

        data.reset()
    }

    fun uploadData(stateController: StateController) {
        data.uploadData(stateController, verticesBuilder.build(), indicesBuilder.build(), false)
    }

    fun draw() {
        data.draw()
    }

}