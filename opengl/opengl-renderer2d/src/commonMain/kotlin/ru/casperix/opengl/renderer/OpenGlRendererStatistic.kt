package ru.casperix.opengl.renderer

import ru.casperix.misc.time.getTime
import kotlin.time.Duration


class FrameStatistic(
    var frameIndex: Long,
    var frameTime: Duration,
    var frameTick: Duration,
    var triangles: Int = 0,
    var batches: Int = 0,
    var states: Int = 0,
    var static: Int = 0,
    var dynamicBufferAmount: Int = 0,
    var staticBufferAmount: Int = 0,
)

class OpenGlRendererStatistic {
    var frameIndex = 0L

    var current = FrameStatistic(frameIndex, Duration.ZERO, Duration.ZERO)
    var last = FrameStatistic(frameIndex, Duration.ZERO, Duration.ZERO)

    fun nextFrame() {
        last = current
        val currentTime = getTime()
        val currentTick = currentTime - last.frameTime
        current = FrameStatistic(++frameIndex, currentTime, currentTick)
    }
}
