package ru.casperix.opengl.renderer.texture

import ru.casperix.opengl.core.glGenTextures


object GLTextureHandlerProvider {
    private var usage: Int = 0
    private var lastBuffer = IntArray(2)
    private var readIndex = 0

    init {
        glGenTextures(lastBuffer)
    }

    fun next(): Int {
        if (length <= readIndex) {
            length = readIndex + 1
        }
        return lastBuffer[readIndex++]
    }

    var length: Int
        get() = usage
        set(value) {
            if (value > lastBuffer.size) {
                val nextBuffer = IntArray(lastBuffer.size * 2)

                val generatedPart = IntArray(lastBuffer.size)
                glGenTextures(generatedPart)

                lastBuffer.copyInto(nextBuffer, 0)
                generatedPart.copyInto(nextBuffer, lastBuffer.size)

                lastBuffer = nextBuffer
            }
            usage = value
        }
}