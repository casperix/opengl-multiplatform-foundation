package ru.casperix.opengl.renderer.texture

import io.github.oshai.kotlinlogging.KotlinLogging
import ru.casperix.opengl.core.*
import ru.casperix.opengl.renderer.texture.GLTexture.Companion.asGLMagFilter
import ru.casperix.opengl.renderer.texture.GLTexture.Companion.asGLMinFilter
import ru.casperix.opengl.renderer.texture.GLTexture.Companion.asGLWrap
import ru.casperix.renderer.material.TextureCube
import ru.casperix.renderer.pixel_map.PixelMap

class GLTextureCube(val texture: TextureCube) : GLTexture {
    val logger = KotlinLogging.logger { }
    val handle = GLTextureHandlerProvider.next()

//	constructor(colorProvider: ColorCube) : this(colorProvider.minX, colorProvider.maxX, colorProvider.minY, colorProvider.maxY, colorProvider.minZ, colorProvider.maxZ)

    init {
        glBindTexture(GL_TEXTURE_CUBE_MAP, handle)

        texture.apply {
            loadFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, minX)
            loadFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X, maxX)
            loadFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, minY)
            loadFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, maxY)
            loadFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, minZ)
            loadFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, maxZ)
        }
        texture.config.apply {
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, asGLWrap(uWrap))
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, asGLWrap(vWrap))
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, asGLWrap(wWrap))
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, asGLMinFilter(minFilter, useMipMap))
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, asGLMagFilter(magFilter, useMipMap))
        }
        logger.debug { "Texture created: ${toShortString()}"  }
    }

    override fun dispose() {
        glDeleteTexture(handle)

        logger.debug { "Texture disposed: ${toShortString()}"  }
    }

    private fun toShortString(): String {
        return "id: $handle"
    }

    private fun loadFace(key: Int, pixelMap: PixelMap) {
        glTexImage2D(
            key,
            0,
            GL_RGBA,
            pixelMap.width,
            pixelMap.height,
            0,
            GL_RGBA,
            GL_UNSIGNED_BYTE,
            pixelMap.byteArray.data.toByteArray()
        )
    }

    override fun bind(channel: Int) {
        glActiveTexture(GL_TEXTURE0 + channel)
        glBindTexture(GL_TEXTURE_CUBE_MAP, handle)
    }
}