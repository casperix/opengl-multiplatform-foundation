package ru.casperix.opengl.renderer.shader

class ShaderSourceController(prefix:String, shader: ShaderBuffer) {
    val Const = ShaderUniform.from(shader, "${prefix}Const")
    val Texture = ShaderUniform.from(shader, "${prefix}Texture")
    val TextureChannelOffset = ShaderUniform.from(shader, "${prefix}TextureChannelOffset")
}