package ru.casperix.opengl.renderer

import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.opengl.core.*
import ru.casperix.renderer.RendererEnvironment
import ru.casperix.renderer.light.Light

class OpenGlRendererEnvironment(val flush: () -> Unit) : RendererEnvironment {
    override var clearColor = Color.WHITE.toRGBA()

    override var lights: List<Light> = emptyList()
        set(value) {
            field = value
            flush()
        }

    override var projectionMatrix = Matrix3f.IDENTITY
        set(value) {
            field = value
            flush()
        }
    override var viewMatrix = Matrix3f.IDENTITY
        set(value) {
            field = value
            flush()
        }
    override var scissor: Box2i? = null
        set(area) {
            if (field == area) return

            flush()

            field = if (area == null) {
                glDisable(GL_SCISSOR_TEST)
                null
            } else {
                val clamped = area//Box2i(area.min.clamp(Vector2i.ZERO, viewportMax), area.max.lower(viewportMax))

                glScissor(clamped.min.x, viewPort.height - clamped.max.y - 1, clamped.dimension.x, clamped.dimension.y)
                glEnable(GL_SCISSOR_TEST)
                clamped
            }

        }

    override var viewPort = Dimension2i.ZERO
        set(value) {
            flush()
            glViewport(0, 0, value.width, value.height)
            field = value
        }

}