package ru.casperix.opengl.renderer.impl

import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.opengl.renderer.DeviceGeometryBuffer
import ru.casperix.opengl.renderer.shader.ShaderController
import ru.casperix.renderer.material.Material

data class RenderState(
    val model: Matrix3f,
    val projectionViewModel: Matrix3f,
    val viewDirection:Vector3f,
    val controller: ShaderController,
    val material: Material,
    val buffer: DeviceGeometryBuffer
)