package ru.casperix.opengl.renderer.impl

enum class ShaderSourceAttribute {
    CONST,
    TEXTURE,
    TEXTURE_ARRAY,
}