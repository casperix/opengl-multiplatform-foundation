package ru.casperix.opengl.renderer.exp

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.material.Material
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.Texture2DArray
import ru.casperix.renderer.vector.vertex.ColorFormat
import ru.casperix.renderer.vector.vertex.VertexAttributes

enum class TileSmooth {
    NONE,
    LINEAR,
    SMOOTHEST,
}

data class TileMapMaterial(val tileMap:Texture2D, val tileTypesArray: Texture2DArray, val smooth: TileSmooth, val texScale:Vector2f = Vector2f(1f), val gird:Boolean = false) : Material {
    override fun createVertexAttributes(position2d: Boolean, vertexColorFormat: ColorFormat?, hasVertexOpacity:Boolean): VertexAttributes {
        return VertexAttributes(true, true, true, false, vertexColorFormat, hasVertexOpacity)
    }

}