package ru.casperix.opengl.renderer.texture

import ru.casperix.misc.Disposable
import ru.casperix.opengl.core.*
import ru.casperix.renderer.material.TextureFilter
import ru.casperix.renderer.material.TextureWrap

interface GLTexture : Disposable {
    fun bind(channel: Int)


    companion object {
        fun asGLMinFilter(filter: TextureFilter, hasMipMap: Boolean): Int {
            return when (filter) {
                TextureFilter.NEAREST -> if (hasMipMap) GL_NEAREST_MIPMAP_NEAREST else GL_NEAREST
                TextureFilter.LINEAR -> if (hasMipMap) GL_LINEAR_MIPMAP_LINEAR else GL_LINEAR
            }
        }
        fun asGLMagFilter(filter: TextureFilter, hasMipMap: Boolean): Int {
            return when (filter) {
                TextureFilter.NEAREST -> GL_NEAREST
                TextureFilter.LINEAR -> GL_LINEAR
            }
        }

        fun asGLWrap(wrap: TextureWrap): Int {
            return when (wrap) {
                TextureWrap.CLAMP -> GL_CLAMP_TO_EDGE
                TextureWrap.REPEAT -> GL_REPEAT
            }
        }
    }
}