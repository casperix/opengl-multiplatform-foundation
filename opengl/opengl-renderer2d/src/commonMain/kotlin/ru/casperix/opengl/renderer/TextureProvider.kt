package ru.casperix.opengl.renderer

import ru.casperix.math.collection.LRUCache
import ru.casperix.opengl.renderer.texture.GLTexture
import ru.casperix.opengl.renderer.texture.GLTexture2D
import ru.casperix.opengl.renderer.texture.GLTexture2DArray
import ru.casperix.opengl.renderer.texture.GLTextureCube
import ru.casperix.renderer.material.Texture
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.Texture2DArray
import ru.casperix.renderer.material.TextureCube

class TextureProvider {
    private val smallTextureCache = LRUCache<Texture, GLTexture>(1024, false)
    private val mediumTextureCache = LRUCache<Texture, GLTexture>(256, false)
    private val largeTextureCache = LRUCache<Texture, GLTexture>(64, false)
    private val otherTextureCache = LRUCache<Texture, GLTexture>(32, false)

    private fun getTextureCache(texture: Texture): LRUCache<Texture, GLTexture> {
        return if (texture is Texture2D) {
            val volume = texture.map.dimension.volume()
            if (volume <= 16384) {
                smallTextureCache
            } else if (volume <= 65536) {
                mediumTextureCache
            } else {
                largeTextureCache
            }
        } else {
            otherTextureCache
        }
    }

    fun getTexture(texture: Texture): GLTexture {
        val cache = getTextureCache(texture)
        return cache.getOrPut(texture) {
            createTexture(texture)
        }.apply {
            cache.removeOldest()?.dispose()
        }
    }

    private fun createTexture(texture: Texture) = when (texture) {
        is Texture2D -> GLTexture2D(texture)
        is Texture2DArray -> GLTexture2DArray(texture)
        is TextureCube -> GLTextureCube(texture)
        else -> throw Exception("Unsupported texture type: ${texture::class.simpleName}")
    }
}