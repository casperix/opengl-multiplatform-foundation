package ru.casperix.opengl.renderer.shader

import ru.casperix.opengl.renderer.impl.ShaderAttributes

class ShaderController(val shader: ShaderBuffer, val attributes: ShaderAttributes) {
    //  SimpleMaterial
    val uMaterialColor = ShaderSourceController("uMaterialColor", shader)
    val uMaterialOpacity = ShaderSourceController("uMaterialOpacity", shader)

    //  PhongMaterial
    val uMaterialAmbient = ShaderSourceController("uMaterialAmbient", shader)
    val uMaterialAlbedo = ShaderSourceController("uMaterialAlbedo", shader)
    val uMaterialNormal = ShaderSourceController("uMaterialNormal", shader)
    val uMaterialSpecular = ShaderSourceController("uMaterialSpecular", shader)
    val uMaterialShines = ShaderSourceController("uMaterialShines", shader)

    //  TileMapMaterial
    val uMaterialTexScale = ShaderUniform.from(shader, "uMaterialTexScale")
    val uMaterialSmoothMode = ShaderUniform.from(shader, "uMaterialSmoothMode")
    val uMaterialTileTypesTextureArray = ShaderUniform.from(shader, "uMaterialTileTypesTextureArray")
    val uMaterialTileMapTexture = ShaderUniform.from(shader, "uMaterialTileMapTexture")

    //  Light
    val lights = (0 until  attributes.numLights).map {
        ShaderLightController(shader, it)
    }

    //  Common
    val uViewDir = ShaderUniform.from(shader, "uViewDir")
    val uModel = ShaderUniform.from(shader, "uModel")
    val uProjectionViewModel = ShaderUniform.from(shader, "uProjectionViewModel")
}

