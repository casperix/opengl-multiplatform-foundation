package ru.casperix.opengl.renderer.shader

import ru.casperix.renderer.light.DirectionLight
import ru.casperix.renderer.light.Light
import ru.casperix.renderer.light.LightColors
import ru.casperix.renderer.light.PointLight

class ShaderLightController(shader: ShaderBuffer, val lightIndex:Int) {
    val uLightType = ShaderUniform.from(shader, "uLightType[$lightIndex]")
    val uLightDir = ShaderUniform.from(shader, "uLightDir[$lightIndex]")
    val uLightPos = ShaderUniform.from(shader, "uLightPos[$lightIndex]")
    val uLightAmbientColor = ShaderUniform.from(shader, "uLightAmbientColor[$lightIndex]")
    val uLightDiffuseColor = ShaderUniform.from(shader, "uLightDiffuseColor[$lightIndex]")
    val uLightSpecularColor = ShaderUniform.from(shader, "uLightSpecularColor[$lightIndex]")

    fun setLight(light: Light)  {
        if (light is PointLight) {
            uLightType?.set(0f)
            uLightPos?.set(light.position)
            setLightColors(light.colors)
        } else if (light is DirectionLight) {
            uLightType?.set(1f)
            uLightDir?.set(light.direction)
            setLightColors(light.colors)
        }
    }

    private fun setLightColors(desc: LightColors)  {
        uLightAmbientColor?.set(desc.ambientColor)
        uLightDiffuseColor?.set(desc.diffuseColor)
        uLightSpecularColor?.set(desc.specularColor)
    }
}