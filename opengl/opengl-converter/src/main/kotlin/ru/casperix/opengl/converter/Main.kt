package ru.casperix.opengl.converter

import ru.casperix.opengl.converter.Config.API_CONVERTED
import ru.casperix.opengl.converter.Config.CONST_ONLY_CONVERTED
import ru.casperix.opengl.converter.Config.CPP_ORIGINAL
import ru.casperix.opengl.converter.Config.FUNCTION_ONLY_CONVERTED
import ru.casperix.opengl.converter.Config.JS_CONVERTED
import ru.casperix.opengl.converter.Config.JVM_CONVERTED
import ru.casperix.opengl.converter.Config.OUTPUT_DIRECTORY
import ru.casperix.misc.fold
import java.io.File
import java.nio.charset.Charset
import kotlin.io.path.*

fun main() {
    prepareDirectories()
    convertOriginalHeader()
    convertByLanguages()
}

@OptIn(ExperimentalPathApi::class)
fun prepareDirectories() {
    listOf(
        OUTPUT_DIRECTORY,
    ).map { pathName ->
        Path(pathName)
    }.forEach { path ->
        path.deleteRecursively()
        path.createDirectories()
    }
}

fun excludeMessage(line: String): String? {
    val excludePlatforms = mutableListOf<String>()
    commonExcludes.forEach { excludeName ->
        if (line.contains(excludeName)) return "Not implemented yet..."
    }

    jvmExcludes.forEach { excludeName ->
        if (line.contains(excludeName)) excludePlatforms += "JVM"
    }

    jsExcludes.forEach { excludeName ->
        if (line.contains(excludeName)) excludePlatforms += "JS"
    }

    if (excludePlatforms.isEmpty()) return null
    return "Not supported in " + excludePlatforms.joinToString(", ")
}

fun convertOriginalHeader() {
    val originalHeader = File(CPP_ORIGINAL).readLines()
    val convertedHeader = originalHeader.mapNotNull {
        convertHeaderLine(it)
    }.sorted()

    var constList = convertedHeader.filter { line -> line.startsWith("const") }
    constList = constList.flatMap { line -> addCommentIfExclude(line) }

    var funcList = convertedHeader.filter { line -> !line.startsWith("const") }
    funcList = funcList.flatMap { line -> addCommentIfExclude(line) }
    funcList = funcList.flatMap { line -> addMemoryBufferVariants(line) }
    funcList = funcList.flatMap { line -> addIntBufferVariants(line) }

    val functionBlock = funcList.joinToString("\n")
    val constBlock = constList.joinToString("\n")

    File(CONST_ONLY_CONVERTED).writeText(constBlock)
    File(FUNCTION_ONLY_CONVERTED).writeText(functionBlock)
    File(API_CONVERTED).writeText(COMMON_HEADER + CONST_HEADER + constBlock + FUNCTION_HEADER + functionBlock)
}

fun addCommentIfExclude(line: String): Iterable<String> {
    val errorMessage = excludeMessage(line)
    return if (errorMessage != null) {
        listOf("//$errorMessage:", "//$line")
    } else {
        listOf(line)
    }
}

fun addMemoryBufferVariants(line: String): Iterable<String> {
    val hasVoidArray = line.contains("voidArray")
    return if (!hasVoidArray) {
        listOf(line)
    } else {
        listOf(
            line.replace(Regex("(\\w+): voidArray"), "clientData: ByteArray"),
            line.replace(Regex("(\\w+): voidArray"), "clientData: ShortArray"),
            line.replace(Regex("(\\w+): voidArray"), "clientData: IntArray"),
            line.replace(Regex("(\\w+): voidArray"), "clientData: LongArray"),
            line.replace(Regex("(\\w+): voidArray"), "clientData: FloatArray"),
            line.replace(Regex("(\\w+): voidArray"), "clientData: DoubleArray"),
        )
    }
}

fun addIntBufferVariants(line: String): Iterable<String> {
    val hasVoidArray = line.contains("GLvoidArray")
    return if (!hasVoidArray) {
        listOf(line)
    } else {
        listOf(
            line.replace(Regex("(\\w+): GLvoidArray"), "$1: ByteArray"),
            line.replace(Regex("(\\w+): GLvoidArray"), "$1: ShortArray"),
            line.replace(Regex("(\\w+): GLvoidArray"), "$1: IntArray"),
        )
    }
}

fun convertByLanguages() {
    val original = File(FUNCTION_ONLY_CONVERTED).readText(Charset.defaultCharset())
    File(JVM_CONVERTED).writeText(convertToJVM(original))
    File(JS_CONVERTED).writeText(convertToJS(original))
}

fun convertHeaderLine(original: String): String? {
    if (!original.startsWith("#define") && !original.startsWith("GL_APICALL")) return null
    val result = applyPatterns(original, cppPatterns + renamePatterns + functionRefactorPatterns)
    if (result == original) return null
    return result
}

fun convertToJVM(original: String): String {
    val result = applyPatternsByLine(original + "\n", commonPatterns + jvmPatterns)
    return JVM_HEADER + result
}

fun convertToJS(original: String): String {
    val result = applyPatternsByLine(original + "\n", commonPatterns + jsPatterns)
    return JS_HEADER + result
}

fun applyPatternsByLine(text: String, map: Map<Regex, PatternReplacer>): String {
    val originalLines = text.split("\n")
    val convertedLines = originalLines.map {
        if (it.startsWith("//")) it
        else applyPatterns(it, map)
    }
    return convertedLines.joinToString("\n")
}

fun applyPatterns(original: String, patterns: Map<Regex, PatternReplacer>): String {
    var result = original
    patterns.forEach { regex, res ->
        res.fold({ str ->
            result = result.replace(regex, str)
        }, { func ->
            result = result.replace(regex, func)
        })
    }
    return result
}
