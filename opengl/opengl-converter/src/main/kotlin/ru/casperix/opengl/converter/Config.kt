package ru.casperix.opengl.converter

object Config {
    const val CPP_ORIGINAL = "src/main/resources/gl3.h"

    const val OUTPUT_DIRECTORY = "generated/gl30/"

    const val API_CONVERTED = OUTPUT_DIRECTORY + "API.kt"
    const val CONST_ONLY_CONVERTED = OUTPUT_DIRECTORY + "ConstOnly.kt"
    const val FUNCTION_ONLY_CONVERTED = OUTPUT_DIRECTORY + "FunctionOnly.kt"
    const val JVM_CONVERTED = OUTPUT_DIRECTORY + "JVM.kt"
    const val JS_CONVERTED = OUTPUT_DIRECTORY + "JS.kt"
}