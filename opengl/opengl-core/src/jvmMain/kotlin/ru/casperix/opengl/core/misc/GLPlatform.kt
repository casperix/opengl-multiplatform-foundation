package ru.casperix.opengl.core.misc

import ru.casperix.opengl.core.misc.GLSLVersion

actual object GLPlatform {
    actual val version = GLSLVersion.GL_3_30
}