package ru.casperix.opengl.core.app

import ru.casperix.opengl.core.app.glfw.GLFWLauncher
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener

fun jvmSurfaceLauncher(yDown:Boolean = false, creator: (Surface) -> SurfaceListener) {
    GLFWLauncher(yDown, creator)
}