package ru.casperix.opengl.core.app.glfw

import org.lwjgl.glfw.GLFW

object MonitorLoader {
    private val cache = loadAll()

    fun getAll(): List<GLFWMonitor> {
        return cache
    }

    private fun loadAll(): List<GLFWMonitor> {
        val monitors = GLFW.glfwGetMonitors() ?: return emptyList()
        val pointers = (0 until monitors.capacity()).map {
            monitors.get(it)
        }

        return pointers.map {
            GLFWMonitor(it)
        }
    }

}