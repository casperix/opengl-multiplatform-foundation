package ru.casperix.opengl.core.app.glfw

import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWErrorCallback
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.app.surface.SurfaceProvider
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.opengl.core.loader.FrameTimeCalculator

internal class GLFWLauncher(yDown: Boolean, creator: (Surface) -> SurfaceListener) {
    private val errorCallback = GLFWErrorCallback.createPrint(System.err)
    private var vSync = false
    private var vSyncStep = 0

    init {
        glfwSetErrorCallback(errorCallback)
        if (!glfwInit()) {
            throw Error("WGL initialize error")
        }
        glfwDefaultWindowHints()
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE)
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3)
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3)
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE)
        glfwWindowHint(GLFW_SAMPLES, 4)

        try {
            val window = GLFWWindow()

            SurfaceProvider.surface = window
            val listener = creator(window)
            GLWindowInput(window, yDown, listener::input)

            try {
                val timer = FrameTimeCalculator()

                while (!glfwWindowShouldClose(window.handle)) {
                    if (PlatformMisc.continuousRendering || window.hasRequestFrame) {
                        window.hasRequestFrame = false
                        listener.nextFrame(timer.tick())
                        glfwSwapBuffers(window.handle)
                    } else {
                        Thread.sleep(1L)
                    }

                    glfwPollEvents()
                    glfwSwapInterval(if (window.vSync) 1 else 0)

                    if (PlatformMisc.debugOneFPSMode) {
                        Thread.sleep(1000L)
                    }
                }
            } finally {
                listener.dispose()
                window.dispose()
            }

        } finally {
            dispose()
        }
    }

    private fun dispose() {
        glfwTerminate()
        errorCallback.free()
    }
}


