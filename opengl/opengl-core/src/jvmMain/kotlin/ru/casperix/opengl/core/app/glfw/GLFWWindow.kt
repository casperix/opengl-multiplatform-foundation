package ru.casperix.opengl.core.app.glfw

import org.lwjgl.glfw.Callbacks
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GLCapabilities
import org.lwjgl.system.MemoryUtil.NULL
import ru.casperix.app.surface.DesktopWindow
import ru.casperix.app.surface.component.Display
import ru.casperix.app.window.Cursor
import ru.casperix.app.window.SystemCursor
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.intersection.int32.Intersection2Int
import ru.casperix.math.vector.int32.Vector2i

internal class GLFWWindow() : DesktopWindow {
    private var title = "App"
    private var position = Vector2i(100, 100)
    private var size = Dimension2i(600, 400)
    var vSync = false
    private var lastArea = Box2i.byDimension(position, size.toVector2i())
    private var cursor: Cursor = SystemCursor.DEFAULT

    val handle:Long = run {
        if (!glfwInit()) {
            throw Error("WGL initialize error")
        }

        glfwCreateWindow(size.width, size.height, title, NULL, NULL).apply {
            if (this == NULL) {
                throw Throwable("Can't create GLFWWindow")
            }
        }
    }

    var hasRequestFrame = true
    val capabilities: GLCapabilities

    init {
        glfwMakeContextCurrent(handle)
        capabilities = GL.createCapabilities()

        glfwSetWindowSizeCallback(handle) { _, x, y ->
            size = Dimension2i(x, y)
        }

        position = getWindowPos()

        glfwSetWindowPosCallback(handle) { _, x, y ->
            position = Vector2i(x, y)
        }

        setVerticalSync(true)
    }

    fun dispose() {
        glfwDestroyWindow(handle)
        Callbacks.glfwFreeCallbacks(handle)
    }

    private fun getWindowPos():Vector2i {
        val xBuffer = IntArray(1)
        val yBuffer = IntArray(1)
        glfwGetWindowPos(handle, xBuffer, yBuffer)
        return Vector2i(xBuffer[0], yBuffer[0])
    }

    override fun setTitle(value: String) {
        title = value
        glfwSetWindowTitle(handle, title)
    }

    override fun getTitle(): String {
        return title
    }


    override fun maximize() {
        glfwMaximizeWindow(handle)
    }

    override fun restore() {
        glfwRestoreWindow(handle)
    }

    override fun setCursor(value: Cursor) {
        if (value is SystemCursor) {
            val type = when (value) {
                SystemCursor.DEFAULT -> GLFW_ARROW_CURSOR
                SystemCursor.HAND -> GLFW_POINTING_HAND_CURSOR
                SystemCursor.TEXT -> GLFW_IBEAM_CURSOR
                else -> GLFW_ARROW_CURSOR
            }
            glfwSetCursor(handle, type.toLong())
        }

        cursor = value
    }


    override fun minimize() {
        glfwHideWindow(handle)
    }

    override fun requestFrame() {
        hasRequestFrame = true
    }

    override fun getVerticalSync(): Boolean {
        return vSync
    }

    override fun setVerticalSync(value: Boolean) {
        vSync = value
    }

    override fun setSize(value: Dimension2i) {
        size = value
        glfwSetWindowSize(handle, value.width, value.height)
    }

    override fun getSize(): Dimension2i {
        return size
    }

    override fun setPosition(value: Vector2i) {
        position = value
        glfwSetWindowPos(handle, value.x, value.y)
    }

    override fun getPosition(): Vector2i {
        return position
    }

    override fun getCursor(): Cursor {
        return cursor
    }

    override fun getDisplay(): Display {
        return getCurrentMonitor().display
    }

    private fun getCurrentMonitor(): GLFWMonitor {
        val monitors = MonitorLoader.getAll()

        if (monitors.isEmpty())
            throw Exception("Invalid display info")

        if (monitors.size == 1) {
            return monitors.first()
        }
        val windowArea = getWindowArea()

        monitors.forEach { monitor ->
            if (Intersection2Int.hasBoxWithBox(windowArea, monitor.display.displayArea)) {
                return monitor
            }
        }
        return monitors.first()
    }

    private fun setMonitorArea(area: Box2i) {
        glfwSetWindowMonitor(
            handle,
            0,
            area.min.x,
            area.min.y,
            area.dimension.x,
            area.dimension.y,
            0
        )
    }

    private fun getWindowArea(): Box2i = Box2i.byDimension(position, size.toVector2i())

    override fun setFullScreen(value: Boolean) {
        if (value) {
            val monitor = getCurrentMonitor()
            lastArea = getWindowArea()
            val display = monitor.display
            glfwSetWindowMonitor(handle, monitor.handle, 0, 0, display.resolution.width, display.resolution.height, GLFW_DONT_CARE)
        } else {
            setMonitorArea(lastArea)
        }

    }
}

