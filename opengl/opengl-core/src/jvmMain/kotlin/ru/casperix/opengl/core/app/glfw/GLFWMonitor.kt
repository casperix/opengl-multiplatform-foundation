package ru.casperix.opengl.core.app.glfw

import ru.casperix.app.surface.component.Display
import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.DirectIntBuffer
import ru.casperix.multiplatform.toDimension
import org.lwjgl.glfw.GLFW

class GLFWMonitor(val handle: Long) {

    val display = buildDisplay()
    

    private fun buildDisplay(): Display {

        val position = getMonitorPosition()
        val inchSize = getMonitorSizeInInch()

        val mode = getVideMode()
            ?: throw Exception("Video-mode info unavailable")

        return Display(
            position,
            Dimension2i(mode.width, mode.height),
            inchSize,
            mode.bitsPerPixel,
            mode.frequency,
        )
    }

    /**
     * Size in inch
     */
    fun getMonitorSizeInInch(): Dimension2f {
        val mmToInchDivider = 25.4

        val xBuffer = DirectIntBuffer(1)
        val yBuffer = DirectIntBuffer(1)
        GLFW.glfwGetMonitorPhysicalSize(handle, xBuffer, yBuffer)

        val inchSizeX = xBuffer[0] / mmToInchDivider
        val inchSizeY = yBuffer[0] / mmToInchDivider

        return Vector2d(inchSizeX, inchSizeY).toVector2f().toDimension()
    }

    fun getMonitorPosition(): Vector2i {

        val xBuffer = DirectIntBuffer(1)
        val yBuffer = DirectIntBuffer(1)
        GLFW.glfwGetMonitorPos(handle, xBuffer, yBuffer)

        return Vector2i(xBuffer[0], yBuffer[0])
    }

    class VideoMode(
        val frequency: Int,
        val bitsPerPixel: Int,
        val width: Int,
        val height: Int,
    )

    fun getVideMode(): VideoMode? {
        GLFW.glfwGetVideoMode(handle)?.let {
            return VideoMode(
                it.refreshRate(),
                it.redBits() + it.greenBits() + it.blueBits(),
                it.width(),
                it.height(),
            )
        }
        return null
    }

}