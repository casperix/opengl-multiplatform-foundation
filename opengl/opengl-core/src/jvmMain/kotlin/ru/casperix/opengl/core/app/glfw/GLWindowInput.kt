package ru.casperix.opengl.core.app.glfw

import io.github.oshai.kotlinlogging.KotlinLogging
import ru.casperix.input.*
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.PlatformMisc
import org.lwjgl.glfw.GLFW

internal class GLWindowInput(window: GLFWWindow, val yDown: Boolean, val inputCatcher: (InputEvent) -> Unit) {
    private val logger = KotlinLogging.logger {  }
    private var lastMousePosition = Vector2f.ZERO

    init {
        GLFW.glfwSetCharCallback(window.handle) { _, key ->
            logger.trace { "SetCharCallback (code: $key)" }

            inputCatcher(KeyTyped(Char(key), getKeyButton(key)))
        }
        GLFW.glfwSetKeyCallback(window.handle) { _, key, scancode, action, mods ->
            logger.trace { "SetKeyCallback (code: $key; scan: $scancode; mods: $mods)" }

            val button =getKeyButton(key)
            when (action) {
                GLFW.GLFW_PRESS -> inputCatcher(KeyDown(button))
                GLFW.GLFW_RELEASE -> inputCatcher(KeyUp(button))
                GLFW.GLFW_REPEAT -> {
                    inputCatcher(KeyDown(button))
                    inputCatcher(KeyUp(button))
                }
            }
        }
        GLFW.glfwSetCursorPosCallback(window.handle) { _, x, y ->
            logger.trace { "SetCursorPosCallback: $x; $y" }

            val nextMousePosition = getMousePosition(window, x, y)
            val delta = nextMousePosition - lastMousePosition
            lastMousePosition = nextMousePosition
            inputCatcher(MouseMove(nextMousePosition))
        }

        GLFW.glfwSetScrollCallback(window.handle) { _, scrollX, scrollY ->
            logger.trace { "SetScrollCallback: (pos: $scrollX, $scrollY)" }

            inputCatcher(MouseWheel(lastMousePosition, Vector2f(scrollX.toFloat(), scrollY.toFloat())))
        }

        GLFW.glfwSetMouseButtonCallback(window.handle) { _, buttonCode, action, mods ->
            logger.trace { "SetMouseButtonCallback: (code: $buttonCode; action: $action; mods: $mods)" }

            when (action) {
                GLFW.GLFW_PRESS -> inputCatcher(
                    MouseDown(
                        lastMousePosition,
                        PointerButton(buttonCode),
                    )
                )

                GLFW.GLFW_RELEASE -> inputCatcher(
                    MouseUp(
                        lastMousePosition,
                        PointerButton(buttonCode),
                    )
                )

                else -> {
                    throw Error("Invalid mouse action: $action")
                }
            }
        }
    }

    private fun getKeyButton(code: Int): KeyButton {
        return KeyMap.glfwToApi[code] ?: KeyButton.UNKNOWN_KEY
    }

    private fun getMousePosition(window: GLFWWindow, x: Double, y: Double): Vector2f {
        val nextY = if (yDown) {
            y
        } else {
            window.getSize().height - y
        }

        return Vector2f(x.toFloat(), nextY.toFloat())
    }
}