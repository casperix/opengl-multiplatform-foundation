package ru.casperix.opengl.core.app

import android.content.Context
import android.opengl.GLSurfaceView
import android.view.inputmethod.InputMethodManager
import ru.casperix.app.surface.AndroidSurface
import ru.casperix.app.surface.component.Display
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.multiplatform.toDimension

class AndroidSurfaceImpl(val view: GLSurfaceView) : AndroidSurface {
    override fun showVirtualKeyboard() {
        if (!view.requestFocus()) return

        val inputMethodManager = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    override fun hideVirtualKeyboard() {
        val inputMethodManager = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun requestFrame() {
        view.requestRender()
    }

    override fun getDisplay(): Display = view.resources.displayMetrics.run {
        Display(
            Vector2i.ZERO,
            Dimension2i(widthPixels, heightPixels),
            (Dimension2i(widthPixels, heightPixels).toVector2f() / Vector2f(xdpi, ydpi)).toDimension(),
            0,
            0

        )
    }

    override fun setTitle(value: String) {

    }

    override fun getTitle(): String {
        return view.context.packageName
    }

    override fun getVerticalSync(): Boolean {
        return false
    }

    override fun setVerticalSync(value: Boolean) {

    }

    override fun getSize(): Dimension2i {
        return Dimension2i(view.width, view.height)

    }

    override fun getPosition(): Vector2i {
        return Vector2f(view.x, view.y).roundToVector2i()
    }

}