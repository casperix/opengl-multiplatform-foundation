package ru.casperix.opengl.core.app

import android.opengl.GLSurfaceView
import ru.casperix.app.surface.AndroidSurface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.app.surface.SurfaceProvider
import ru.casperix.input.InputEvent
import ru.casperix.opengl.core.loader.FrameTimeCalculator
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class NativeSurfaceRenderer(val surface:AndroidSurface, val creator: (AndroidSurface)-> SurfaceListener) : GLSurfaceView.Renderer {
    private var listener: SurfaceListener? = null
    private val calculator = FrameTimeCalculator()

    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        SurfaceProvider.surface = surface
        listener = creator(surface)
    }

    override fun onDrawFrame(unused: GL10) {
        listener?.nextFrame(calculator.tick())
    }

    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
//        (surface as AndroidSurfaceImpl).size = Dimension2i(width, height)
    }

    fun input(event: InputEvent) {
        listener?.input(event)
    }

}