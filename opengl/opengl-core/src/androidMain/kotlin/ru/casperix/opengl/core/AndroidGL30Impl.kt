package ru.casperix.opengl.core

import ru.casperix.misc.*
import android.opengl.GLES30 as GL

private val BUFFER_NAME_LENGTH = 1024

actual fun glActiveTexture(texture: GLenum) {
	GL.glActiveTexture(texture)
}

actual fun glAttachShader(program: GLuint, shader: GLuint) {
	GL.glAttachShader(program, shader)
}

actual fun glBeginQuery(target: GLenum, query: GLuint) {
	GL.glBeginQuery(target, query)
}

actual fun glBeginTransformFeedback(primitiveMode: GLenum) {
	GL.glBeginTransformFeedback(primitiveMode)
}

actual fun glBindAttribLocation(program: GLuint, index: GLuint, name: GLcharPointer) {
	GL.glBindAttribLocation(program, index, name)
}

actual fun glBindBuffer(target: GLenum, buffer: GLuint) {
	GL.glBindBuffer(target, buffer)
}

actual fun glBindBufferBase(target: GLenum, index: GLuint, buffer: GLuint) {
	GL.glBindBufferBase(target, index, buffer)
}

actual fun glBindBufferRange(target: GLenum, index: GLuint, buffer: GLuint, offset: GLintptr, size: GLsizeiptr) {
	GL.glBindBufferRange(target, index, buffer, offset.toInt(), size.toInt())
}

actual fun glBindFramebuffer(target: GLenum, frameBuffer: GLuint) {
	GL.glBindFramebuffer(target, frameBuffer)
}

actual fun glBindRenderbuffer(target: GLenum, renderBuffer: GLuint) {
	GL.glBindRenderbuffer(target, renderBuffer)
}

actual fun glBindSampler(unit: GLuint, sampler: GLuint) {
	GL.glBindSampler(unit, sampler)
}

actual fun glBindTexture(target: GLenum, texture: GLuint) {
	GL.glBindTexture(target, texture)
}

//Not supported in JVM:
//expect fun glBindTransformFeedback(target: GLenum, id: GLuint)
actual fun glBindVertexArray(array: GLuint) {
	GL.glBindVertexArray(array)
}

actual fun glBlendColor(red: GLfloat, green: GLfloat, blue: GLfloat, alpha: GLfloat) {
	GL.glBlendColor(red, green, blue, alpha)
}

actual fun glBlendEquation(mode: GLenum) {
	GL.glBlendEquation(mode)
}

actual fun glBlendEquationSeparate(modeRGB: GLenum, modeAlpha: GLenum) {
	GL.glBlendEquationSeparate(modeRGB, modeAlpha)
}

actual fun glBlendFunc(sourceFactor: GLenum, destFactor: GLenum) {
	GL.glBlendFunc(sourceFactor, destFactor)
}

actual fun glBlendFuncSeparate(sourceRGB: GLenum, destRGB: GLenum, sourceAlpha: GLenum, destAlpha: GLenum) {
	GL.glBlendFuncSeparate(sourceRGB, destRGB, sourceAlpha, destAlpha)
}

actual fun glBlitFramebuffer(srcX0: GLint, srcY0: GLint, srcX1: GLint, srcY1: GLint, dstX0: GLint, dstY0: GLint, dstX1: GLint, dstY1: GLint, mask: GLbitfield, filter: GLenum) {
	GL.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter)
}

actual fun glBufferData(target: GLenum, clientData: ByteArray, usage: GLenum) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferData(target, buffer.capacity(), buffer, usage)
}

actual fun glBufferData(target: GLenum, clientData: ShortArray, usage: GLenum) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferData(target, buffer.capacity(), buffer, usage)
}

actual fun glBufferData(target: GLenum, clientData: IntArray, usage: GLenum) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferData(target, buffer.capacity(), buffer, usage)
}

actual fun glBufferData(target: GLenum, clientData: LongArray, usage: GLenum) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferData(target, buffer.capacity(), buffer, usage)
}

actual fun glBufferData(target: GLenum, clientData: FloatArray, usage: GLenum) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferData(target, buffer.capacity(), buffer, usage)
}

actual fun glBufferData(target: GLenum, clientData: DoubleArray, usage: GLenum) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferData(target, buffer.capacity(), buffer, usage)
}

actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: ByteArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferSubData(target, offset.toInt(), buffer.capacity(), buffer)
}

actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: ShortArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferSubData(target, offset.toInt(), buffer.capacity(), buffer)
}

actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: IntArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferSubData(target, offset.toInt(), buffer.capacity(), buffer)
}

actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: LongArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferSubData(target, offset.toInt(), buffer.capacity(), buffer)
}

actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: FloatArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferSubData(target, offset.toInt(), buffer.capacity(), buffer)
}

actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: DoubleArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glBufferSubData(target, offset.toInt(), buffer.capacity(), buffer)
}

actual fun glCheckFramebufferStatus(target: GLenum): GLenum {
	return GL.glCheckFramebufferStatus(target)
}

actual fun glClear(mask: GLbitfield) {
	GL.glClear(mask)
}

actual fun glClearBufferfi(buffer: GLenum, drawBuffer: GLint, depth: GLfloat, stencil: GLint) {
	GL.glClearBufferfi(buffer, drawBuffer, depth, stencil)
}

actual fun glClearBufferfv(buffer: GLenum, drawBuffer: GLint, value: GLfloatPointer) {
	GL.glClearBufferfv(buffer, drawBuffer, value, 0)
}

actual fun glClearBufferiv(buffer: GLenum, drawBuffer: GLint, value: GLintPointer) {
	GL.glClearBufferiv(buffer, drawBuffer, value, 0)
}

actual fun glClearBufferuiv(buffer: GLenum, drawBuffer: GLint, value: GLuintPointer) {
	GL.glClearBufferuiv(buffer, drawBuffer, value, 0)
}

actual fun glClearColor(red: GLfloat, green: GLfloat, blue: GLfloat, alpha: GLfloat) {
	GL.glClearColor(red, green, blue, alpha)
}

actual fun glClearDepth(depth: GLclampd) {
	GL.glClearDepthf(depth.toFloat())
}

actual fun glClearStencil(s: GLint) {
	GL.glClearStencil(s)
}

actual fun glClientWaitSync(sync: GLsync, flags: GLbitfield, timeout: GLuint64): GLenum {
	return GL.glClientWaitSync(sync as Long, flags, timeout)
}

actual fun glColorMask(red: GLboolean, green: GLboolean, blue: GLboolean, alpha: GLboolean) {
	GL.glColorMask(red, green, blue, alpha)
}

actual fun glCompileShader(shader: GLuint) {
	GL.glCompileShader(shader)
}

actual fun glCompressedTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, border: GLint, clientData: ByteArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage2D(target, level, internalFormat, width, height, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, border: GLint, clientData: ShortArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage2D(target, level, internalFormat, width, height, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, border: GLint, clientData: IntArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage2D(target, level, internalFormat, width, height, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, border: GLint, clientData: LongArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage2D(target, level, internalFormat, width, height, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, border: GLint, clientData: FloatArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage2D(target, level, internalFormat, width, height, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, border: GLint, clientData: DoubleArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage2D(target, level, internalFormat, width, height, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage3D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, clientData: ByteArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage3D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, clientData: ShortArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage3D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, clientData: IntArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage3D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, clientData: LongArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage3D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, clientData: FloatArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexImage3D(target: GLenum, level: GLint, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, clientData: DoubleArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexImage3D(target, level, internalFormat, width, height, depth, border, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, clientData: ByteArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, clientData: ShortArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, clientData: IntArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, clientData: LongArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, clientData: FloatArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, clientData: DoubleArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, clientData: ByteArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, clientData: ShortArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, clientData: IntArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, clientData: LongArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, clientData: FloatArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, buffer.capacity(), buffer)
}

actual fun glCompressedTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, clientData: DoubleArray) {
	val buffer = clientData.toDirectByteBuffer()
	GL.glCompressedTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, buffer.capacity(), buffer)
}

actual fun glCopyBufferSubData(readTarget: GLenum, writeTarget: GLenum, readOffset: GLintptr, writeOffset: GLintptr, size: GLsizeiptr) {
	GL.glCopyBufferSubData(readTarget, writeTarget, readOffset.toInt(), writeOffset.toInt(), size.toInt())
}

actual fun glCopyTexImage2D(target: GLenum, level: GLint, internalFormat: GLenum, x: GLint, y: GLint, width: GLsizei, height: GLsizei, border: GLint) {
	GL.glCopyTexImage2D(target, level, internalFormat, x, y, width, height, border)
}

actual fun glCopyTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, x: GLint, y: GLint, width: GLsizei, height: GLsizei) {
	GL.glCopyTexSubImage2D(target, level, xOffset, yOffset, x, y, width, height)
}

actual fun glCopyTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, x: GLint, y: GLint, width: GLsizei, height: GLsizei) {
	GL.glCopyTexSubImage3D(target, level, xOffset, yOffset, zOffset, x, y, width, height)
}

actual fun glCreateProgram(): GLuint {
	return GL.glCreateProgram()
}

actual fun glCreateShader(type: GLenum): GLuint {
	return GL.glCreateShader(type)
}

actual fun glCullFace(mode: GLenum) {
	GL.glCullFace(mode)
}

actual fun glDeleteBuffers(buffers: GLuintArray) {
	GL.glDeleteBuffers(buffers.size, buffers, 0)
}

actual fun glDeleteFramebuffers(frameBuffers: GLuintArray) {
	GL.glDeleteFramebuffers(frameBuffers.size, frameBuffers, 0)
}

actual fun glDeleteProgram(program: GLuint) {
	GL.glDeleteProgram(program)
}

actual fun glDeleteQueries(ids: GLuintArray) {
	GL.glDeleteQueries(ids.size, ids, 0)
}

actual fun glDeleteRenderbuffers(renderBuffers: GLuintArray) {
	GL.glDeleteRenderbuffers(renderBuffers.size, renderBuffers, 0)
}

actual fun glDeleteSamplers(samplers: GLuintArray) {
	GL.glDeleteSamplers(samplers.size, samplers, 0)
}

actual fun glDeleteShader(shader: GLuint) {
	GL.glDeleteShader(shader)
}

actual fun glDeleteSync(sync: GLsync) {
	GL.glDeleteSync(sync as Long)
}

actual fun glDeleteTextures(textures: GLuintArray) {
	GL.glDeleteTextures(textures.size, textures , 0)
}

//Not supported in JVM:
//expect fun glDeleteTransformFeedbacks(ids: GLuintArray)
actual fun glDeleteVertexArrays(arrays: GLuintArray) {
	GL.glDeleteVertexArrays(arrays.size, arrays, 0)
}

actual fun glDepthFunc(func: GLenum) {
	GL.glDepthFunc(func)
}

actual fun glDepthMask(flag: GLboolean) {
	GL.glDepthMask(flag)
}

actual fun glDepthRange(near: GLdouble, far: GLdouble) {
	GL.glDepthRangef(near.toFloat(), far.toFloat())
}

actual fun glDepthRangef(near: GLfloat, far: GLfloat) {
	GL.glDepthRangef(near, far)
}

actual fun glDetachShader(program: GLuint, shader: GLuint) {
	GL.glDetachShader(program, shader)
}

actual fun glDisable(capability: GLenum) {
	GL.glDisable(capability)
}

actual fun glDisableVertexAttribArray(index: GLuint) {
	GL.glDisableVertexAttribArray(index)
}

actual fun glDrawArrays(mode: GLenum, vertexOffset: GLint, vertexCount: GLsizei) {
	GL.glDrawArrays(mode, vertexOffset, vertexCount)
}

actual fun glDrawArraysInstanced(mode: GLenum, vertexOffset: GLint, vertexCount: GLsizei, instanceCount: GLsizei) {
	GL.glDrawArraysInstanced(mode, vertexOffset, vertexCount, instanceCount)
}

actual fun glDrawBuffers(bufs: GLenumArray) {
	GL.glDrawBuffers(bufs.size, bufs, 0)
}

actual fun glDrawElements(mode: GLenum, count: GLsizei, type: GLenum, indicesOffset: GLintptr) {
	GL.glDrawElements(mode, count, type, indicesOffset.toInt())
}

actual fun glDrawElementsInstanced(mode: GLenum, count: GLsizei, type: GLenum, offset: GLintptr, instanceCount: GLsizei) {
	GL.glDrawElementsInstanced(mode, count, type, offset.toInt(), instanceCount)
}

actual fun glDrawRangeElements(mode: GLenum, start: GLuint, end: GLuint, count: GLsizei, type: GLenum, offset: GLintptr) {
	GL.glDrawRangeElements(mode, start, end, count, type, offset.toInt())
}

actual fun glEnable(capability: GLenum) {
	GL.glEnable(capability)
}

actual fun glEnableVertexAttribArray(index: GLuint) {
	GL.glEnableVertexAttribArray(index)
}

actual fun glEndQuery(target: GLenum) {
	GL.glEndQuery(target)
}

actual fun glEndTransformFeedback() {
	GL.glEndTransformFeedback()
}

actual fun glFenceSync(condition: GLenum, flags: GLbitfield): GLsync {
	return GL.glFenceSync(condition, flags)
}

actual fun glFinish() {
	GL.glFinish()
}

actual fun glFlush() {
	GL.glFlush()
}

//Not supported in JS:
//actual fun glFlushMappedBufferRange(target: GLenum, offset: GLintptr, length: GLsizeiptr) {
//	GL.glFlushMappedBufferRange(target, offset, length)
//}

actual fun glFramebufferRenderbuffer(target: GLenum, attachment: GLenum, renderBuffertarget: GLenum, renderBuffer: GLuint) {
	GL.glFramebufferRenderbuffer(target, attachment, renderBuffertarget, renderBuffer)
}

actual fun glFramebufferTexture2D(target: GLenum, attachment: GLenum, textureTarget: GLenum, texture: GLuint, level: GLint) {
	GL.glFramebufferTexture2D(target, attachment, textureTarget, texture, level)
}

actual fun glFramebufferTextureLayer(target: GLenum, attachment: GLenum, texture: GLuint, level: GLint, layer: GLint) {
	GL.glFramebufferTextureLayer(target, attachment, texture, level, layer)
}

actual fun glFrontFace(mode: GLenum) {
	GL.glFrontFace(mode)
}

actual fun glGenBuffers(buffers: GLuintArray) {
	GL.glGenBuffers(buffers.size, buffers, 0)
}

actual fun glGenFramebuffers(frameBuffers: GLuintArray) {
	GL.glGenFramebuffers(frameBuffers.size, frameBuffers, 0)
}

actual fun glGenQueries(ids: GLuintArray) {
	GL.glGenQueries(ids.size, ids, 0)
}

actual fun glGenRenderbuffers(renderBuffers: GLuintArray) {
	GL.glGenRenderbuffers(renderBuffers.size, renderBuffers, 0)
}

actual fun glGenSamplers(samplers: GLuintArray) {
	GL.glGenSamplers(samplers.size, samplers, 0)
}

actual fun glGenTextures(textures: GLuintArray) {
	GL.glGenTextures(textures.size, textures, 0)
}

//Not supported in JVM:
//expect fun glGenTransformFeedbacks(ids: GLuintArray)
actual fun glGenVertexArrays(vertexArrays: GLuintArray) {
	GL.glGenVertexArrays(vertexArrays.size, vertexArrays, 0)
}

actual fun glGenerateMipmap(target: GLenum) {
	GL.glGenerateMipmap(target)
}

actual fun glGetActiveAttrib(program: GLuint, index: GLuint): GLActiveInfo? {
	val lengthBuffer = intArrayOf(1)
	val sizeBuffer = IntArray(1)
	val typeBuffer = IntArray(1)
	val nameBuffer = ByteArray(BUFFER_NAME_LENGTH)
	GL.glGetActiveAttrib(program, index, 1, lengthBuffer, 0, sizeBuffer, 0, typeBuffer, 0, nameBuffer, 0)

	val name = String(nameBuffer, 0, lengthBuffer[0])
	return GLActiveInfo(sizeBuffer[0], typeBuffer[0], name)
}

actual fun glGetActiveUniform(program: GLuint, index: GLuint): GLActiveInfo? {
	val lengthBuffer = intArrayOf(1)
	val sizeBuffer = IntArray(1)
	val typeBuffer = IntArray(1)
	val nameBuffer = ByteArray(BUFFER_NAME_LENGTH)
	GL.glGetActiveUniform(program, index, 1, lengthBuffer, 0, sizeBuffer, 0, typeBuffer, 0, nameBuffer, 0)

	val name = String(nameBuffer, 0, lengthBuffer[0])
	return GLActiveInfo(sizeBuffer[0], typeBuffer[0], name)
}

actual fun glGetActiveUniformBlockName(program: GLuint, uniformBlockIndex: GLuint):String? {
	val lengthBuffer = intArrayOf(1)
	val nameBuffer = ByteArray(BUFFER_NAME_LENGTH)
	GL.glGetActiveUniformBlockName(program, uniformBlockIndex, 1, lengthBuffer, 0, nameBuffer, 0)
	val name = String(nameBuffer, 0, lengthBuffer[0])
	return name
}

actual fun glGetActiveUniformBlockiv(program: GLuint, uniformBlockIndex: GLuint, parameterName: GLenum, outputParams: GLintPointer) {
	GL.glGetActiveUniformBlockiv(program, uniformBlockIndex, parameterName, outputParams, 0)
}

actual fun glGetActiveUniformsiv(program: GLuint, uniformIndices: GLuintArray, parameterName: GLenum, outputParams: GLintPointer) {
	GL.glGetActiveUniformsiv(program, uniformIndices.size, uniformIndices, 0, parameterName, outputParams, 0)
}

actual fun glGetAttachedShaders(program: GLuint, count: GLsizeiArray, shaders: GLuintPointer) {
	GL.glGetAttachedShaders(program, count.size, count, 0, shaders, 0)
}

actual fun glGetAttribLocation(program: GLuint, name: GLcharPointer): GLint {
	return GL.glGetAttribLocation(program, name)
}

//Not implemented yet...:
//expect fun glGetBooleanv(parameterName: GLenum, data: GLbooleanPointer)

actual fun glGetBufferParameteri64v(target: GLenum, parameterName: GLenum, params: GLint64Pointer) {
	GL.glGetBufferParameteri64v(target, parameterName, params, 0)
}

actual fun glGetBufferParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
	GL.glGetBufferParameteriv(target, parameterName, params, 0)
}

//Not implemented yet...:
//expect fun glGetBufferPointerv(target: GLenum, parameterName: GLenum, void **params)
actual fun glGetError(): GLenum {
	return GL.glGetError()
}

actual fun glGetFloatv(parameterName: GLenum, data: GLfloatPointer) {
	GL.glGetFloatv(parameterName, data, 0)
}

actual fun glGetFragDataLocation(program: GLuint, name: GLcharPointer): GLint {
	return GL.glGetFragDataLocation(program, name)
}

actual fun glGetFramebufferAttachmentParameteriv(target: GLenum, attachment: GLenum, parameterName: GLenum, params: GLintPointer) {
	GL.glGetFramebufferAttachmentParameteriv(target, attachment, parameterName, params, 0)
}

actual fun glGetInteger64i_v(target: GLenum, index: GLuint, data: GLint64Pointer) {
	GL.glGetInteger64i_v(target, index, data, 0)
}

actual fun glGetInteger64v(parameterName: GLenum, data: GLint64Pointer) {
	GL.glGetInteger64v(parameterName, data, 0)
}

actual fun glGetIntegeri_v(target: GLenum, index: GLuint, data: GLintPointer) {
	GL.glGetIntegeri_v(target, index, data, 0)
}

actual fun glGetIntegerv(parameterName: GLenum, data: GLintPointer) {
	GL.glGetIntegerv(parameterName, data, 0)
}

//Not supported in JVM:
//expect fun glGetInternalformativ(target: GLenum, internalFormat: GLenum, parameterName: GLenum, params: GLintArray)
//Not supported in JVM:
//expect fun glGetProgramBinary(program: GLuint, length: GLsizeiArray, binaryFormat: GLenumPointer, binary: voidPointer)
actual fun glGetProgramInfoLog(program: GLuint):String? {
	return GL.glGetProgramInfoLog(program)
}

actual fun glGetProgramiv(program: GLuint, parameterName: GLenum, params: GLintPointer) {
	GL.glGetProgramiv(program, parameterName, params, 0)
}

actual fun glGetQueryObjectuiv(query: GLuint, parameterName: GLenum, params: GLuintPointer) {
	GL.glGetQueryObjectuiv(query, parameterName, params, 0)
}

actual fun glGetQueryiv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
	GL.glGetQueryiv(target, parameterName, params, 0)
}

actual fun glGetRenderbufferParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
	GL.glGetRenderbufferParameteriv(target, parameterName, params, 0)
}

actual fun glGetSamplerParameterfv(sampler: GLuint, parameterName: GLenum, params: GLfloatPointer) {
	GL.glGetSamplerParameterfv(sampler, parameterName, params, 0)
}

actual fun glGetSamplerParameteriv(sampler: GLuint, parameterName: GLenum, params: GLintPointer) {
	GL.glGetSamplerParameteriv(sampler, parameterName, params, 0)
}

actual fun glGetShaderInfoLog(shader: GLuint):String? {
	return GL.glGetShaderInfoLog(shader)
}

//Not supported in JVM:
//expect fun glGetShaderPrecisionFormat(shadertype: GLenum, precisiontype: GLenum, range: GLintPointer, precision: GLintPointer)
actual fun glGetShaderSource(shader: GLuint):String? {
	return GL.glGetShaderSource(shader)
}

actual fun glGetShaderiv(shader: GLuint, parameterName: GLenum, params: GLintPointer) {
	GL.glGetShaderiv(shader, parameterName, params, 0)
}

actual fun glGetString(name: GLenum): String? {
	return GL.glGetString(name)
}

actual fun glGetStringi(name: GLenum, index: GLuint): String? {
	return GL.glGetStringi(name, index)
}

actual fun glGetSynci(sync: GLsync, parameterName: GLenum):Int? {
	val lengths = IntArray(1)
	val values = IntArray(1)
	GL.glGetSynciv(sync as Long, parameterName, 1, lengths, 0, values, 0)
	return values[0]
}

actual fun glGetTexParameterfv(target: GLenum, parameterName: GLenum, params: GLfloatPointer) {
	GL.glGetTexParameterfv(target, parameterName, params, 0)
}

actual fun glGetTexParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
	GL.glGetTexParameteriv(target, parameterName, params, 0)
}

actual fun glGetTransformFeedbackVarying(program: GLuint, index: GLuint): GLActiveInfo? {
	val lengthBuffer = intArrayOf(1)
	val sizeBuffer = IntArray(1)
	val typeBuffer = IntArray(1)
	val nameBuffer = ByteArray(BUFFER_NAME_LENGTH)
	GL.glGetTransformFeedbackVarying(program, index, 1, lengthBuffer, 0, sizeBuffer, 0, typeBuffer, 0, nameBuffer, 0)

	val name = String(nameBuffer, 0, lengthBuffer[0])
	return GLActiveInfo(sizeBuffer[0], typeBuffer[0], name)
}

actual fun glGetUniformBlockIndex(program: GLuint, uniformBlockName: GLcharPointer): GLuint {
	return GL.glGetUniformBlockIndex(program, uniformBlockName)
}

//Not implemented yet...:
//expect fun glGetUniformIndices(program: GLuint, uniformNames: GLcharArray, uniformIndices: GLuintPointer)

actual fun glGetUniformLocation(program: GLuint, name: GLcharPointer): GLLocation? {
	return GL.glGetUniformLocation(program, name)
}

actual fun glGetUniformfv(program: GLuint, location: GLLocation, params: GLfloatPointer) {
	GL.glGetUniformfv(program, location as GLuint, params, 0)
}

actual fun glGetUniformiv(program: GLuint, location: GLLocation, params: GLintPointer) {
	GL.glGetUniformiv(program, location as GLuint, params, 0)
}

actual fun glGetUniformuiv(program: GLuint, location: GLLocation, params: GLuintPointer) {
	GL.glGetUniformuiv(program, location as GLuint, params, 0)
}

actual fun glGetVertexAttribIiv(index: GLuint, parameterName: GLenum, params: GLintPointer) {
	GL.glGetVertexAttribIiv(index, parameterName, params, 0)
}

actual fun glGetVertexAttribIuiv(index: GLuint, parameterName: GLenum, params: GLuintPointer) {
	GL.glGetVertexAttribIuiv(index, parameterName, params, 0)
}

//Not implemented yet...:
//expect fun glGetVertexAttribPointerv(index: GLuint, parameterName: GLenum, void **pointer)
actual fun glGetVertexAttribfv(index: GLuint, parameterName: GLenum, params: GLfloatPointer) {
	GL.glGetVertexAttribfv(index, parameterName, params, 0)
}

actual fun glGetVertexAttribiv(index: GLuint, parameterName: GLenum, params: GLintPointer) {
	GL.glGetVertexAttribiv(index, parameterName, params, 0)
}

actual fun glHint(target: GLenum, mode: GLenum) {
	GL.glHint(target, mode)
}

//Not supported in JVM:
//expect fun glInvalidateFramebuffer(target: GLenum, attachments: GLenumArray)
//Not supported in JVM:
//expect fun glInvalidateSubFramebuffer(target: GLenum, attachments: GLenumArray, x: GLint, y: GLint, width: GLsizei, height: GLsizei)
actual fun glIsBuffer(buffer: GLuint): GLboolean {
	return GL.glIsBuffer(buffer)
}

actual fun glIsEnabled(capability: GLenum): GLboolean {
	return GL.glIsEnabled(capability)
}

actual fun glIsFramebuffer(frameBuffer: GLuint): GLboolean {
	return GL.glIsFramebuffer(frameBuffer)
}

actual fun glIsProgram(program: GLuint): GLboolean {
	return GL.glIsProgram(program)
}

actual fun glIsQuery(query: GLuint): GLboolean {
	return GL.glIsQuery(query)
}

actual fun glIsRenderbuffer(renderBuffer: GLuint): GLboolean {
	return GL.glIsRenderbuffer(renderBuffer)
}

actual fun glIsSampler(sampler: GLuint): GLboolean {
	return GL.glIsSampler(sampler)
}

actual fun glIsShader(shader: GLuint): GLboolean {
	return GL.glIsShader(shader)
}

actual fun glIsSync(sync: GLsync): GLboolean {
	return GL.glIsSync(sync as Long)
}

actual fun glIsTexture(texture: GLuint): GLboolean {
	return GL.glIsTexture(texture)
}

//Not supported in JVM:
//expect fun glIsTransformFeedback(id: GLuint):GLboolean
actual fun glIsVertexArray(array: GLuint): GLboolean {
	return GL.glIsVertexArray(array)
}

actual fun glLineWidth(width: GLfloat) {
	GL.glLineWidth(width)
}

actual fun glLinkProgram(program: GLuint) {
	GL.glLinkProgram(program)
}

//Not supported in JS:
//actual fun glMapBufferRange(target: GLenum, offset: GLintptr, length: GLsizeiptr, access: GLbitfield): voidPointer? {
//	return GL.glMapBufferRange(target, offset, length, access)?.toByteArray()
//}

//Not supported in JVM:
//expect fun glPauseTransformFeedback()
actual fun glPixelStorei(parameterName: GLenum, param: GLint) {
	GL.glPixelStorei(parameterName, param)
}

actual fun glPolygonOffset(factor: GLfloat, units: GLfloat) {
	GL.glPolygonOffset(factor, units)
}

//Not supported in JVM:
//expect fun glProgramBinary(program: GLuint, binaryFormat: GLenum, binary: voidPointer, length: GLsizei)
//Not supported in JVM:
//expect fun glProgramParameteri(program: GLuint, parameterName: GLenum, value: GLint)
actual fun glReadBuffer(mode: GLenum) {
	GL.glReadBuffer(mode)
}

actual fun glReadPixels(x: GLint, y: GLint, width: GLsizei, height: GLsizei, format: GLenum, type: GLenum, pixels: voidPointer) {
	val pixelsBuffer = pixels.toDirectByteBuffer()
	GL.glReadPixels(x, y, width, height, format, type, pixelsBuffer)
	pixels.put(pixelsBuffer)
}

//Not supported in JVM:
//expect fun glReleaseShaderCompiler()
actual fun glRenderbufferStorage(target: GLenum, internalFormat: GLenum, width: GLsizei, height: GLsizei) {
	GL.glRenderbufferStorage(target, internalFormat, width, height)
}

actual fun glRenderbufferStorageMultisample(target: GLenum, samples: GLsizei, internalFormat: GLenum, width: GLsizei, height: GLsizei) {
	GL.glRenderbufferStorageMultisample(target, samples, internalFormat, width, height)
}

//Not supported in JVM:
//expect fun glResumeTransformFeedback()
actual fun glSampleCoverage(value: GLfloat, invert: GLboolean) {
	GL.glSampleCoverage(value, invert)
}

actual fun glSamplerParameterf(sampler: GLuint, parameterName: GLenum, param: GLfloat) {
	GL.glSamplerParameterf(sampler, parameterName, param)
}

//Not supported in JS:
//actual fun glSamplerParameterfv(sampler: GLuint, parameterName: GLenum, param: GLfloatPointer) {
//	GL.glSamplerParameterfv(sampler, parameterName, param)
//}

actual fun glSamplerParameteri(sampler: GLuint, parameterName: GLenum, param: GLint) {
	GL.glSamplerParameteri(sampler, parameterName, param)
}

//Not supported in JS:
//actual fun glSamplerParameteriv(sampler: GLuint, parameterName: GLenum, param: GLintPointer) {
//	GL.glSamplerParameteriv(sampler, parameterName, param)
//}

actual fun glScissor(x: GLint, y: GLint, width: GLsizei, height: GLsizei) {
	GL.glScissor(x, y, width, height)
}

//Not supported in JVM:
//expect fun glShaderBinary(shaders: GLuintArray, binaryFormat: GLenum, binary: voidPointer, length: GLsizei)
actual fun glShaderSource(shader: GLuint, source: GLcharArray) {
	GL.glShaderSource(shader, source)
}

actual fun glStencilFunc(func: GLenum, ref: GLint, mask: GLuint) {
	GL.glStencilFunc(func, ref, mask)
}

actual fun glStencilFuncSeparate(face: GLenum, func: GLenum, ref: GLint, mask: GLuint) {
	GL.glStencilFuncSeparate(face, func, ref, mask)
}

actual fun glStencilMask(mask: GLuint) {
	GL.glStencilMask(mask)
}

actual fun glStencilMaskSeparate(face: GLenum, mask: GLuint) {
	GL.glStencilMaskSeparate(face, mask)
}

actual fun glStencilOp(fail: GLenum, zfail: GLenum, zpass: GLenum) {
	GL.glStencilOp(fail, zfail, zpass)
}

actual fun glStencilOpSeparate(face: GLenum, sourceFail: GLenum, depthFail: GLenum, depthPass: GLenum) {
	GL.glStencilOpSeparate(face, sourceFail, depthFail, depthPass)
}

actual fun glTexImage2D(target: GLenum, level: GLint, internalFormat: GLint, width: GLsizei, height: GLsizei, border: GLint, format: GLenum, type: GLenum, pixels: voidPointer) {
	GL.glTexImage2D(target, level, internalFormat, width, height, border, format, type, pixels.toDirectByteBuffer())
}

actual fun glTexImage3D(target: GLenum, level: GLint, internalFormat: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, border: GLint, format: GLenum, type: GLenum, pixels: voidPointer) {
	GL.glTexImage3D(target, level, internalFormat, width, height, depth, border, format, type, pixels.toDirectByteBuffer())
}

actual fun glTexParameterf(target: GLenum, parameterName: GLenum, param: GLfloat) {
	GL.glTexParameterf(target, parameterName, param)
}

actual fun glTexParameterfv(target: GLenum, parameterName: GLenum, params: GLfloatPointer) {
	GL.glTexParameterfv(target, parameterName, params, 0)
}

actual fun glTexParameteri(target: GLenum, parameterName: GLenum, param: GLint) {
	GL.glTexParameteri(target, parameterName, param)
}

actual fun glTexParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
	GL.glTexParameteriv(target, parameterName, params, 0)
}

//Not supported in JVM:
//expect fun glTexStorage2D(target: GLenum, levels: GLsizei, internalFormat: GLenum, width: GLsizei, height: GLsizei)
//Not supported in JVM:
//expect fun glTexStorage3D(target: GLenum, levels: GLsizei, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei)
actual fun glTexSubImage2D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, width: GLsizei, height: GLsizei, format: GLenum, type: GLenum, pixels: voidPointer) {
	GL.glTexSubImage2D(target, level, xOffset, yOffset, width, height, format, type, pixels.toDirectByteBuffer())
}

actual fun glTexSubImage3D(target: GLenum, level: GLint, xOffset: GLint, yOffset: GLint, zOffset: GLint, width: GLsizei, height: GLsizei, depth: GLsizei, format: GLenum, type: GLenum, pixels: voidPointer) {
	GL.glTexSubImage3D(target, level, xOffset, yOffset, zOffset, width, height, depth, format, type, pixels.toDirectByteBuffer())
}

actual fun glTransformFeedbackVaryings(program: GLuint, varyings: Array<String>, bufferMode: GLenum) {
	GL.glTransformFeedbackVaryings(program, varyings, bufferMode)
}

actual fun glUniform1f(location: GLLocation, v0: GLfloat) {
	GL.glUniform1f(location as GLuint, v0)
}

actual fun glUniform1fv(location: GLLocation, value: GLfloatArray) {
	GL.glUniform1fv(location as GLuint, value.size, value, 0)
}

actual fun glUniform1i(location: GLLocation, v0: GLint) {
	GL.glUniform1i(location as GLuint, v0)
}

actual fun glUniform1iv(location: GLLocation, value: GLintArray) {
	GL.glUniform1iv(location as GLuint, value.size, value, 0)
}

actual fun glUniform1ui(location: GLLocation, v0: GLuint) {
	GL.glUniform1ui(location as GLuint, v0)
}

actual fun glUniform1uiv(location: GLLocation, value: GLuintArray) {
	GL.glUniform1uiv(location as GLuint, value.size, value, 0)
}

actual fun glUniform2f(location: GLLocation, v0: GLfloat, v1: GLfloat) {
	GL.glUniform2f(location as GLuint, v0, v1)
}

actual fun glUniform2fv(location: GLLocation, value: GLfloatArray) {
	GL.glUniform2fv(location as GLuint, value.size, value, 0)
}

actual fun glUniform2i(location: GLLocation, v0: GLint, v1: GLint) {
	GL.glUniform2i(location as GLuint, v0, v1)
}

actual fun glUniform2iv(location: GLLocation, value: GLintArray) {
	GL.glUniform2iv(location as GLuint, value.size, value, 0)
}

actual fun glUniform2ui(location: GLLocation, v0: GLuint, v1: GLuint) {
	GL.glUniform2ui(location as GLuint, v0, v1)
}

actual fun glUniform2uiv(location: GLLocation, value: GLuintArray) {
	GL.glUniform2uiv(location as GLuint, value.size, value, 0)
}

actual fun glUniform3f(location: GLLocation, v0: GLfloat, v1: GLfloat, v2: GLfloat) {
	GL.glUniform3f(location as GLuint, v0, v1, v2)
}

actual fun glUniform3fv(location: GLLocation, value: GLfloatArray) {
	GL.glUniform3fv(location as GLuint, value.size, value, 0)
}

actual fun glUniform3i(location: GLLocation, v0: GLint, v1: GLint, v2: GLint) {
	GL.glUniform3i(location as GLuint, v0, v1, v2)
}

actual fun glUniform3iv(location: GLLocation, value: GLintArray) {
	GL.glUniform3iv(location as GLuint, value.size, value, 0)
}

actual fun glUniform3ui(location: GLLocation, v0: GLuint, v1: GLuint, v2: GLuint) {
	GL.glUniform3ui(location as GLuint, v0, v1, v2)
}

actual fun glUniform3uiv(location: GLLocation, value: GLuintArray) {
	GL.glUniform3uiv(location as GLuint, value.size, value, 0)
}

actual fun glUniform4f(location: GLLocation, v0: GLfloat, v1: GLfloat, v2: GLfloat, v3: GLfloat) {
	GL.glUniform4f(location as GLuint, v0, v1, v2, v3)
}

actual fun glUniform4fv(location: GLLocation, value: GLfloatArray) {
	GL.glUniform4fv(location as GLuint, value.size, value, 0)
}

actual fun glUniform4i(location: GLLocation, v0: GLint, v1: GLint, v2: GLint, v3: GLint) {
	GL.glUniform4i(location as GLuint, v0, v1, v2, v3)
}

actual fun glUniform4iv(location: GLLocation, value: GLintArray) {
	GL.glUniform4iv(location as GLuint, value.size, value, 0)
}

actual fun glUniform4ui(location: GLLocation, v0: GLuint, v1: GLuint, v2: GLuint, v3: GLuint) {
	GL.glUniform4ui(location as GLuint, v0, v1, v2, v3)
}

actual fun glUniform4uiv(location: GLLocation, value: GLuintArray) {
	GL.glUniform4uiv(location as GLuint, value.size, value, 0)
}

actual fun glUniformBlockBinding(program: GLuint, uniformBlockIndex: GLuint, uniformBlockBinding: GLuint) {
	GL.glUniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding)
}

actual fun glUniformMatrix2fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix2fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix2x3fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix2x3fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix2x4fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix2x4fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix3fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix3fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix3x2fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix3x2fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix3x4fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix3x4fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix4fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix4fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix4x2fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix4x2fv(location as GLuint, 1, transpose, value, 0)
}

actual fun glUniformMatrix4x3fv(location: GLLocation, transpose: GLboolean, value: GLfloatArray) {
	GL.glUniformMatrix4x3fv(location as GLuint, 1, transpose, value, 0)
}

//Not supported in JS:
//actual fun glUnmapBuffer(target: GLenum): GLboolean {
//	return GL.glUnmapBuffer(target)
//}

actual fun glUseProgram(program: GLuint) {
	GL.glUseProgram(program)
}

actual fun glValidateProgram(program: GLuint) {
	GL.glValidateProgram(program)
}

actual fun glVertexAttrib1f(index: GLuint, x: GLfloat) {
	GL.glVertexAttrib1f(index, x)
}

actual fun glVertexAttrib1fv(index: GLuint, v: GLfloatPointer) {
	GL.glVertexAttrib1fv(index, v, 0)
}

actual fun glVertexAttrib2f(index: GLuint, x: GLfloat, y: GLfloat) {
	GL.glVertexAttrib2f(index, x, y)
}

actual fun glVertexAttrib2fv(index: GLuint, v: GLfloatPointer) {
	GL.glVertexAttrib2fv(index, v, 0)
}

actual fun glVertexAttrib3f(index: GLuint, x: GLfloat, y: GLfloat, z: GLfloat) {
	GL.glVertexAttrib3f(index, x, y, z)
}

actual fun glVertexAttrib3fv(index: GLuint, v: GLfloatPointer) {
	GL.glVertexAttrib3fv(index, v, 0)
}

actual fun glVertexAttrib4f(index: GLuint, x: GLfloat, y: GLfloat, z: GLfloat, w: GLfloat) {
	GL.glVertexAttrib4f(index, x, y, z, w)
}

actual fun glVertexAttrib4fv(index: GLuint, v: GLfloatPointer) {
	GL.glVertexAttrib4fv(index, v, 0)
}

actual fun glVertexAttribDivisor(index: GLuint, divisor: GLuint) {
	GL.glVertexAttribDivisor(index, divisor)
}

actual fun glVertexAttribI4i(index: GLuint, x: GLint, y: GLint, z: GLint, w: GLint) {
	GL.glVertexAttribI4i(index, x, y, z, w)
}

actual fun glVertexAttribI4iv(index: GLuint, v: GLintPointer) {
	GL.glVertexAttribI4iv(index, v, 0)
}

actual fun glVertexAttribI4ui(index: GLuint, x: GLuint, y: GLuint, z: GLuint, w: GLuint) {
	GL.glVertexAttribI4ui(index, x, y, z, w)
}

actual fun glVertexAttribI4uiv(index: GLuint, v: GLuintPointer) {
	GL.glVertexAttribI4uiv(index, v, 0)
}

actual fun glVertexAttribIPointer(index: GLuint, size: GLint, type: GLenum, stride: GLsizei, offset: GLsizei) {
	GL.glVertexAttribIPointer(index, size, type, stride, offset)
}

actual fun glVertexAttribPointer(index: GLuint, size: GLint, type: GLenum, normalized: GLboolean, stride: GLsizei, offset: GLsizei) {
	GL.glVertexAttribPointer(index, size, type, normalized, stride, offset)
}

actual fun glViewport(x: GLint, y: GLint, width: GLsizei, height: GLsizei) {
	GL.glViewport(x, y, width, height)
}

actual fun glWaitSync(sync: GLsync, flags: GLbitfield, timeout: GLuint64) {
	GL.glWaitSync(sync as Long, flags, timeout)
}

