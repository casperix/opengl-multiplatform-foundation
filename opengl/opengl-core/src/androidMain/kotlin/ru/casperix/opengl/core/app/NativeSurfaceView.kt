package ru.casperix.opengl.core.app

import android.content.Context
import android.opengl.GLSurfaceView
import android.view.KeyEvent
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener

class NativeSurfaceView(context: Context) : GLSurfaceView(context) {
    val impl = AndroidSurfaceImpl(this)

    init {
        isFocusableInTouchMode = true
    }

    fun setCreator(creator: ((Surface) -> SurfaceListener)) {
        setEGLContextClientVersion(3)

        val container = NativeSurfaceRenderer(impl, creator)
        AndroidInput(this, container::input)
        setRenderer(container)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        return true
    }

}
