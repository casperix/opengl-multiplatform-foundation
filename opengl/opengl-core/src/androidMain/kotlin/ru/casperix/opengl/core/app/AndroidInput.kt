package ru.casperix.opengl.core.app

import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.View.OnKeyListener
import android.view.View.OnTouchListener
import io.github.oshai.kotlinlogging.KotlinLogging
import ru.casperix.input.*
import ru.casperix.math.vector.vectorOf

class AndroidInput(view: View, val consumer: (InputEvent) -> Unit) : OnKeyListener, OnTouchListener {
    private val logger = KotlinLogging.logger { }

    init {
        view.setOnKeyListener(this)
        view.setOnTouchListener(this)
    }

    override fun onTouch(p0: View, event: MotionEvent): Boolean {
        logger.trace { event }

        val position = vectorOf(event.x, event.y)

        return dispatchEvent(
            when (event.actionMasked) {
                MotionEvent.ACTION_DOWN -> TouchDown(position, 0, PointerButton.LEFT)
                MotionEvent.ACTION_MOVE -> TouchMove(position, 0)
                MotionEvent.ACTION_UP -> TouchUp(position, 0, PointerButton.LEFT)
                else -> null
            }
        )
    }

    private fun dispatchEvent(event: InputEvent?): Boolean {
        if (event == null) return false
        consumer(event)
        return true//event.captured
    }

    override fun onKey(p0: View, p1: Int, event: KeyEvent): Boolean {
        logger.trace { event }

        return when (event.action) {
            KeyEvent.ACTION_DOWN -> {
                dispatchEvent(KeyDown(getKeyButton(event.keyCode)))
                dispatchEvent(KeyTyped(event.unicodeChar.toChar(), getKeyButton(event.keyCode)))
                true
            }

            KeyEvent.ACTION_UP -> {
                dispatchEvent(KeyUp(getKeyButton(event.keyCode)))
                true
            }

            else -> false
        }

    }

    private fun getKeyButton(keyCode: Int): KeyButton {
        return KeyMap.androidToApi[keyCode] ?: KeyButton.UNKNOWN_KEY
    }
}