package ru.casperix.opengl.core.app

import android.app.Activity
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.multiplatform.clipboard.AndroidClipboard

fun androidSurfaceLauncher(activity: Activity, creator: (Surface) -> SurfaceListener) {
    AndroidClipboard.setup(activity)

    activity.setContentView(NativeSurfaceView(activity).apply {
        setCreator(creator)
    })
}