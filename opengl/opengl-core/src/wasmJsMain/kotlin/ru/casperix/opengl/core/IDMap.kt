package ru.casperix.opengl.core

class IDMap<T> {
	private val map = mutableMapOf<Int, T>()
	private var lastIndex = 0

	fun addOrNegative(value: T?): Int {
		if (value == null) return -1

		val id = ++lastIndex
		map[id] = value
		return id
	}

	fun remove(id: Int):T? {
		return map.remove(id)
	}

	fun get(buffer: Int): T? {
		return map[buffer]
	}

	fun search(type:T):Int {
		map.forEach { (id, next)->
			if (type == next) return id
		}
		return -1
	}
}