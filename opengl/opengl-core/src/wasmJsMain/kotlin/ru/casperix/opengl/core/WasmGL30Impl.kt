package ru.casperix.opengl.core

import WebGL2RenderingContext
import WebGLSync
import ru.casperix.opengl.core.misc.GLErrorPrinter
import org.khronos.webgl.WebGLUniformLocation

internal lateinit var GL: WebGL2RenderingContext

private fun assertError() {
    if (!GLErrorPrinter.debugThrowGlError) return
    GLErrorPrinter.throwIfExist()
}

actual fun glActiveTexture(texture: Int) {
    GL.activeTexture(texture)
    assertError()
}

actual fun glAttachShader(program: Int, shader: Int) {
    GL.attachShader(programMap.get(program), shaderMap.get(shader))
    assertError()
}


actual fun glBeginQuery(target: GLenum, query: GLuint) {
    val glQuery = queryMap.get(query) ?: return
    GL.beginQuery(target, glQuery)
    assertError()
}


actual fun glBeginTransformFeedback(primitiveMode: Int) {
    GL.beginTransformFeedback(primitiveMode)
    assertError()
}


actual fun glBindAttribLocation(program: Int, index: Int, name: String) {
    GL.bindAttribLocation(programMap.get(program), index, name)
    assertError()
}


actual fun glBindBuffer(target: Int, buffer: Int) {
    val value = bufferMap.get(buffer)
    GL.bindBuffer(target, value)
    assertError()
}


actual fun glBindBufferBase(target: Int, index: Int, buffer: Int) {
    val value = bufferMap.get(buffer)
    GL.bindBufferBase(target, index, value)
    assertError()
}


actual fun glBindBufferRange(target: GLenum, index: GLuint, buffer: GLuint, offset: GLintptr, size: GLsizeiptr) {
    val value = bufferMap.get(buffer)
    GL.bindBufferRange(target, index, value, offset, size.toLong())
    assertError()
}


actual fun glBindFramebuffer(target: Int, frameBuffer: Int) {
    GL.bindFramebuffer(target, frameBufferMap.get(frameBuffer))
    assertError()
}


actual fun glBindRenderbuffer(target: Int, renderBuffer: Int) {
    GL.bindRenderbuffer(target, renderBufferMap.get(renderBuffer))
    assertError()
}


actual fun glBindSampler(unit: Int, sampler: Int) {
    GL.bindSampler(unit, samplerMap.get(sampler))
    assertError()
}


actual fun glBindTexture(target: Int, texture: Int) {
    GL.bindTexture(target, textureMap.get(texture))
    assertError()
}


actual fun glBindVertexArray(array: Int) {
    GL.bindVertexArray(VAOMap.get(array))
    assertError()
}


actual fun glBlendColor(red: Float, green: Float, blue: Float, alpha: Float) {
    GL.blendColor(red, green, blue, alpha)
    assertError()
}


actual fun glBlendEquation(mode: Int) {
    GL.blendEquation(mode)
    assertError()
}


actual fun glBlendEquationSeparate(modeRGB: Int, modeAlpha: Int) {
    GL.blendEquationSeparate(modeRGB, modeAlpha)
    assertError()
}


actual fun glBlendFunc(sourceFactor: GLenum, destFactor: GLenum) {
    GL.blendFunc(sourceFactor, destFactor)
    assertError()
}


actual fun glBlendFuncSeparate(sourceRGB: GLenum, destRGB: GLenum, sourceAlpha: GLenum, destAlpha: GLenum) {
    GL.blendFuncSeparate(sourceRGB, destRGB, sourceAlpha, destAlpha)
    assertError()
}


actual fun glBlitFramebuffer(
    srcX0: Int,
    srcY0: Int,
    srcX1: Int,
    srcY1: Int,
    dstX0: Int,
    dstY0: Int,
    dstX1: Int,
    dstY1: Int,
    mask: Int,
    filter: Int
) {
    GL.blitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter)
    assertError()
}


actual fun glBufferData(target: GLenum, clientData: ByteArray, usage: GLenum) {
    GL.bufferData(target, clientData.toJsInt8Array(), usage)
    assertError()
}


actual fun glBufferData(target: GLenum, clientData: ShortArray, usage: GLenum) {
    GL.bufferData(target, clientData.toJsInt16Array(), usage)
    assertError()
}


actual fun glBufferData(target: GLenum, clientData: IntArray, usage: GLenum) {
    GL.bufferData(target, clientData.toJsInt32Array(), usage)
    assertError()
}


actual fun glBufferData(target: GLenum, clientData: LongArray, usage: GLenum) {
    GL.bufferData(target, clientData.toJsBigInt64Array(), usage)
    assertError()
}


actual fun glBufferData(target: GLenum, clientData: FloatArray, usage: GLenum) {
    GL.bufferData(target, clientData.toJsFloat32Array(), usage)
    assertError()
}


actual fun glBufferData(target: GLenum, clientData: DoubleArray, usage: GLenum) {
    GL.bufferData(target, clientData.toJsFloat64Array(), usage)
    assertError()
}


actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: ByteArray) {
    GL.bufferSubData(target, offset.toInt(), clientData.toJsInt8Array())
    assertError()
}


actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: ShortArray) {
    GL.bufferSubData(target, offset.toInt(), clientData.toJsInt16Array())
    assertError()
}


actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: IntArray) {
    GL.bufferSubData(target, offset.toInt(), clientData.toJsInt32Array())
    assertError()
}


actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: LongArray) {
    GL.bufferSubData(target, offset.toInt(), clientData.toJsBigInt64Array())
    assertError()
}


actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: FloatArray) {
    GL.bufferSubData(target, offset.toInt(), clientData.toJsFloat32Array())
    assertError()
}


actual fun glBufferSubData(target: GLenum, offset: GLintptr, clientData: DoubleArray) {
    GL.bufferSubData(target, offset.toInt(), clientData.toJsFloat64Array())
    assertError()
}


actual fun glCheckFramebufferStatus(target: Int): Int {
    return GL.checkFramebufferStatus(target).apply {
        assertError()
    }
}


actual fun glClear(mask: Int) {
    GL.clear(mask)
    assertError()
}


actual fun glClearBufferfi(buffer: GLenum, drawBuffer: GLint, depth: GLfloat, stencil: GLint) {
    GL.clearBufferfi(buffer, drawBuffer, depth, stencil)
    assertError()
}


actual fun glClearBufferfv(buffer: GLenum, drawBuffer: GLint, value: GLfloatPointer) {
    GL.clearBufferfv(buffer, drawBuffer, value.toFloat32List(), null)
    assertError()
}


actual fun glClearBufferiv(buffer: GLenum, drawBuffer: GLint, value: GLintPointer) {
    GL.clearBufferiv(buffer, drawBuffer, value.toInt32List(), null)
    assertError()
}


actual fun glClearBufferuiv(buffer: GLenum, drawBuffer: GLint, value: GLuintPointer) {
    GL.clearBufferuiv(buffer, drawBuffer, value.toUint32List(), null)
    assertError()
}


actual fun glClearColor(red: Float, green: Float, blue: Float, alpha: Float) {
    GL.clearColor(red, green, blue, alpha)
    assertError()
}


actual fun glClearDepth(depth: GLclampd) {
    GL.clearDepth(depth.toFloat())
    assertError()
}


actual fun glClearStencil(s: Int) {
    GL.clearStencil(s)
    assertError()
}


actual fun glClientWaitSync(sync: GLsync, flags: GLbitfield, timeout: GLuint64): GLenum {
    return GL.clientWaitSync(sync as WebGLSync, flags, timeout).apply {
        assertError()
    }
}


actual fun glColorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean) {
    GL.colorMask(red, green, blue, alpha)
    assertError()
}


actual fun glCompileShader(shader: Int) {
    GL.compileShader(shaderMap.get(shader))
    assertError()
}


actual fun glCompressedTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    clientData: ByteArray
) {
    GL.compressedTexImage2D(target, level, internalFormat, width, height, border, clientData.toJsInt8Array())
    assertError()
}


actual fun glCompressedTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    clientData: ShortArray
) {
    GL.compressedTexImage2D(target, level, internalFormat, width, height, border, clientData.toJsInt16Array())
    assertError()
}


actual fun glCompressedTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    clientData: IntArray
) {
    GL.compressedTexImage2D(target, level, internalFormat, width, height, border, clientData.toJsInt32Array())
    assertError()
}


actual fun glCompressedTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    clientData: LongArray
) {
    GL.compressedTexImage2D(target, level, internalFormat, width, height, border, clientData.toJsBigInt64Array())
    assertError()
}


actual fun glCompressedTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    clientData: FloatArray
) {
    GL.compressedTexImage2D(target, level, internalFormat, width, height, border, clientData.toJsFloat32Array())
    assertError()
}


actual fun glCompressedTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    clientData: DoubleArray
) {
    GL.compressedTexImage2D(target, level, internalFormat, width, height, border, clientData.toJsFloat64Array())
    assertError()
}


actual fun glCompressedTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    clientData: ByteArray
) {
    GL.compressedTexImage3D(
        target,
        level,
        internalFormat,
        width,
        height,
        depth,
        border,
        clientData.toJsInt8Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    clientData: ShortArray
) {
    GL.compressedTexImage3D(
        target,
        level,
        internalFormat,
        width,
        height,
        depth,
        border,
        clientData.toJsInt16Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    clientData: IntArray
) {
    GL.compressedTexImage3D(
        target,
        level,
        internalFormat,
        width,
        height,
        depth,
        border,
        clientData.toJsInt32Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    clientData: LongArray
) {
    GL.compressedTexImage3D(
        target,
        level,
        internalFormat,
        width,
        height,
        depth,
        border,
        clientData.toJsBigInt64Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    clientData: FloatArray
) {
    GL.compressedTexImage3D(
        target,
        level,
        internalFormat,
        width,
        height,
        depth,
        border,
        clientData.toJsFloat32Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    clientData: DoubleArray
) {
    GL.compressedTexImage3D(
        target,
        level,
        internalFormat,
        width,
        height,
        depth,
        border,
        clientData.toJsFloat64Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    clientData: ByteArray
) {
    GL.compressedTexSubImage2D(
        target,
        level,
        xOffset,
        yOffset,
        width,
        height,
        format,
        clientData.toJsInt8Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    clientData: ShortArray
) {
    GL.compressedTexSubImage2D(
        target,
        level,
        xOffset,
        yOffset,
        width,
        height,
        format,
        clientData.toJsInt16Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    clientData: IntArray
) {
    GL.compressedTexSubImage2D(
        target,
        level,
        xOffset,
        yOffset,
        width,
        height,
        format,
        clientData.toJsInt32Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    clientData: LongArray
) {
    GL.compressedTexSubImage2D(
        target,
        level,
        xOffset,
        yOffset,
        width,
        height,
        format,
        clientData.toJsBigInt64Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    clientData: FloatArray
) {
    GL.compressedTexSubImage2D(
        target,
        level,
        xOffset,
        yOffset,
        width,
        height,
        format,
        clientData.toJsFloat32Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    clientData: DoubleArray
) {
    GL.compressedTexSubImage2D(
        target,
        level,
        xOffset,
        yOffset,
        width,
        height,
        format,
        clientData.toJsFloat64Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    clientData: ByteArray
) {
    GL.compressedTexSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        clientData.toJsInt8Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    clientData: ShortArray
) {
    GL.compressedTexSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        clientData.toJsInt16Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    clientData: IntArray
) {
    GL.compressedTexSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        clientData.toJsInt32Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    clientData: LongArray
) {
    GL.compressedTexSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        clientData.toJsBigInt64Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    clientData: FloatArray
) {
    GL.compressedTexSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        clientData.toJsFloat32Array(),
        null,
        null
    )
    assertError()
}


actual fun glCompressedTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    clientData: DoubleArray
) {
    GL.compressedTexSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        clientData.toJsFloat64Array(),
        null,
        null
    )
    assertError()
}


actual fun glCopyBufferSubData(
    readTarget: GLenum,
    writeTarget: GLenum,
    readOffset: GLintptr,
    writeOffset: GLintptr,
    size: GLsizeiptr
) {
    GL.copyBufferSubData(readTarget, writeTarget, readOffset, writeOffset, size)
    assertError()
}


actual fun glCopyTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLenum,
    x: GLint,
    y: GLint,
    width: GLsizei,
    height: GLsizei,
    border: GLint
) {
    GL.copyTexImage2D(target, level, internalFormat, x, y, width, height, border)
    assertError()
}


actual fun glCopyTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    x: GLint,
    y: GLint,
    width: GLsizei,
    height: GLsizei
) {
    GL.copyTexSubImage2D(target, level, xOffset, yOffset, x, y, width, height)
    assertError()
}


actual fun glCopyTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    x: GLint,
    y: GLint,
    width: GLsizei,
    height: GLsizei
) {
    GL.copyTexSubImage3D(target, level, xOffset, yOffset, zOffset, x, y, width, height)
    assertError()
}


actual fun glCreateProgram(): Int {
    val program = GL.createProgram()
    assertError()
    return programMap.addOrNegative(program)
}


actual fun glCreateShader(type: Int): Int {
    val shader = GL.createShader(type)
    assertError()
    return shaderMap.addOrNegative(shader)
}


actual fun glCullFace(mode: Int) {
    GL.cullFace(mode)
    assertError()
}


actual fun glDeleteBuffers(buffers: IntArray) {
    buffers.forEach { buffer ->
        GL.deleteBuffer(bufferMap.remove(buffer))
        assertError()
}

    assertError()
}



actual fun glDeleteFramebuffers(frameBuffers: IntArray) {
    frameBuffers.forEach { buffer ->
        GL.deleteFramebuffer(frameBufferMap.remove(buffer))
        assertError()
}

    assertError()
}


actual fun glDeleteProgram(program: Int) {
    GL.deleteProgram(programMap.remove(program))
    assertError()
}


actual fun glDeleteQueries(ids: IntArray) {
    ids.forEach {
        GL.deleteQuery(queryMap.get(it))
        assertError()
}

    assertError()
}


actual fun glDeleteRenderbuffers(renderBuffers: IntArray) {
    renderBuffers.forEach { buffer ->
        GL.deleteRenderbuffer(renderBufferMap.remove(buffer))
        assertError()
}

    assertError()
}


actual fun glDeleteSamplers(samplers: IntArray) {
    samplers.forEach {
        GL.deleteSampler(samplerMap.get(it))
        assertError()
}

    assertError()
}


actual fun glDeleteShader(shader: Int) {
    GL.deleteShader(shaderMap.remove(shader))
    assertError()
}


actual fun glDeleteSync(sync: GLsync) {
    GL.deleteSync(sync as WebGLSync)
    assertError()
}


actual fun glDeleteTextures(textures: IntArray) {
    textures.forEach { texture ->
        GL.deleteTexture(textureMap.remove(texture))
        assertError()
}

    assertError()
}


actual fun glDeleteVertexArrays(arrays: GLuintArray) {
    arrays.forEach {
        GL.deleteVertexArray(VAOMap.remove(it))
        assertError()
}

    assertError()
}


actual fun glDepthFunc(func: Int) {
    GL.depthFunc(func)
    assertError()
}


actual fun glDepthMask(flag: Boolean) {
    GL.depthMask(flag)
    assertError()
}


actual fun glDepthRange(near: GLdouble, far: GLdouble) {
    GL.depthRange(near.toFloat(), far.toFloat())
    assertError()
}


actual fun glDepthRangef(near: GLfloat, far: GLfloat) {
    GL.depthRange(near, far)
    assertError()
}


actual fun glDetachShader(program: Int, shader: Int) {
    GL.detachShader(programMap.get(program), shaderMap.get(shader))
    assertError()
}


actual fun glDisable(capability: GLenum) {
    GL.disable(capability)
    assertError()
}


actual fun glDisableVertexAttribArray(index: Int) {
    GL.disableVertexAttribArray(index)
    assertError()
}


actual fun glDrawArrays(mode: GLenum, vertexOffset: GLint, vertexCount: GLsizei) {
    GL.drawArrays(mode, vertexOffset, vertexCount)
    assertError()
}


actual fun glDrawArraysInstanced(mode: GLenum, vertexOffset: GLint, vertexCount: GLsizei, instanceCount: GLsizei) {
    GL.drawArraysInstanced(mode, vertexOffset, vertexCount, instanceCount)
    assertError()
}


actual fun glDrawBuffers(bufs: GLenumArray) {
    GL.drawBuffers(bufs.toJsNumberArray())
    assertError()
}


actual fun glDrawElements(mode: GLenum, count: GLsizei, type: GLenum, indicesOffset: GLintptr) {
    GL.drawElements(mode, count, type, indicesOffset.toInt())
    assertError()
}


actual fun glDrawElementsInstanced(
    mode: GLenum,
    count: GLsizei,
    type: GLenum,
    offset: GLintptr,
    instanceCount: GLsizei
) {
    GL.drawElementsInstanced(mode, count, type, offset, instanceCount)
    assertError()
}


actual fun glDrawRangeElements(
    mode: GLenum,
    start: GLuint,
    end: GLuint,
    count: GLsizei,
    type: GLenum,
    offset: GLintptr
) {
    GL.drawRangeElements(mode, start, end, count, type, offset)
    assertError()
}


actual fun glEnable(capability: GLenum) {
    GL.enable(capability)
    assertError()
}


actual fun glEnableVertexAttribArray(index: GLuint) {
    GL.enableVertexAttribArray(index)
    assertError()
}


actual fun glEndQuery(target: GLenum) {
    GL.endQuery(target)
    assertError()
}


actual fun glEndTransformFeedback() {
    GL.endTransformFeedback()
    assertError()
}


actual fun glFenceSync(condition: GLenum, flags: GLbitfield): GLsync {
    return GL.fenceSync(condition, flags)!!.apply {
        assertError()
    }
}


actual fun glFinish() {
    GL.finish()
    assertError()
}


actual fun glFlush() {
    GL.flush()
    assertError()
}


actual fun glFramebufferRenderbuffer(
    target: GLenum,
    attachment: GLenum,
    renderBuffertarget: GLenum,
    renderBuffer: GLuint
) {
    GL.framebufferRenderbuffer(target, attachment, renderBuffertarget, renderBufferMap.get(renderBuffer))
    assertError()
}


actual fun glFramebufferTexture2D(
    target: GLenum,
    attachment: GLenum,
    textureTarget: GLenum,
    texture: GLuint,
    level: GLint
) {
    GL.framebufferTexture2D(target, attachment, textureTarget, textureMap.get(texture), level)
    assertError()
}


actual fun glFramebufferTextureLayer(target: GLenum, attachment: GLenum, texture: GLuint, level: GLint, layer: GLint) {
    GL.framebufferTextureLayer(target, attachment, textureMap.get(texture), level, layer)
    assertError()
}


actual fun glFrontFace(mode: Int) {
    GL.frontFace(mode)
    assertError()
}



actual fun glGenBuffers(buffers: GLuintArray) {
    buffers.indices.forEach {
        buffers[it] = bufferMap.addOrNegative(GL.createBuffer())
        assertError()
}

    assertError()
}


actual fun glGenFramebuffers(frameBuffers: GLuintArray) {
    frameBuffers.indices.forEach {
        frameBuffers[it] = frameBufferMap.addOrNegative(GL.createFramebuffer())
        assertError()
}

    assertError()
}


actual fun glGenQueries(ids: IntArray) {
    ids.indices.forEach { id ->
        ids[id] = queryMap.addOrNegative(GL.createQuery())
        assertError()
}

    assertError()
}


actual fun glGenRenderbuffers(renderBuffers: GLuintArray) {
    renderBuffers.indices.forEach {
        renderBuffers[it] = renderBufferMap.addOrNegative(GL.createRenderbuffer())
        assertError()
}

    assertError()
}


actual fun glGenSamplers(samplers: IntArray) {
    samplers.indices.forEach { index ->
        samplers[index] = samplerMap.addOrNegative(GL.createSampler())
        assertError()
}

    assertError()
}


actual fun glGenTextures(textures: GLuintArray) {
    textures.indices.forEach { id ->
        textures[id] = textureMap.addOrNegative(GL.createTexture())
        assertError()
}

    assertError()
}


actual fun glGenVertexArrays(vertexArrays: GLuintArray) {
    vertexArrays.indices.forEach {
        vertexArrays[it] = VAOMap.addOrNegative(GL.createVertexArray())
        assertError()
}

    assertError()
}


actual fun glGenerateMipmap(target: Int) {
    GL.generateMipmap(target)
    assertError()
}


actual fun glGetActiveAttrib(program: GLuint, index: GLuint): GLActiveInfo? {
    val attribute = GL.getActiveAttrib(programMap.get(program), index) ?: return null
    assertError()
    return attribute.toGLActiveInfo()
}


actual fun glGetActiveUniform(program: GLuint, index: GLuint): GLActiveInfo? {
    val uniform = GL.getActiveUniform(programMap.get(program), index) ?: return null
    assertError()
    return uniform.toGLActiveInfo()
}


actual fun glGetActiveUniformBlockName(program: Int, uniformBlockIndex: Int): String? {
    val glProgram = programMap.get(program) ?: return null
    assertError()
    return GL.getActiveUniformBlockName(glProgram, uniformBlockIndex)
}


actual fun glGetActiveUniformBlockiv(
    program: GLuint,
    uniformBlockIndex: GLuint,
    parameterName: GLenum,
    outputParams: GLintPointer
) {
    val glProgram = programMap.get(program) ?: return
    GLParameters.decodeToIntArray(
        GL.getActiveUniformBlockParameter(glProgram, uniformBlockIndex, parameterName),
        outputParams
    )
    assertError()
}


actual fun glGetActiveUniformsiv(
    program: GLuint,
    uniformIndices: GLuintArray,
    parameterName: GLenum,
    outputParams: GLintPointer
) {
    val glProgram = programMap.get(program) ?: return
    uniformIndices.forEachIndexed { index, uniformIndex ->
        val result = GL.getActiveUniform(glProgram, uniformIndex)
        if (result != null) outputParams[index] = result.type
        assertError()
}

    assertError()
}


actual fun glGetAttachedShaders(program: GLuint, count: GLsizeiArray, shaders: GLuintPointer) {
    val shaderInfoArray = GL.getAttachedShaders(programMap.get(program))
    val length =shaderInfoArray?.length ?: 0
    count[0] = length
    (0 until  length).forEach { index->
        val glShader = shaderInfoArray?.get(index)!!
        val shader = shaderMap.search(glShader)
        shaders[index] = shader
        assertError()

    }

    assertError()
}


actual fun glGetAttribLocation(program: GLuint, name: String): Int {
    return GL.getAttribLocation(programMap.get(program), name).apply {
        assertError()
    }
}


//Not implemented yet...:
//expect fun glGetBooleanv(parameterName: GLenum, data: GLbooleanPointer)

actual fun glGetBufferParameteri64v(target: GLenum, parameterName: GLenum, params: GLint64Pointer) {
    GLParameters.decodeToLongArray(GL.getBufferParameter(target, parameterName), params)
    assertError()
}


actual fun glGetBufferParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
    val handle = GL.getBufferParameter(target, parameterName)
    GLParameters.decodeToIntArray(handle, params)
    assertError()
}


actual fun glGetError(): GLenum {
    return GL.getError()
}


actual fun glGetFloatv(parameterName: GLenum, data: GLfloatPointer) {
    val handle = GL.getParameter(parameterName)
    GLParameters.decodeToFloatArray(handle, data)
    assertError()
}


actual fun glGetFragDataLocation(program: GLuint, name: GLcharPointer): Int {
    val glProgram = programMap.get(program) ?: return -1
    return GL.getFragDataLocation(glProgram, name).apply {
        assertError()
    }
}


actual fun glGetFramebufferAttachmentParameteriv(
    target: GLenum,
    attachment: GLenum,
    parameterName: GLenum,
    params: GLintPointer
) {
    val parameter = GL.getFramebufferAttachmentParameter(target, attachment, parameterName)
    GLParameters.decodeToIntArray(parameter, params)
    assertError()
}


actual fun glGetInteger64i_v(target: GLenum, index: GLuint, data: GLint64Pointer) {
    GLParameters.decodeToLongArray(GL.getIndexedParameter(target, index), data)
    assertError()
}


actual fun glGetInteger64v(parameterName: GLenum, data: GLint64Pointer) {
    GLParameters.decodeToLongArray(GL.getParameter(parameterName), data)
    assertError()
}


actual fun glGetIntegeri_v(target: GLenum, index: GLuint, data: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getIndexedParameter(target, index), data)
    assertError()
}


actual fun glGetIntegerv(parameterName: GLenum, data: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getParameter(parameterName), data)
    assertError()
}


//Not supported in JVM:
//expect fun glGetInternalformativ(target: GLenum, internalFormat: GLenum, parameterName: GLenum, params: GLintArray)
//Not supported in JVM:
//expect fun glGetProgramBinary(program: GLuint, length: GLsizeiArray, binaryFormat: GLenumPointer, binary: voidPointer)

actual fun glGetProgramInfoLog(program: GLuint): String? {
    return GL.getProgramInfoLog(programMap.get(program)).apply {
        assertError()
    }
}


actual fun glGetProgramiv(program: GLuint, parameterName: GLenum, params: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getProgramParameter(programMap.get(program), parameterName), params)
    assertError()
}


actual fun glGetQueryObjectuiv(query: GLuint, parameterName: GLenum, params: IntArray) {
    val glQuery = queryMap.get(query) ?: return
    GLParameters.decodeToIntArray(GL.getQueryParameter(glQuery, parameterName), params)
    assertError()
}


actual fun glGetQueryiv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getQuery(target, parameterName), params)
    assertError()
}


actual fun glGetRenderbufferParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getRenderbufferParameter(target, parameterName), params)
    assertError()
}


actual fun glGetSamplerParameterfv(sampler: GLuint, parameterName: GLenum, params: GLfloatPointer) {
    val glSampler = samplerMap.get(sampler) ?: return
    GLParameters.decodeToFloatArray(GL.getSamplerParameter(glSampler, parameterName), params)
    assertError()
}


actual fun glGetSamplerParameteriv(sampler: GLuint, parameterName: GLenum, params: GLintPointer) {
    val glSampler = samplerMap.get(sampler) ?: return
    GLParameters.decodeToIntArray(GL.getSamplerParameter(glSampler, parameterName), params)
    assertError()
}


actual fun glGetShaderInfoLog(shader: GLuint): String? {
    val glShader = shaderMap.get(shader) ?: return null
    return GL.getShaderInfoLog(glShader).apply {
        assertError()
    }
}


actual fun glGetShaderSource(shader: GLuint): String? {
    val glShader = shaderMap.get(shader) ?: return null
    return GL.getShaderSource(glShader).apply {
        assertError()
    }
}


actual fun glGetShaderiv(shader: GLuint, parameterName: GLenum, params: GLintPointer) {
    val parameter = GL.getShaderParameter(shaderMap.get(shader), parameterName)
    GLParameters.decodeToIntArray(parameter, params)
    assertError()
}


actual fun glGetString(name: GLenum): String? {
    val value = GL.getParameter(name)
    assertError()
    return GLParameters.decodeToString(value)
}


actual fun glGetStringi(name: GLenum, index: GLuint): String? {
    val value = GL.getIndexedParameter(name, index)
    assertError()
    return GLParameters.decodeToString(value)
}


actual fun glGetSynci(sync: GLsync, parameterName: GLenum): Int? {
    val value = GL.getSyncParameter(sync as WebGLSync, parameterName)
    assertError()
    return GLParameters.decodeToInt(value)
}


actual fun glGetTexParameterfv(target: GLenum, parameterName: GLenum, params: GLfloatPointer) {
    val value = GL.getTexParameter(target, parameterName)
    assertError()
    GLParameters.decodeToFloatArray(value, params)
}


actual fun glGetTexParameteriv(target: GLenum, parameterName: GLenum, params: GLintPointer) {
    val value = GL.getTexParameter(target, parameterName)
    assertError()
    GLParameters.decodeToIntArray(value, params)
}


actual fun glGetTransformFeedbackVarying(program: GLuint, index: GLuint): GLActiveInfo? {
    val glProgram = programMap.get(program) ?: return null
    val info = GL.getTransformFeedbackVarying(glProgram, index) ?: return null
    assertError()
    return info.toGLActiveInfo()
}


actual fun glGetUniformBlockIndex(program: Int, uniformBlockName: String): Int {
    val glProgram = programMap.get(program) ?: return -1
    return GL.getUniformBlockIndex(glProgram, uniformBlockName).apply {
        assertError()
    }
}


//Not implemented yet...:
//actual fun glGetUniformIndices(program: Int, uniformNames: Array<String>, uniformIndices: IntArray) {
//	val glProgram = programMap.get(program) ?: return
//
//	uniformNames.forEachIndexed { index, uniformName ->
//		val result = GL.getUniformBlockIndex(glProgram, uniformName)
//		uniformIndices[index] = result
//	    assertError()
//}



actual fun glGetUniformLocation(program: GLuint, name: GLcharPointer): GLLocation? {
    val glProgram = programMap.get(program) ?: return null
    val location = GL.getUniformLocation(glProgram, name) ?: return null
    assertError()
    return location
}


actual fun glGetUniformfv(program: GLuint, location: GLLocation, params: GLfloatPointer) {
    GLParameters.decodeToFloatArray(GL.getUniform(programMap.get(program), location as? WebGLUniformLocation), params)
    assertError()
}


actual fun glGetUniformuiv(program: GLuint, location: GLLocation, params: GLuintPointer) {
    GLParameters.decodeToIntArray(GL.getUniform(programMap.get(program), location as? WebGLUniformLocation), params)
    assertError()
}


actual fun glGetUniformiv(program: GLuint, location: GLLocation, params: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getUniform(programMap.get(program), location as? WebGLUniformLocation), params)
    assertError()
}


actual fun glGetVertexAttribIiv(index: GLuint, parameterName: GLenum, params: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getVertexAttrib(index, parameterName), params)
    assertError()
}


actual fun glGetVertexAttribIuiv(index: GLuint, parameterName: GLenum, params: GLuintPointer) {
    GLParameters.decodeToIntArray(GL.getVertexAttrib(index, parameterName), params)
    assertError()
}



actual fun glGetVertexAttribfv(index: GLuint, parameterName: GLenum, params: GLfloatPointer) {
    GLParameters.decodeToFloatArray(GL.getVertexAttrib(index, parameterName), params)
    assertError()
}


actual fun glGetVertexAttribiv(index: GLuint, parameterName: GLenum, params: GLintPointer) {
    GLParameters.decodeToIntArray(GL.getVertexAttrib(index, parameterName), params)
    assertError()
}


actual fun glHint(target: GLenum, mode: GLenum) {
    GL.hint(target, mode)
    assertError()
}


actual fun glIsBuffer(buffer: GLuint): GLboolean {
    return GL.isBuffer(bufferMap.get(buffer)).apply {
        assertError()
    }
}


actual fun glIsEnabled(capability: GLenum): GLboolean {
    return GL.isEnabled(capability).apply {
        assertError()
    }
}


actual fun glIsFramebuffer(frameBuffer: GLuint): GLboolean {
    return GL.isFramebuffer(frameBufferMap.get(frameBuffer)).apply {
        assertError()
    }
}


actual fun glIsProgram(program: GLuint): GLboolean {
    return GL.isProgram(programMap.get(program)).apply {
        assertError()
    }
}


actual fun glIsQuery(query: GLuint): GLboolean {
    val glQuery = queryMap.get(query) ?: return false
    return GL.isQuery(glQuery).apply {
        assertError()
    }
}


actual fun glIsRenderbuffer(renderBuffer: GLuint): GLboolean {
    return GL.isRenderbuffer(renderBufferMap.get(renderBuffer)).apply {
        assertError()
    }
}


actual fun glIsSampler(sampler: GLuint): GLboolean {
    return GL.isSampler(samplerMap.get(sampler)).apply {
        assertError()
    }
}


actual fun glIsShader(shader: GLuint): GLboolean {
    return GL.isShader(shaderMap.get(shader)).apply {
        assertError()
    }
}


actual fun glIsSync(sync: GLsync): GLboolean {
    return GL.isSync(sync as? WebGLSync).apply {
        assertError()
    }
}


actual fun glIsTexture(texture: GLuint): GLboolean {
    return GL.isTexture(textureMap.get(texture)).apply {
        assertError()
    }
}


actual fun glIsVertexArray(array: GLuint): GLboolean {
    val glVAO = VAOMap.get(array) ?: return false
    return GL.isVertexArray(glVAO).apply {
        assertError()
    }
}


actual fun glLineWidth(width: GLfloat) {
    GL.lineWidth(width)
    assertError()
}


actual fun glLinkProgram(program: GLuint) {
    GL.linkProgram(programMap.get(program))
    assertError()
}


//Not supported in JS:
//actual fun glMapBufferRange(target: GLenum, offset: GLintptr, length: GLsizeiptr, access: GLbitfield): voidPointer? {
//	return GL.mapBufferRange(target, offset, length, access)
//    assertError()
//}


actual fun glPixelStorei(parameterName: Int, param: Int) {
    GL.pixelStorei(parameterName, param)
    assertError()
}


actual fun glPolygonOffset(factor: Float, units: Float) {
    GL.polygonOffset(factor, units)
    assertError()
}


actual fun glReadBuffer(mode: GLenum) {
    GL.readBuffer(mode)
    assertError()
}


actual fun glReadPixels(
    x: GLint,
    y: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    type: GLenum,
    pixels: voidPointer
) {
    GL.readPixels(x, y, width, height, format, type, pixels.toJsInt8Array())
    assertError()
}


actual fun glRenderbufferStorage(target: Int, internalFormat: Int, width: Int, height: Int) {
    GL.renderbufferStorage(target, internalFormat, width, height)
    assertError()
}


actual fun glRenderbufferStorageMultisample(
    target: GLenum,
    samples: GLsizei,
    internalFormat: GLenum,
    width: GLsizei,
    height: GLsizei
) {
    GL.renderbufferStorageMultisample(target, samples, internalFormat, width, height)
    assertError()
}


//Not supported in JVM:
//expect fun glResumeTransformFeedback()
actual fun glSampleCoverage(value: GLfloat, invert: GLboolean) {
    GL.sampleCoverage(value, invert)
    assertError()
}


actual fun glSamplerParameterf(sampler: GLuint, parameterName: GLenum, param: GLfloat) {
    val glSampler = samplerMap.get(sampler) ?: return
    GL.samplerParameterf(glSampler, parameterName, param)
    assertError()
}


//Not supported in JS:
//expect fun glSamplerParameterfv(sampler: GLuint, parameterName: GLenum, param: GLfloatPointer)

actual fun glSamplerParameteri(sampler: GLuint, parameterName: GLenum, param: GLint) {
    val glSampler = samplerMap.get(sampler) ?: return
    GL.samplerParameteri(glSampler, parameterName, param)
    assertError()
}


//Not supported in JS:
//expect fun glSamplerParameteriv(sampler: GLuint, parameterName: GLenum, param: GLintPointer)

actual fun glScissor(x: GLint, y: GLint, width: GLsizei, height: GLsizei) {
    GL.scissor(x, y, width, height)
    assertError()
}


//Not supported in JVM:
//expect fun glShaderBinary(shaders: GLuintArray, binaryFormat: GLenum, binary: voidPointer, length: GLsizei)
actual fun glShaderSource(shader: GLuint, source: GLcharArray) {
    GL.shaderSource(shaderMap.get(shader), source)
    assertError()
}


actual fun glStencilFunc(func: Int, ref: Int, mask: Int) {
    GL.stencilFunc(func, ref, mask)
    assertError()
}


actual fun glStencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int) {
    GL.stencilFuncSeparate(face, func, ref, mask)
    assertError()
}



actual fun glStencilMask(mask: Int) {
    GL.stencilMask(mask)
    assertError()
}


actual fun glStencilMaskSeparate(face: Int, mask: Int) {
    GL.stencilMaskSeparate(face, mask)
    assertError()
}


actual fun glStencilOp(fail: Int, zfail: Int, zpass: Int) {
    GL.stencilOp(fail, zfail, zpass)
    assertError()
}


actual fun glStencilOpSeparate(face: GLenum, sourceFail: GLenum, depthFail: GLenum, depthPass: GLenum) {
    GL.stencilOpSeparate(face, sourceFail, depthFail, depthPass)
    assertError()
}


actual fun glTexImage2D(
    target: GLenum,
    level: GLint,
    internalFormat: GLint,
    width: GLsizei,
    height: GLsizei,
    border: GLint,
    format: GLenum,
    type: GLenum,
    pixels: voidPointer
) {
    val data = pixels.toJsUInt8Array()
    GL.texImage2D(target, level, internalFormat, width, height, border, format, type, data)
    assertError()
}


actual fun glTexImage3D(
    target: GLenum,
    level: GLint,
    internalFormat: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    border: GLint,
    format: GLenum,
    type: GLenum,
    pixels: voidPointer
) {
    val data = pixels.toJsUInt8Array()
    GL.texImage3D(target, level, internalFormat, width, height, depth, border, format, type, data)
    assertError()
}


actual fun glTexParameterf(target: GLenum, parameterName: GLenum, param: GLfloat) {
    GL.texParameterf(target, parameterName, param)
    assertError()
}


actual fun glTexParameterfv(target: GLenum, parameterName: GLenum, params: GLfloatPointer) {
    params.forEach { param ->
        GL.texParameterf(target, parameterName, param)
        assertError()
}

    assertError()
}


actual fun glTexParameteri(target: GLenum, parameterName: GLenum, param: GLint) {
    GL.texParameteri(target, parameterName, param)
    assertError()
}


actual fun glTexParameteriv(target: Int, parameterName: Int, params: IntArray) {
    params.forEach { param ->
        GL.texParameteri(target, parameterName, param)
        assertError()
}

    assertError()
}


//Not supported in JVM:
//expect fun glTexStorage2D(target: GLenum, levels: GLsizei, internalFormat: GLenum, width: GLsizei, height: GLsizei)
//Not supported in JVM:
//expect fun glTexStorage3D(target: GLenum, levels: GLsizei, internalFormat: GLenum, width: GLsizei, height: GLsizei, depth: GLsizei)

actual fun glTexSubImage2D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    format: GLenum,
    type: GLenum,
    pixels: voidPointer
) {
    GL.texSubImage2D(target, level, xOffset, yOffset, width, height, format, type, pixels.toJsInt8Array())
    assertError()
}


actual fun glTexSubImage3D(
    target: GLenum,
    level: GLint,
    xOffset: GLint,
    yOffset: GLint,
    zOffset: GLint,
    width: GLsizei,
    height: GLsizei,
    depth: GLsizei,
    format: GLenum,
    type: GLenum,
    pixels: voidPointer
) {
    GL.texSubImage3D(
        target,
        level,
        xOffset,
        yOffset,
        zOffset,
        width,
        height,
        depth,
        format,
        type,
        pixels.toJsInt8Array(),
        null
    )
    assertError()
}


actual fun glTransformFeedbackVaryings(program: GLuint, varyings: Array<String>, bufferMode: Int) {
    val glProgram = programMap.get(program) ?: return
    GL.transformFeedbackVaryings(glProgram, varyings.toJsStringArray(), bufferMode)
    assertError()
}


actual fun glUniform1f(location: GLLocation, v0: GLfloat) {
    GL.uniform1f(location as? WebGLUniformLocation, v0)
    assertError()
}


actual fun glUniform1fv(location: GLLocation, value: GLfloatArray) {
    GL.uniform1fv(location as? WebGLUniformLocation, value.toJsFloat32Array())
    assertError()
}


actual fun glUniform1i(location: GLLocation, v0: GLint) {
    GL.uniform1i(location as? WebGLUniformLocation, v0)
    assertError()
}


actual fun glUniform1iv(location: GLLocation, value: GLintArray) {
    GL.uniform1iv(location as? WebGLUniformLocation, value.toJsInt32Array())
    assertError()
}


actual fun glUniform1ui(location: GLLocation, v0: GLuint) {
    GL.uniform1ui(location as? WebGLUniformLocation, v0)
    assertError()
}


actual fun glUniform1uiv(location: GLLocation, value: GLuintArray) {
    GL.uniform1uiv(location as? WebGLUniformLocation, value.toJsUint32Array(), null, null)
    assertError()
}


actual fun glUniform2f(location: GLLocation, v0: GLfloat, v1: GLfloat) {
    GL.uniform2f(location as? WebGLUniformLocation, v0, v1)
    assertError()
}


actual fun glUniform2fv(location: GLLocation, value: GLfloatArray) {
    GL.uniform2fv(location as? WebGLUniformLocation, value.toJsFloat32Array())
    assertError()
}


actual fun glUniform2i(location: GLLocation, v0: GLint, v1: GLint) {
    GL.uniform2i(location as? WebGLUniformLocation, v0, v1)
    assertError()
}


actual fun glUniform2iv(location: GLLocation, value: GLintArray) {
    GL.uniform2iv(location as? WebGLUniformLocation, value.toJsInt32Array())
    assertError()
}


actual fun glUniform2ui(location: GLLocation, v0: GLuint, v1: GLuint) {
    GL.uniform2ui(location as? WebGLUniformLocation, v0, v1)
    assertError()
}


actual fun glUniform2uiv(location: GLLocation, value: GLuintArray) {
    GL.uniform2uiv(location as? WebGLUniformLocation, value.toUint32List(), null, null)
    assertError()
}


actual fun glUniform3f(location: GLLocation, v0: GLfloat, v1: GLfloat, v2: GLfloat) {
    GL.uniform3f(location as? WebGLUniformLocation, v0, v1, v2)
    assertError()
}


actual fun glUniform3fv(location: GLLocation, value: GLfloatArray) {
    GL.uniform3fv(location as? WebGLUniformLocation, value.toJsFloat32Array())
    assertError()
}


actual fun glUniform3i(location: GLLocation, v0: GLint, v1: GLint, v2: GLint) {
    GL.uniform3i(location as? WebGLUniformLocation, v0, v1, v2)
    assertError()
}


actual fun glUniform3iv(location: GLLocation, value: GLintArray) {
    GL.uniform3iv(location as? WebGLUniformLocation, value.toJsInt32Array())
    assertError()
}


actual fun glUniform3ui(location: GLLocation, v0: GLuint, v1: GLuint, v2: GLuint) {
    GL.uniform3ui(location as? WebGLUniformLocation, v0, v1, v2)
    assertError()
}


actual fun glUniform3uiv(location: GLLocation, value: GLuintArray) {
    GL.uniform3uiv(location as? WebGLUniformLocation, value.toUint32List(), null, null)
    assertError()
}


actual fun glUniform4f(location: GLLocation, v0: GLfloat, v1: GLfloat, v2: GLfloat, v3: GLfloat) {
    GL.uniform4f(location as? WebGLUniformLocation, v0, v1, v2, v3)
    assertError()
}


actual fun glUniform4fv(location: GLLocation, value: GLfloatArray) {
    GL.uniform4fv(location as? WebGLUniformLocation, value.toJsFloat32Array())
    assertError()
}


actual fun glUniform4i(location: GLLocation, v0: GLint, v1: GLint, v2: GLint, v3: GLint) {
    GL.uniform4i(location as? WebGLUniformLocation, v0, v1, v2, v3)
    assertError()
}


actual fun glUniform4iv(location: GLLocation, value: GLintArray) {
    GL.uniform4iv(location as? WebGLUniformLocation, value.toJsInt32Array())
    assertError()
}


actual fun glUniform4ui(location: GLLocation, v0: GLuint, v1: GLuint, v2: GLuint, v3: GLuint) {
    GL.uniform4ui(location as? WebGLUniformLocation, v0, v1, v2, v3)
    assertError()
}


actual fun glUniform4uiv(location: GLLocation, value: GLuintArray) {
    GL.uniform4uiv(location as? WebGLUniformLocation, value.toUint32List(), null, null)
    assertError()
}


actual fun glUniformBlockBinding(program: Int, uniformBlockIndex: Int, uniformBlockBinding: Int) {
    GL.uniformBlockBinding(programMap.get(program)!!, uniformBlockIndex, uniformBlockBinding)
    assertError()
}


actual fun glUniformMatrix2fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix2fv(location as? WebGLUniformLocation, transpose, value.toJsFloat32Array())
    assertError()
}


actual fun glUniformMatrix2x3fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix2x3fv(location as? WebGLUniformLocation, transpose, value.toFloat32List(), null, null)
    assertError()
}


actual fun glUniformMatrix2x4fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix2x4fv(location as? WebGLUniformLocation, transpose, value.toFloat32List(), null, null)
    assertError()
}


actual fun glUniformMatrix3fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix3fv(location as? WebGLUniformLocation, transpose, value.toJsFloat32Array())
    assertError()
}


actual fun glUniformMatrix3x2fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix3x2fv(location as? WebGLUniformLocation, transpose, value.toFloat32List(), null, null)
    assertError()
}


actual fun glUniformMatrix3x4fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix3x4fv(location as? WebGLUniformLocation, transpose, value.toFloat32List(), null, null)
    assertError()
}


actual fun glUniformMatrix4fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix4fv(location as? WebGLUniformLocation, transpose, value.toJsFloat32Array())
    assertError()
}


actual fun glUniformMatrix4x2fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix4x2fv(location as? WebGLUniformLocation, transpose, value.toFloat32List(), null, null)
    assertError()
}


actual fun glUniformMatrix4x3fv(location: GLLocation, transpose: Boolean, value: FloatArray) {
    GL.uniformMatrix4x3fv(location as? WebGLUniformLocation, transpose, value.toFloat32List(), null, null)
    assertError()
}


//Not supported in JS:
//expect fun glUnmapBuffer(target: GLenum):GLboolean

actual fun glUseProgram(program: GLuint) {
    GL.useProgram(programMap.get(program))
    assertError()
}


actual fun glValidateProgram(program: GLuint) {
    GL.validateProgram(programMap.get(program))
    assertError()
}


actual fun glVertexAttrib1f(index: GLuint, x: GLfloat) {
    GL.vertexAttrib1f(index, x)
    assertError()
}


actual fun glVertexAttrib1fv(index: GLuint, v: GLfloatPointer) {
    GL.vertexAttrib1fv(index, v.toJsFloat32Array())
    assertError()
}


actual fun glVertexAttrib2f(index: GLuint, x: GLfloat, y: GLfloat) {
    GL.vertexAttrib2f(index, x, y)
    assertError()
}


actual fun glVertexAttrib2fv(index: GLuint, v: GLfloatPointer) {
    GL.vertexAttrib2fv(index, v.toJsFloat32Array())
    assertError()
}


actual fun glVertexAttrib3f(index: GLuint, x: GLfloat, y: GLfloat, z: GLfloat) {
    GL.vertexAttrib3f(index, x, y, z)
    assertError()
}


actual fun glVertexAttrib3fv(index: GLuint, v: GLfloatPointer) {
    GL.vertexAttrib3fv(index, v.toJsFloat32Array())
    assertError()
}


actual fun glVertexAttrib4f(index: GLuint, x: GLfloat, y: GLfloat, z: GLfloat, w: GLfloat) {
    GL.vertexAttrib4f(index, x, y, z, w)
    assertError()
}


actual fun glVertexAttrib4fv(index: GLuint, v: GLfloatPointer) {
    GL.vertexAttrib4fv(index, v.toJsFloat32Array())
    assertError()
}


actual fun glVertexAttribDivisor(index: GLuint, divisor: GLuint) {
    GL.vertexAttribDivisor(index, divisor)
    assertError()
}


actual fun glVertexAttribI4i(index: GLuint, x: GLint, y: GLint, z: GLint, w: GLint) {
    GL.vertexAttribI4i(index, x, y, z, w)
    assertError()
}


actual fun glVertexAttribI4iv(index: GLuint, v: GLintPointer) {
    GL.vertexAttribI4iv(index, v.toJsInt32Array())
    assertError()
}


actual fun glVertexAttribI4ui(index: GLuint, x: GLuint, y: GLuint, z: GLuint, w: GLuint) {
    GL.vertexAttribI4ui(index, x, y, z, w)
    assertError()
}


actual fun glVertexAttribI4uiv(index: GLuint, v: GLuintPointer) {
    GL.vertexAttribI4uiv(index, v.toUint32List())
    assertError()
}


actual fun glVertexAttribIPointer(index: GLuint, size: GLint, type: GLenum, stride: GLsizei, offset: GLsizei) {
    GL.vertexAttribIPointer(index, size, type, stride, offset.toLong())
    assertError()
}


actual fun glVertexAttribPointer(
    index: GLuint,
    size: GLint,
    type: GLenum,
    normalized: GLboolean,
    stride: GLsizei,
    offset: GLsizei
) {
    GL.vertexAttribPointer(index, size, type, normalized, stride, offset)
    assertError()
}


actual fun glViewport(x: GLint, y: GLint, width: GLsizei, height: GLsizei) {
    GL.viewport(x, y, width, height)
    assertError()
}


actual fun glWaitSync(sync: GLsync, flags: GLbitfield, timeout: GLuint64) {
    GL.waitSync(sync as WebGLSync, flags, timeout)
    assertError()
}


