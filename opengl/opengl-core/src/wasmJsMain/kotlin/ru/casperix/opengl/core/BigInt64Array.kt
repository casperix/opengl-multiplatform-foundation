package ru.casperix.opengl.core

import org.khronos.webgl.ArrayBuffer
import org.khronos.webgl.ArrayBufferView

/**
 * Exposes the JavaScript [BigInt64Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt64Array) to Kotlin
 */
public external open class BigInt64Array : ArrayBufferView {
	constructor(length: Int)
	constructor(array: BigInt64Array)
	constructor(array: JsArray<JsBigInt>)
	constructor(buffer: ArrayBuffer, byteOffset: Int = definedExternally, length: Int = definedExternally)
	open val length: Int
	override val buffer: ArrayBuffer
	override val byteOffset: Int
	override val byteLength: Int
	fun set(array: BigInt64Array, offset: Int = definedExternally)
	fun set(array: JsArray<JsBigInt>, offset: Int = definedExternally)
	fun subarray(start: Int, end: Int): BigInt64Array

	companion object {
		val BYTES_PER_ELEMENT: Int
	}
}