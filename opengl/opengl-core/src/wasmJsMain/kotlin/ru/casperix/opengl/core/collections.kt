package ru.casperix.opengl.core

import Float32List
import Int32List
import Uint32List
import org.khronos.webgl.*

fun Array<String>.toJsStringArray(): JsArray<JsString> {
    val output = JsArray<JsString>()
    indices.forEach { output[it] = this[it].toJsString() }
    return output
}

fun JsArray<*>.toFloatArray(): Array<Float> {
    val output = Array<Float>(length) {index->
        val next = this[index]
        if (next is JsNumber) next.toDouble().toFloat()
        else throw Exception("Item is not number: $next")
    }
    return output
}

fun FloatArray.toJsNumberArray(): JsArray<JsNumber> {
    val output = JsArray<JsNumber>()
    indices.forEach { output[it] = this[it].toDouble().toJsNumber() }
    return output
}

fun IntArray.toJsNumberArray(): JsArray<JsNumber> {
    val output = JsArray<JsNumber>()
    indices.forEach { output[it] = this[it].toJsNumber() }
    return output
}

fun ByteArray.toJsInt8Array(): Int8Array {
    val output = Int8Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun ByteArray.toJsUInt8Array(): Uint8Array {
    val output = Uint8Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun ShortArray.toJsInt16Array(): Int16Array {
    val output = Int16Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}


fun IntArray.toJsInt32Array(): Int32Array {
    val output = Int32Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun IntArray.toJsUint32Array(): Uint32Array {
    val output = Uint32Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun LongArray.toJsBigInt64Array(): BigInt64Array {
    TODO("BigInt64Array haven't full api")
}

fun FloatArray.toJsFloat32Array(): Float32Array {
    val output = Float32Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun DoubleArray.toJsFloat64Array(): Float64Array {
    val output = Float64Array(size)
    indices.forEach { output[it] = this[it] }
    return output
}


fun IntArray.toUint32List(): Uint32List {
    val output = Uint32List(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun IntArray.toInt32List(): Int32List {
    val output = Int32List(size)
    indices.forEach { output[it] = this[it] }
    return output
}

fun FloatArray.toFloat32List(): Float32List {
    val output = Float32List(size)
    indices.forEach { output[it] = this[it] }
    return output
}
