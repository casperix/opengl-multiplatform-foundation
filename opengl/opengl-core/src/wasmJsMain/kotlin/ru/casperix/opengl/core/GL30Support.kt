package ru.casperix.opengl.core

import WebGLQuery
import WebGLSampler
import WebGLVertexArrayObject
import org.khronos.webgl.*

internal val bufferMap = IDMap<WebGLBuffer>()
internal val frameBufferMap = IDMap<WebGLFramebuffer>()
internal val renderBufferMap = IDMap<WebGLRenderbuffer>()
internal val shaderMap = IDMap<WebGLShader>()
internal val programMap = IDMap<WebGLProgram>()
internal val VAOMap = IDMap<WebGLVertexArrayObject>()
internal val textureMap = IDMap<WebGLTexture>()
internal val samplerMap = IDMap<WebGLSampler>()
internal val queryMap = IDMap<WebGLQuery>()


sealed class GLParameters {
	companion object {
		fun decodeToIntArray(value:JsAny?, buffer:IntArray) {
			val parameter = decode(value)
			if (parameter is GLIntParameter) {
				buffer[0] = parameter.value
			} else if (parameter is GLBooleanParameter) {
				buffer[0] = if (parameter.value) 1 else 0
			}
		}

		fun decodeToLongArray(value:JsAny?, buffer:LongArray) {
			val parameter = decode(value)
			if (parameter is GLLongParameter) {
				buffer[0] = parameter.value
			} else if (parameter is GLBooleanParameter) {
				buffer[0] = if (parameter.value) 1L else 0L
			}
		}

		fun decodeToFloatArray(value:JsAny?, buffer:FloatArray) {
			val parameter = decode(value)
			if (parameter is GLFloat2Parameter) {
				buffer[0] = parameter.x
				buffer[1] = parameter.y
			} else if (parameter is GLFloat4Parameter) {
				buffer[0] = parameter.x
				buffer[1] = parameter.y
				buffer[2] = parameter.z
				buffer[3] = parameter.w
			}
		}

		fun decodeToString(value:JsAny?):String? {
			val parameter = decode(value)
			if (parameter is GLStringParameter) {
				return parameter.value
			}
			return null

		}
		fun decodeToInt(value:JsAny?):Int? {
			val parameter = decode(value)
			if (parameter is GLIntParameter) {
				return parameter.value
			}
			return null
		}

		fun decode(value: JsAny?): GLParameters? {
			if (value == null) return null

			if (value is JsBoolean) return GLBooleanParameter(value.toBoolean())

			if (value is JsArray<*>) {
				val value  = value.toFloatArray()
				if (value.size == 2) {
					return GLFloat2Parameter(value[0], value[1])
				} else if (value.size == 4) {
					return GLFloat4Parameter(value[0], value[1], value[2], value[3])
				} else {
					throw Error("Expected float amount is 2 or 4, but actual: ${value.size}")
				}
			}
			if (value is JsString) return GLStringParameter(value.toString())
			if (value is JsBigInt) return GLLongParameter(value.toLong())
			if (value is JsNumber) return GLIntParameter(value.toInt())
			if (value is WebGLBuffer) return GLBufferParameter(value)
			return null
		}
	}
}

class GLStringParameter(val value: String) : GLParameters()
class GLBooleanParameter(val value: Boolean) : GLParameters()
class GLIntParameter(val value: Int) : GLParameters()
class GLLongParameter(val value: Long) : GLParameters()
class GLFloat2Parameter(val x: Float, val y: Float) : GLParameters()
class GLFloat4Parameter(val x: Float, val y: Float, val z: Float, val w: Float) : GLParameters()
class GLBufferParameter(val value: WebGLBuffer) : GLParameters()

fun WebGLActiveInfo.toGLActiveInfo(): GLActiveInfo {
	return GLActiveInfo(size, type, name)
}

