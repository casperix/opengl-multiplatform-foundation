import ru.casperix.input.*
import ru.casperix.math.vector.float32.Vector2f
import kotlinx.browser.window
import org.w3c.dom.Window
import ru.casperix.opengl.core.input.KeyMap

class WasmInput(window: Window, val input: (InputEvent) -> Unit) {
    init {
        window.apply {
            onmousemove = {
                input(MouseMove(getPosition(it.x, it.y)))
            }

            onmousedown = {
                val button = PointerButton(it.button.toInt())
                input(MouseDown(getPosition(it.x, it.y), button))
            }
            onmouseup = {
                val button = PointerButton(it.button.toInt())
                input(MouseUp(getPosition(it.x, it.y), button))
            }
            onkeydown = {
                input(KeyDown(getKeyButton(it.code)))
            }
            onkeyup = {
                input(KeyUp(getKeyButton(it.code)))
            }
            onkeypress = {
                if (it.key.length == 1) {
                    input(KeyTyped(it.key.first(), getKeyButton(it.code)))
                }
            }
        }
    }

    private fun getKeyButton(code: String): KeyButton {
        return KeyMap.jsCodeToApi[code] ?: KeyButton.UNKNOWN_KEY
    }

    private fun getPosition(x: Double, y: Double): Vector2f {
        val factor = window.devicePixelRatio.toFloat()
        return Vector2f(x.toFloat(), y.toFloat()) * factor
    }
}