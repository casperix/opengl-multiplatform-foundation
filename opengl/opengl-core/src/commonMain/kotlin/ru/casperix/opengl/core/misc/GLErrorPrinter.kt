package ru.casperix.opengl.core.misc

import io.github.oshai.kotlinlogging.KotlinLogging
import ru.casperix.opengl.core.GL_INVALID_FRAMEBUFFER_OPERATION
import ru.casperix.opengl.core.GLenum
import ru.casperix.opengl.core.glGetError

object GLErrorPrinter {
    private val logger = KotlinLogging.logger { }

    /**
     *  Has an extreme impact on performance. Use only for debugging
     */
    var debugThrowGlError = false
    var debugThrowGlErrorOnceOnly = false

    //TODO: move to extension
    const val GL_NO_ERROR = 0
    const val GL_INVALID_ENUM = 0x0500
    const val GL_INVALID_VALUE = 0x0501
    const val GL_INVALID_OPERATION = 0x0502
    const val GL_OUT_OF_MEMORY = 0x0505

    private val existErrors = mutableSetOf<Int>()
    private val errorMap = mapOf<GLenum, String>(
        Pair(
            GL_INVALID_ENUM,
            "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag."
        ),
        Pair(
            GL_INVALID_VALUE,
            " A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag."
        ),
        Pair(
            GL_INVALID_OPERATION,
            "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag."
        ),
        Pair(
            GL_INVALID_FRAMEBUFFER_OPERATION,
            "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag."
        ),
        Pair(
            GL_OUT_OF_MEMORY,
            "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded."
        ),
//        Pair(GL_STACK_UNDERFLOW,"An attempt has been made to perform an operation that would cause an internal stack to underflow."),
    )

    fun throwIfExist() {
        ifError {
            throw Exception(it)
        }
    }

    fun printIfExist() {
        ifError {
            logger.warn { it }
        }
    }

    fun ifError(errorPrinter: (String) -> Unit) {
        val errorCode = glGetError()
        if (errorCode == GL_NO_ERROR) {
            return
        }

        val newError = existErrors.add(errorCode)

        if (debugThrowGlErrorOnceOnly && !newError) {
            return
        }

        val message = errorMap[errorCode] ?: "Unknown error: $errorCode"
        errorPrinter("GL error ($errorCode):\n$message")
    }

}






