package ru.casperix.opengl.core

/**	EXTENSIONS	**/
fun glGenTexture():Int {
	val ints = IntArray(1)
	glGenTextures(ints)
	return ints[0]
}

fun glDeleteTexture(id: GLint):Int {
	val ints = intArrayOf(id)
	glDeleteTextures(ints)
	return ints[0]
}

fun glGenBuffer():Int {
	val ints = IntArray(1)
	glGenBuffers(ints)
	return ints[0]
}

fun glGenVertexArray():Int {
	val ints = IntArray(1)
	glGenVertexArrays(ints)
	return ints[0]
}

fun glGetShaderi(shader: GLuint, parameterName: GLenum):Int {
	val ints = IntArray(1)
	glGetShaderiv(shader, parameterName, ints)
	return ints[0]
}

fun glGetProgrami(shader: GLuint, parameterName: GLenum):Int {
	val ints = IntArray(1)
	glGetProgramiv(shader, parameterName, ints)
	return ints[0]
}