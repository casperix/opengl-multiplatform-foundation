package ru.casperix.opengl.core.misc

object ShaderFactory {
    fun setup(source: String, defines: Set<String> = emptySet(), customVersion: GLSLVersion? = null): String {
        val version = customVersion ?: GLPlatform.version

        val blocks = mutableListOf<String>()

        blocks += listOf("#version ${version.code}")

        blocks += defines.map {
            "#define $it"
        }

        if (version.es) blocks += "precision highp float;"

        return blocks.joinToString("\n") + "\n\n" + source
    }
}