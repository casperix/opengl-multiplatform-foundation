package ru.casperix.opengl.core.loader

import ru.casperix.misc.time.getTime
import kotlin.time.Duration

class FrameTimeCalculator() {
    private var lastTime = getTime()

    fun tick(): Duration {
        val nextTime = getTime()
        val delta = nextTime - lastTime
        lastTime = nextTime
        return delta
    }
}