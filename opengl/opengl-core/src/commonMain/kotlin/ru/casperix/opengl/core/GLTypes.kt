package ru.casperix.opengl.core

import ru.casperix.misc.Either


//special thanks for unique webgl api
typealias GLLocation = Any

/**
 * 	https://www.khronos.org/opengl/wiki/OpenGL_Type
 */
typealias GLboolean = Boolean
typealias GLbyte = Byte
typealias GLubyte = Byte//must unsigned
typealias GLshort = Short
typealias GLushort = Short//must unsigned
typealias GLint = Int
typealias GLuint = Int//must unsigned
typealias GLfixed = Int
typealias GLint64 = Long
typealias GLuint64 = Long
typealias GLsizei = Int
typealias GLenum = Int
typealias GLintptr = Long//pointer used as byte shift
typealias GLsizeiptr = Long//pointer used as byte shift
typealias GLsync = Any
typealias GLbitfield = Int
//typealias GLhalf = 16 bit float
typealias GLfloat = Float
typealias GLclampf = Float
typealias GLdouble = Double
typealias GLclampd = Double

typealias GLcharPointer = String
typealias GLfloatPointer = FloatArray
typealias GLintPointer = IntArray
typealias GLuintPointer = IntArray
typealias GLenumPointer = IntArray
typealias GLbooleanPointer = BooleanArray
typealias GLint64Pointer = LongArray
typealias voidPointer = ByteArray

typealias GLcharArray = String
typealias GLfloatArray = FloatArray
typealias GLintArray = IntArray
typealias GLuintArray = IntArray
typealias GLenumArray = IntArray
typealias GLbooleanArray = BooleanArray
typealias GLint64Array = LongArray
typealias GLsizeiArray = IntArray
typealias voidArray = ByteArray
typealias GLubytePointer = ByteArray


class GLActiveInfo(val size: Int, val type: Int, val name: String)