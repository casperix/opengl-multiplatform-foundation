package  ru.casperix.opengl.core

fun isPowerOf2(value: Long): Boolean {
    return value > 0L && value and value - 1L == 0L
}

fun isPowerOf2(value: Int): Boolean {
    return value > 0 && value and value - 1 == 0
}