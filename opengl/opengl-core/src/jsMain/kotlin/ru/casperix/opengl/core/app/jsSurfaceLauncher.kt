package ru.casperix.opengl.core.app

import JsInput
import WebGL2RenderingContext
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.app.surface.SurfaceProvider
import ru.casperix.opengl.core.loader.FrameTimeCalculator
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.opengl.core.GL
import ru.casperix.opengl.core.glFlush
import kotlinx.browser.document
import kotlinx.browser.window
import org.w3c.dom.HTMLCanvasElement


private var hasRequestFrame = false

fun jsSurfaceLauncher(creator: (Surface) -> SurfaceListener) {
    val canvas = document.querySelector("#canvas")
        ?: throw Error("id \"canvas\" not found")
    if (canvas !is HTMLCanvasElement) {
        throw Error("element type must be \"canvas\"")
    }

    val renderingContext = canvas.getContext("webgl2", js("{alpha: false, premultipliedAlpha: true}"))
        ?: throw Error("WebGL not supported")
    if (renderingContext !is WebGL2RenderingContext) throw Error("Invalid rendering context")

    GL = renderingContext

    var requestCallback: () -> Unit = { }

    val page = WebPageImpl(canvas) {
        requestCallback()
    }

    SurfaceProvider.surface = page
    val listener = creator(page)
    updateSize(canvas)

    window.onresize = {
        updateSize(canvas)
    }

    JsInput(window, listener::input)

    val timer = FrameTimeCalculator()
    requestCallback = {
        if (!hasRequestFrame) {
            hasRequestFrame = true
            window.requestAnimationFrame {
                step(listener, timer)
            }
        }
    }

    requestCallback()
}

fun step(listener: SurfaceListener, timer: FrameTimeCalculator) {
    hasRequestFrame = false

    if (PlatformMisc.continuousRendering) {
        hasRequestFrame = true
        window.requestAnimationFrame {
            step(listener, timer)
        }
    }

    listener.nextFrame(timer.tick())
    glFlush()
}

fun updateSize(canvas: HTMLCanvasElement) {
    val scale = window.devicePixelRatio
    val nextSize =
        (Vector2i(window.innerWidth, window.innerHeight).toVector2f() * scale.toFloat()).toVector2i().toDimension2i()

    canvas.width = nextSize.width
    canvas.height = nextSize.height
}
