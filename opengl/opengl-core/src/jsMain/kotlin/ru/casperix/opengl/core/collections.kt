package ru.casperix.opengl.core

import Float32List
import Int32List
import Uint32List
import org.khronos.webgl.*

fun ByteArray.toJsInt8Array(): Int8Array {
	return Int8Array(toTypedArray())
}

fun ByteArray.toJsUInt8Array(): Uint8Array {
	return Uint8Array(toTypedArray())
}

fun ShortArray.toJsInt16Array(): Int16Array {
	return Int16Array(toTypedArray())
}


fun IntArray.toJsInt32Array(): Int32Array {
	return Int32Array(toTypedArray())
}

fun IntArray.toJsUint32Array(): Uint32Array {
	return Uint32Array(toTypedArray())
}

fun LongArray.toJsBigInt64Array(): BigInt64Array {
	return BigInt64Array(toTypedArray())
}

fun FloatArray.toJsFloat32Array(): Float32Array {
	return Float32Array(toTypedArray())
}

fun DoubleArray.toJsFloat64Array(): Float64Array {
	return Float64Array(toTypedArray())
}


fun IntArray.toUint32List(): Uint32List {
	return Uint32List(toTypedArray())
}

fun IntArray.toInt32List(): Int32List {
	return Int32List(toTypedArray())
}

fun FloatArray.toFloat32List(): Float32List {
	return Float32List(toTypedArray())
}
