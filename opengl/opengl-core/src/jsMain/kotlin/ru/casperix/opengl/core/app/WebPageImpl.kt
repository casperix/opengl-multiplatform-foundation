package ru.casperix.opengl.core.app

import ru.casperix.app.surface.WebPage
import ru.casperix.app.surface.component.Display
import ru.casperix.app.window.Cursor
import ru.casperix.app.window.SystemCursor
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.multiplatform.toDimension
import kotlinx.browser.document
import kotlinx.browser.window
import org.w3c.dom.HTMLCanvasElement

class WebPageImpl(val canvas: HTMLCanvasElement, val requestFrameCallback: () -> Unit) : WebPage {
    override fun setCursor(value: Cursor) {
        if (value is SystemCursor) {
            when (value) {
                SystemCursor.DEFAULT -> canvas.style.cursor = "default"
                SystemCursor.HAND -> canvas.style.cursor = "pointer"
                SystemCursor.TEXT -> canvas.style.cursor = "text"
                SystemCursor.WAIT -> canvas.style.cursor = "wait"
                SystemCursor.MOVE -> canvas.style.cursor = "move"
            }
        }
    }

    override fun getCursor(): Cursor {
        if (canvas.style.cursor == "default") return SystemCursor.DEFAULT
        if (canvas.style.cursor == "pointer") return SystemCursor.HAND
        if (canvas.style.cursor == "text") return SystemCursor.TEXT
        if (canvas.style.cursor == "wait") return SystemCursor.WAIT
        if (canvas.style.cursor == "move") return SystemCursor.MOVE
        return SystemCursor.DEFAULT
    }

    override fun getDisplay(): Display {
        val screen = window.screen
        val pixelSize = (Vector2i(screen.width, screen.height).toVector2d() * window.devicePixelRatio).roundToVector2i()
        val densityPerInch = (window.devicePixelRatio * 96.0)
        val physicalSize = pixelSize.toVector2d() / densityPerInch

        return Display(
            Vector2i.ZERO,
            pixelSize.toDimension2i(),
            physicalSize.toVector2f().toDimension(),
            screen.pixelDepth,
            0,
        )
    }

    override fun requestFrame() {
        requestFrameCallback()
    }


    override fun setTitle(value: String) {
        window.name = value
    }

    override fun getTitle(): String {
        return window.name

    }

    override fun getVerticalSync(): Boolean {
        return false
    }

    override fun setVerticalSync(value: Boolean) {

    }

    override fun getSize(): Dimension2i {
        return (Vector2i(
            window.innerWidth,
            window.innerHeight
        ).toVector2d() * window.devicePixelRatio).roundToVector2i().toDimension2i()
    }

    override fun getPosition(): Vector2i {
        return (Vector2i(
            window.screenX,
            window.screenY
        ).toVector2d() * window.devicePixelRatio).roundToVector2i()
    }

    override fun setFullScreen(value: Boolean) {
        if (value) {
            canvas.requestFullscreen()
        } else {
            document.exitFullscreen()
        }
    }

}