package ru.casperix.opengl.core

import WebGLQuery
import WebGLSampler
import WebGLVertexArrayObject
import org.khronos.webgl.*

internal val bufferMap = IDMap<WebGLBuffer>()
internal val frameBufferMap = IDMap<WebGLFramebuffer>()
internal val renderBufferMap = IDMap<WebGLRenderbuffer>()
internal val shaderMap = IDMap<WebGLShader>()
internal val programMap = IDMap<WebGLProgram>()
internal val VAOMap = IDMap<WebGLVertexArrayObject>()
internal val textureMap = IDMap<WebGLTexture>()
internal val samplerMap = IDMap<WebGLSampler>()
internal val queryMap = IDMap<WebGLQuery>()


sealed class GLParameters {
	companion object {
		fun decodeToIntArray(value:Any?, buffer:IntArray) {
			val parameter = decode(value)
			if (parameter is GLIntParameter) {
				buffer[0] = parameter.value
			} else if (parameter is GLBooleanParameter) {
				buffer[0] = if (parameter.value) 1 else 0
			}
		}

		fun decodeToLongArray(value:Any?, buffer:LongArray) {
			val parameter = decode(value)
			if (parameter is GLLongParameter) {
				buffer[0] = parameter.value
			} else if (parameter is GLBooleanParameter) {
				buffer[0] = if (parameter.value) 1L else 0L
			}
		}

		fun decodeToFloatArray(value:Any?, buffer:FloatArray) {
			val parameter = decode(value)
			if (parameter is GLFloat2Parameter) {
				buffer[0] = parameter.x
				buffer[1] = parameter.y
			} else if (parameter is GLFloat4Parameter) {
				buffer[0] = parameter.x
				buffer[1] = parameter.y
				buffer[2] = parameter.z
				buffer[3] = parameter.w
			}
		}

		fun decodeToString(value:Any?):String? {
			val parameter = decode(value)
			if (parameter is GLStringParameter) {
				return parameter.value
			}
			return null

		}
		fun decodeToInt(value:Any?):Int? {
			val parameter = decode(value)
			if (parameter is GLIntParameter) {
				return parameter.value
			}
			return null
		}

		fun decode(value: Any?): GLParameters? {
			if (value == null) return null

			if (value is Boolean) return GLBooleanParameter(value)

			if (value is Float32Array) {
				if (value.length == 2) {
					return GLFloat2Parameter(value[0], value[1])
				} else if (value.length == 4) {
					return GLFloat4Parameter(value[0], value[1], value[2], value[3])
				} else {
					throw Error("Expected float amount is 2 or 4, but actual: ${value.length}")
				}
			}
			if (value is String) return GLStringParameter(value)
			if (value is Long) return GLLongParameter(value)
			if (value is Int) return GLIntParameter(value)
			if (value is WebGLBuffer) return GLBufferParameter(value)
			return null
		}
	}
}

class GLStringParameter(val value: String) : GLParameters()
class GLBooleanParameter(val value: Boolean) : GLParameters()
class GLIntParameter(val value: Int) : GLParameters()
class GLLongParameter(val value: Long) : GLParameters()
class GLFloat2Parameter(val x: Float, val y: Float) : GLParameters()
class GLFloat4Parameter(val x: Float, val y: Float, val z: Float, val w: Float) : GLParameters()
class GLBufferParameter(val value: WebGLBuffer) : GLParameters()

fun WebGLActiveInfo.toGLActiveInfo(): GLActiveInfo {
	return GLActiveInfo(size, type, name)
}

